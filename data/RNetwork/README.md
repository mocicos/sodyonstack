# Annual network for the R community (from 2008-2009 to 2019)

Eahc year 'AAAA', the network is described with 2 files:
- edgesAAAAR.csv for the list of edges of the community
- nodesAAAAR.csv for the list of nodes of the community

<!-- find ./20* -type f -exec wc -l {} \; | sort -k2 | awk '{print substr($2,3,4), substr($2,8,4), "count",$1-1}' -->

- 2009 
  - 964 edges
  - 411 nodes
- 2010 
  - 3658 edges
  - 1241 nodes
- 2011 
  - 8594 edges
  - 2829 nodes
- 2012 
  - 15509 edges
  - 5386 nodes
- 2013 
  - 27331 edges
  - 9887 nodes
- 2014 
  - 35689 edges
  - 14280 nodes
- 2015 
  - 44243 edges
  - 19471 nodes
- 2016 
  - 47864 edges
  - 22973 nodes
- 2017 
  - 54975 edges
  - 26618 nodes
- 2018 
  - 58765 edges
  - 27626 nodes
- 2019 
  - 53887 edges
  - 26735 nodes

## Edges

There is an edge from a source to a destination iff the source user answers at
least once to a question of the destination user.

There is no loop, i.e an edge that joins a vertex to itself.

The relations are store in a CSV file with one relation per line and the
following header.

    Source,Target,Weight,Type,Community

Where:
  - `Source` is the identifier of the helper
  - `Target` is the identifier of the asker
  - `Weight` is the number of answers from the helper to the asker 
  - `Type` is `"directed"` since the graph is directed
  - `Community` is `"r"` or `"pandas"` 
   
## Nodes

A node is a member who contributes at least once during the early stage of the community.

    Id,Label,Reputation,Community,Zscore,MEC,Entrance_year,Exit_year

Where:
- `Id` is the identifier of the member
- `Label` is the full name on Stack Overflow
- `Reputation` is a rough measurement of how much the platform trusts the user
- `Community` is `"r"` or `"pandas"` 
- `Zscore` is positive if, during the whole community life, the member is a responder and negative otherwise 
- `MEC` is a measure of the expertise of the member during the whole community life
- `in_degree` is the number of answers received by the member
- `out_degree` is  the number of answers sent by the member
- `Entrance_year` is the year of the member's first contribution
  (question or answer) in the community
- `Exit_year` is the year of the member's last contribution
  (question or answer) 

