DROP DATABASE IF EXISTS rallstar;
CREATE DATABASE rallstar OWNER morge;

DROP TABLE IF EXISTS DWH_BODY;
CREATE TABLE DWH_BODY(
  dwh_body_id SERIAL,
  body TEXT,
  PRIMARY KEY (dwh_body_id)
);
DELETE FROM DWH_BODY;
\COPY DWH_BODY from bodies.csv CSV HEADER DELIMITER ';' QUOTE '"' ESCAPE '"';

DROP TABLE IF EXISTS DWH_CONTENT;
CREATE TABLE DWH_CONTENT(
  dwh_content_id SERIAL,
  title VARCHAR(512),
  tags VARCHAR(512),
  body TEXT,
  PRIMARY KEY (dwh_content_id)
);

DELETE FROM DWH_CONTENT;
\COPY DWH_CONTENT from contents.csv CSV HEADER DELIMITER ';' QUOTE '"' ESCAPE '"';

DROP TABLE IF EXISTS DWH_USER;
CREATE TABLE DWH_USER(
  dwh_user_id SERIAL,
  reputation INTEGER,
  views INTEGER,
  down_votes INTEGER,
  up_votes INTEGER,
  creation_date TIMESTAMP,
  last_access_date TIMESTAMP,
  display_name VARCHAR(255),
  location VARCHAR(512),
  website_url VARCHAR(255),
  about_me TEXT,
  PRIMARY KEY (dwh_user_id)
);

DELETE FROM DWH_USER;
\COPY DWH_USER from users.csv CSV HEADER DELIMITER ';' QUOTE '"' ESCAPE '"';

DROP TABLE IF EXISTS DWH_DATE;
CREATE TABLE DWH_DATE (
  dwh_date_id SERIAL,
  date TIMESTAMP,
  day VARCHAR(10),
  numberofday INTEGER,
  numberofweek INTEGER,
  month VARCHAR(10),
  numberofmonth INTEGER,
  year NUMERIC(4),
  epoch_date BIGINT,
  PRIMARY KEY (dwh_date_id)
);

DELETE FROM DWH_DATE;
\COPY DWH_DATE from dates.csv CSV HEADER DELIMITER ';' QUOTE '"' ESCAPE '"';

DROP TABLE IF EXISTS DWH_POSTTYPE;
CREATE TABLE DWH_POSTTYPE(
  dwh_post_type_id SERIAL,
  post_type VARCHAR(64),
  PRIMARY KEY (dwh_post_type_id)
);

DELETE FROM DWH_POSTTYPE;
\COPY DWH_POSTTYPE from posttypes.csv CSV HEADER DELIMITER ';' QUOTE '"' ESCAPE '"';

DROP TABLE IF EXISTS DWH_CONTRIBUTES;
CREATE TABLE DWH_CONTRIBUTES(
  dwh_author_id SERIAL,
  dwh_kind_of_id SERIAL,
  dwh_creation_date_id SERIAL,
  dwh_contains_id SERIAL,
  dwh_interlocutor_id INTEGER,
  dwh_last_activity_id INTEGER,
  dwh_last_edit_date_id INTEGER,
  dwh_reply_id INTEGER,
  score INTEGER,
  view_count INTEGER,
  favorite_count INTEGER,
  answer_count INTEGER,
  comment_count INTEGER,
  delay BIGINT,
  is_accepted BOOLEAN,
  stamp TIMESTAMP,
  PRIMARY KEY (dwh_author_id, dwh_contains_id, dwh_creation_date_id, dwh_kind_of_id),
  CONSTRAINT dwh_fk_author
  FOREIGN KEY (dwh_author_id)
  REFERENCES DWH_User(dwh_user_id),
  CONSTRAINT dwh_fk_kind_of
  FOREIGN KEY (dwh_kind_of_id)
  REFERENCES DWH_PostType(dwh_post_type_id),
  CONSTRAINT dwh_fk_creation_date
  FOREIGN KEY (dwh_creation_date_id)
  REFERENCES DWH_Date(dwh_date_id),
  CONSTRAINT dwh_fk_contains
  FOREIGN KEY (dwh_contains_id)
  REFERENCES DWH_Content(dwh_content_id)
);

DELETE FROM DWH_CONTRIBUTES;
\COPY DWH_CONTRIBUTES from contributes.csv CSV HEADER DELIMITER ';' QUOTE '"' ESCAPE '"';


DROP TABLE IF EXISTS DWH_COMMENTS;
CREATE TABLE DWH_COMMENTS(
  dwh_author_id SERIAL,
  dwh_reacts_id SERIAL,
  dwh_contains_id SERIAL,
  dwh_creation_date_id SERIAL,
  dwh_interlocutor_id INTEGER,
  score INTEGER,
  delay BIGINT,
  stamp TIMESTAMP,
  PRIMARY KEY (dwh_author_id, dwh_reacts_id, dwh_contains_id, dwh_creation_date_id),
  CONSTRAINT dwh_fk_author
  FOREIGN KEY (dwh_author_id)
  REFERENCES DWH_User(dwh_user_id),
  CONSTRAINT dwh_fk_reacts
  FOREIGN KEY (dwh_reacts_id)
  REFERENCES DWH_Content(dwh_content_id),
  CONSTRAINT dwh_fk_creation_date
  FOREIGN KEY (dwh_creation_date_id)
  REFERENCES DWH_Date(dwh_date_id),
  CONSTRAINT dwh_fk_contains
  FOREIGN KEY (dwh_contains_id)
  REFERENCES DWH_Body(dwh_body_id)
);

DELETE FROM DWH_COMMENTS;
\COPY DWH_COMMENTS from comments.csv CSV HEADER DELIMITER ';' QUOTE '"' ESCAPE '"';
