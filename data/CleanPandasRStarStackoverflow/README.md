# Data warehouse for the Tag `<r>` 

![Star schema](./star.svg "Diagram")

[Posgres importation script](./import.sql)


Data files :
- contents.csv 1,4G with  over 1 268 600 contents
- smallContents.csv 22 Mo with  over 1 268 600 contents
- users.csv 31M with over 242 148 users
- dates.csv 273M with over 3 810 627 dates
- posttypes.csv 47B with 2 post types
- contributes.csv 98M with over 1 268 600 contributions
- bodies.csv 299M with over 1 934 894 bodies
- comments.csv 121M with over 1 932 342 comments

The dataset is consolidated since :
- 3 879 anonymous contributions have been removed.
- 2 552 comments with uncoherent date have been removed.
- 2 008 contributions (questions/answers) have been removed

## Dictionary

### DWH_Content

- `dwh_content_id` the primary key 
- `title` a single sentence
- `tags` a set of tag e.g. `<r><foreach><parallel-processing><xts>`
- `body` the content

### DWH_Content

- `dwh_content_id` the primary key 
- `tags` a set of tag e.g. `<r><foreach><parallel-processing><xts>`

### DWH_Body

- `dwh_body_id` the primary key 
- `body` the content of the comment

### DWH_User

- `dwh_user_id` the primary key 
- `reputation` a rough measurement of how much the community trusts the user
- `views` number of times the profile is viewed
- `down_votes` how many downvotes the user has cast
- `up_votes` how many upvotes the user has cast
- `creation_date` datetime user creates it account
- `last_access_date` datetime user last loaded a page
- `display_name` the full name
- `location` city and country
- `website_url` personal webpage
- `about_me` biography

### DWH_Date 

- `dwh_date_id` is the primary key
- `date` datetime
- `day` name of the day
- `numberOfDay` the number of the day in the month
- `numberOfWeek` the number of the week in the year
- `month` the name of the month
- `numberOfMonth` the number of the month in the year
- `year` the number of the year
- `epoch_date`  the number of seconds since 1970-01-01 00:00:00-00

### DWH_PostType 

- `dwh_post_type_id` is the primary key
- `post_type` is `Question` or  `Answer`

### DWH_Contributes

- `dwh_author_id` is the author, a foreign key which refers to the primary key of the relation `DWH_User`
- `dwh_kind_of_id` is the contribution type, a foreign key which refers to the primary key of the relation `DWH_PostType`
- `dwh_creation_date_id` is the datetime when the contributes is created, a foreign key which refers to the primary key of the relation `DWH_Date`
- `dwh_contains_id` is the content, a foreign key which refers to the primary key of the relation `DWH_Content`
- `dwh_interlocutor_id` is eventually the interlocutor of the contribution
   - either the author of the question for an answer 
   - or the author of the accepted answer for a question 
   a foreign key which refers to the primary key of the relation `DWH_User`
- `dwh_last_activity_id` is eventually the datetime when the contribution has changed or answers has been added/edited, a foreign key which refers to the primary key of the relation `DWH_Date` 
- `dwh_last_edit_date_id` is eventually the datetime when users last edited the contribution, a foreign key which refers to the primary key of the relation `DWH_Date` 
- `dwh_reply_id` is eventually the mirror of the content,
   - either the question for an answer 
   - or the accepted answer for a question 
   a foreign key which refers to the primary key of the relation `DWH_Content`
- `score` is a rough measurement of how much the question is good
- `view_count` is the number of times the contribute is viewed
- `answer_count` is the number of answers
- `comment_count`  is the number of comments
- `favorite_count` is the the number of users who marked the question as favorite
- `delay` is eventually the number of seconds since the question if the contribution is an answer
- `is_accepted` is true if the contribution is an accepted answers
-  `stamp` is the timestamp  of the contribution

### DWH_Comments

-  `dwh_author_id` is the author, a foreign key which refers to the primary key of the relation `DWH_User`
-  `dwh_reacts_id` is the content which is commented, a foreign key which refers
   to the primary key of the relation `DWH_Content`
-  `dwh_contains_id` is the body of the comment, a foreign key which refers to
   the primary key of the relation `DWH_Body`
-  `dwh_creation_date_id` is the datetime when the contributes is created, a
   foreign key which refers to the primary key of the relation `DWH_Date`
-  `dwh_interlocutor_id` is the interlocutor of the comments 
   - either the author of the question for an answer 
   - or the author of the accepted answer for a question 
   a foreign key which refers to the primary key of the relation `DWH_User`
-  `score`  a rough measurement of how much the question is good
-  `delay` is eventually the number of seconds since the question if the contribution is an answer
-  `stamp` is the timestamp  of the comment
