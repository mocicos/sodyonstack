# Data analysis schema documentation 

![Multidimensional Conceptual Data Model](./mcmd.pdf "ERD Diagram")

![Star schema](./star.pdf "Logical Diagram")

## What are the questions ?

- The proxy of the collective knowledge and the technological evolution for a
community, i.e the average response time per tag ∂(Tag)

- The size of epistemic communities, i.e. the number of active users per tag
  card(Tag)

- The ratio ∂(Tag)/card(Tag). If the ratio is low (a lot of people does not
  reply quickly), the community disappear. Otherwise (few people answers
  quickly), the community is made of early adopters promoting this technology.

## What are the facts ?

A user can : 
1. make a post (ask a question or gives an answer)
2. make a comment

We can first restrict ourself to the posts (postTypeId = 1 or 2)

## Dimensions

### Who?

Users (having some badges)

### At Whom ?

An interlocutor (i.e. an inquirer or a responder), i.e. a user

### When?

The date of : 
- the creation date of the post/comment
- eventually, the last activity date about the post
- eventually, the last edit date
- eventually, the delay of reply

### Where ?

Not relevant

### What ?

A contribution content:
- a body
- some tags
- a title

A comment content:
- a text
- some tags
- a title

### How ?

PostType, i.e. a question or an answer eventually accepted

## Metrics

- score : INTEGER (+)
- view_count : INTEGER (+)
- favorite_count : INTEGER (+)
- answer_count : INTEGER (+)
- comment_count : INTEGER (+)
- delay : BIGINT (+)
- datetime : DATETIME (#)

