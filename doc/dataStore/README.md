# Data Model for Extraction

## Conceptual Data Model

![Conceptual Data Model](CDM/dataModelStore.svg)

## Logical Data Model

![Logical Data Model for Extraction](LDM/dataModelStore.svg)

## SQL script

[SQL for Extraction](SQL/dataModelStore_postgresql.svg)

