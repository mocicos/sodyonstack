# Database schema documentation for the public data dump and SEDE

![Entity-Relation Diagram](./ERDdiagram.png "ERD Diagram")

See also the [interactive HTML](https://sedeschema.github.io) version.

# Details

See this [thread](https://meta.stackexchange.com/questions/2677/database-schema-documentation-for-the-public-data-dump-and-sede/2678#2678) on Stack Exchange.


* <b>foreign key</b> fields are formatted as [links](https://data.stackexchange.com/stackoverflow/query/472607?table=posts) to their parent table
* <i>italic</i> table names are found in <b>both</b> the Data Dump on [Archive.org ](https://ia800500.us.archive.org/22/items/stackexchange/readme.txt)  as well as in the [SEDE](https://data.stackexchange.com/)

## <i><a name="Posts">Posts</a></i> / PostsWithDeleted

You find in `Posts` all non-deleted posts. `PostsWithDeleted` includes rows with
deleted posts while sharing the same columns with Posts but [for deleted posts
only a few fields
populated](https://meta.stackexchange.com/questions/157462/can-some-metadata-about-deleted-posts-be-included-in-data-se/266431#266431) which are marked with a <sup>1</sup> below.

- `Id`<sup>1</sup>

- `PostTypeId`<sup>1</sup> (listed in the PostTypes table)
    1 = Question
    2 = Answer
    3 = Orphaned tag wiki
    4 = Tag wiki excerpt
    5 = Tag wiki
    6 = Moderator nomination
    7 = "Wiki placeholder" (seems to only be the [election description](https://stackoverflow.com/posts/8041931/body))
    8 = Privilege wiki

- [`AcceptedAnswerId`](#Posts) (only present if `PostTypeId` = 1)

- `ParentId`<sup>1</sup> (only present if `PostTypeId` = 2)

- `CreationDate`<sup>1</sup>

- `DeletionDate`<sup>1</sup> (only non-null for the SEDE `PostsWithDeleted`
  table. Deleted posts are not present on Posts. Column not present on data
  dump.)

- `Score`<sup>1</sup>

- `ViewCount` (nullable)

- `Body` ([as rendered HTML](https://meta.stackexchange.com/questions/51177/include-markdown-post-bodies-in-the-data-dump), not Markdown)

- `OwnerUserId` (only present if user has not been deleted; always -1 for tag
  wiki entries, i.e. the community user owns them)

- `OwnerDisplayName` (nullable)

- [`LastEditorUserId`](#Users) (nullable)

- `LastEditorDisplayName` (nullable)

- `LastEditDate` (e.g. `2009-03-05T22:28:34.823`) - the date and time of the most
  recent edit to the post (nullable)

- `LastActivityDate` (e.g. `2009-03-11T12:51:01.480`) - datetime of the post's
  most recent activity

- `Title` (nullable)

- `Tags`<sup>1</sup> (nullable)

- `AnswerCount` (nullable)

- `CommentCount` (nullable)

- `FavoriteCount` (nullable)

- `ClosedDate`<sup>1</sup> (present only if the post is closed)

- `CommunityOwnedDate` (present only if post is community wiki'd)

- `ContentLicense`

## <i><a name="Users">Users</a></i>

- `Id`

- `Reputation`

- `CreationDate`

- `DisplayName`

- `LastAccessDate` ([Datetime user last loaded a page; updated every 30 min at
  most](https://meta.stackexchange.com/a/294750/230261))
    
- `WebsiteUrl` 

- `Location`

- `AboutMe`

- `Views` ([Number of times the profile is viewed](https://meta.stackexchange.com/questions/311938/what-is-views-of-users-in-the-database-schema-documentation))

- `UpVotes` ([How many upvotes the user has cast](https://meta.stackexchange.com/questions/260897/what-is-upvotes-of-users-in-database-schema-documentation))

- `DownVotes`

- `ProfileImageUrl`

- `EmailHash` (now always blank)

- `AccountId` (User's Stack Exchange Network profile ID)

## <i><a name="Badges">Badges</a></i>

- `Id`

- `UserId`

- `Name` (Name of the badge)

- `Date` (e.g. 2008-09-15T08:55:03.923)

- `Class`
    1 = Gold
    2 = Silver
    3 = Bronze

-  `TagBased` = `True` if badge is for a tag, otherwise it is a named badge


## Tags

- `Id`

- `TagName`

- `Count`

- `ExcerptPostId` (nullable) Id of Post that holds the excerpt text of the tag

- `WikiPostId` (nullable) Id of Post that holds the wiki text of the tag

- `IsModeratorOnly`

- `IsRequired`

## <i><a name="Comments">Comments</a></i>

- `Id`

- [`PostId`](#Posts)

- `Score`

- `Text` (Comment body)

- `CreationDate`

- `UserDisplayName`

- [`UserId`](#Users) (Optional. Absent if user has been deleted)

- `ContentLicense`
