# Some notes

## Data

### Data for datascience.stackexchange.com

https://archive.org/download/stackexchange/datascience.stackexchange.com.7z



### Data for stackoverflow.com

https://archive.org/download/stackexchange/stackoverflow.com-Badges.7z

https://archive.org/download/stackexchange/stackoverflow.com-Comments.7z

https://archive.org/download/stackexchange/stackoverflow.com-PostHistory.7z

https://archive.org/download/stackexchange/stackoverflow.com-PostLinks.7z

https://archive.org/download/stackexchange/stackoverflow.com-Posts.7z

https://archive.org/download/stackexchange/stackoverflow.com-Tags.7z

https://archive.org/download/stackexchange/stackoverflow.com-Users.7z

https://archive.org/download/stackexchange/stackoverflow.com-Votes.7z
