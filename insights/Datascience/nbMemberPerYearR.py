# Copyright Maxime MORGE, 2024
import pandas as pd
import matplotlib.pyplot as plt

# create DataFrame
df = pd.DataFrame({'Old-timers': [0, 152, 444, 1016, 1999, 3372, 4820, 6364, 7134, 7957, 7811],
                   'Newcomers': [411, 1089, 2385, 4370,7888, 10908, 14651, 16609, 19484, 19669, 18925]},
                   index=['2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019']
                   )

# create stacked bar chart for monthly temperatures
df.plot(kind='bar', stacked=True, color=['blue', 'green'])

# labels for x & y axis
plt.xlabel('Years')
plt.ylabel('Nb. of members')

# title of plot
plt.title('Number of Active Community Members R')

plt.savefig("figures/nbMemberPerYearR.svg", format="svg")
