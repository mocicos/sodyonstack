# Copyright Maxime MORGE, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import numpy as np

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
  SELECT rank() OVER (ORDER BY probability_post DESC, dwh_member_id) AS Position, probability_post 
    FROM DWH_Membership
   WHERE dwh_topic_id = 1
   ORDER BY probability_post DESC;      
"""
)
data = {"User rank over posts" : [], "Bi-weekly number of posts" : [], "Community" : []}
rows = cur.fetchall()

for i in rows:
     data["User rank over posts"].append(i[0])
     data["Bi-weekly number of posts"].append(i[1])
     data["Community"].append("R")


cur.execute("""
  SELECT rank() OVER (ORDER BY probability_post DESC, dwh_member_id) AS Position, probability_post 
    FROM DWH_Membership
   WHERE dwh_topic_id = 2
   ORDER BY probability_post DESC;
""")
rows = cur.fetchall()

for i in rows:
    data["User rank over posts"].append(i[0])
    data["Bi-weekly number of posts"].append(i[1])
    data["Community"].append("Pandas")

N = 140000
a = 200 #*33*10**(-53)
k = -.4  #-10
s = 0
for x in range(1,N):
     f = a  *  x**k + 1  - a * N**k
     s = s + f
     data["User rank over posts"].append(x)
     data["Bi-weekly number of posts"].append(f)
     data["Community"].append("Power law")


plot = sns.lineplot(x="User rank over posts", y="Bi-weekly number of posts", hue="Community", data=data)
pal = sns.color_palette()

cur.execute("""
    SELECT AVG(probability_post)
      FROM DWH_Membership
     WHERE dwh_topic_id = 1;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[0])

cur.execute("""
    SELECT AVG(probability_post)
      FROM DWH_Membership
     WHERE dwh_topic_id = 2;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[1])

plot.axhline(s/N, color=pal[2])
print(s/N)

#plot.set(xscale="log")
#plot.set(yscale="log")
plot.set(title = 'Propensity of users to contribute regularly')
fig=plot.get_figure()
fig.savefig("figures/reverseFrequency.svg", format="svg")
cur.close()
conn.close()
