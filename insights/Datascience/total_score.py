# Copyright Maxime MORGE, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import numpy as np

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
  SELECT rank() OVER (ORDER BY total_score ASC, dwh_member_id) AS Position, total_score
    FROM DWH_Membership
   WHERE dwh_topic_id = 1
   ORDER BY total_score
 ASC;
"""
)
data = {"User rank over total score" : [], "total score" : [], "Community" : []}
rows = cur.fetchall()

for i in rows:
     data["User rank over total score"].append(i[0])
     data["total score"].append(i[1])
     data["Community"].append("R")


cur.execute("""
  SELECT rank() OVER (ORDER BY total_score ASC, dwh_member_id) AS Position, total_score
    FROM DWH_Membership
   WHERE dwh_topic_id = 2
   ORDER BY total_score
 ASC;
""")
rows = cur.fetchall()
for i in rows:
    data["User rank over total score"].append(i[0])
    data["total score"].append(i[1])
    data["Community"].append("Pandas")

plot = sns.lineplot(x="User rank over total score", y="total score", hue="Community", data=data)
pal = sns.color_palette()

cur.execute("""
    SELECT AVG(total_score)
      FROM DWH_Membership
     WHERE dwh_topic_id = 1;
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[0])

cur.execute("""
    SELECT AVG(total_score)
      FROM DWH_Membership
     WHERE dwh_topic_id = 2;
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[1])
#plot.set(xscale="log")
#plot.set(yscale="log")
plot.set(title = 'Expertise of users')
fig=plot.get_figure()
fig.savefig("figures/total_score.svg", format="svg")
cur.close()
conn.close()
