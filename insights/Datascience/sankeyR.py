# Copyright Maxime MORGE, 2024
import plotly.graph_objects as go

# Données
sources = [
    "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", 
    "incoming", "incoming", "incoming", "incoming", "incoming", 
    "incoming", "incoming", "incoming", "incoming", "incoming",
    "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"
]
targets = [
    "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", 
    "2010", "2011", "2012", "2013", "2014", 
    "2015", "2016", "2017", "2018", "2019", 
    "outcoming", "outcoming", "outcoming", "outcoming", "outcoming", 
    "outcoming", "outcoming", "outcoming", "outcoming", "outcoming"
]
values = [
    152, 444, 1016, 1999, 3372, 4820, 6364, 7134, 7957, 7811, 
    1089, 2385, 4370, 7888, 10908, 
    14651, 16609, 19484, 19669, 18925,
    259, 797, 1813, 3587, 6515, 
    9460, 13107, 15839, 18661, 18924
]

# Palette de couleurs
color_palette = {
    "incoming": "rgba(50, 166, 140, 0.2)",  # vertAvenir
    "outcoming": "rgba(252, 83, 92, 0.2)",  # rougeAction
    "default": "rgba(86, 98, 237, 01)"     # indigoInclusif
}

# Création des couleurs des liens
link_colors = []
for source, target in zip(sources, targets):
    if source == "incoming":
        link_colors.append(color_palette["incoming"])  # Flux provenant de "incoming"
    elif target == "outcoming":
        link_colors.append(color_palette["outcoming"])  # Flux menant à "outcoming"
    else:
        link_colors.append(color_palette["default"])  # Flux par défaut

# Création des nœuds uniques
nodes = list(set(sources + targets))
node_indices = {node: i for i, node in enumerate(nodes)}

# Conversion des labels en indices
source_indices = [node_indices[source] for source in sources]
target_indices = [node_indices[target] for target in targets]

# Création du Sankey
fig = go.Figure(data=[go.Sankey(
    node=dict(
        pad=15,
        thickness=20,
        line=dict(color="black", width=0.5),
        label=nodes
    ),
    link=dict(
        source=source_indices,
        target=target_indices,
        value=values,
        color=link_colors
    )
)])

# Mise en forme
fig.update_layout(
    font_size=10
)#     title_text="Diagramme de Sankey avec Mise en Évidence des Flux",

# Enregistrement en PNG
fig.write_image("figures/sankeyR.png", format="png")

