# Copyright Maxime MORGE, 2024
import pandas as pd
import matplotlib.pyplot as plt

# create DataFrame
df = pd.DataFrame({'Old-timers': [0, 227, 588, 1081, 1803, 2852, 4214, 5549],
                   'Newcomers': [569, 1522, 2675, 4732, 7674, 12288, 17378, 22264]},
                   index=['2012', '2013', '2014', '2015', '2016', '2017', '2018', '2019']
                   )

# create stacked bar chart for monthly temperatures
df.plot(kind='bar', stacked=True, color=['blue', 'green'])

# labels for x & y axis
plt.xlabel('Years')
plt.ylabel('Nb. of members')

# title of plot
plt.title('Number of Active Community Members Pandas')

plt.savefig("figures/nbMemberPerYearPandas.svg", format="svg")
