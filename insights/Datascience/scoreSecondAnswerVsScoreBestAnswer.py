# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import pandas as pd

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Second best answer score" : [], "Community" : [], "Best answer score" : [], "Number of answers" : []}

cur.execute("""
  SELECT
    reponse2.score score_reponse2,
    reponse1.score score_reponse1
    FROM dwh_contributes reponse1
         JOIN dwh_content contenu_question
             ON reponse1.dwh_reply_id = contenu_question.dwh_content_id
         JOIN dwh_contributes reponse2 
             ON reponse2.dwh_reply_id = contenu_question.dwh_content_id
         JOIN dwh_contributes question
             ON contenu_question.dwh_content_id=question.dwh_contains_id 
   WHERE contenu_question.tags LIKE '%<r>%'
     AND reponse1.rank = 1
     AND reponse2.rank = 2
"""
)
rows = cur.fetchall()
for i in rows:
    data["Second best answer score"].append(i[0])
    data["Best answer score"].append(i[1])
    data["Community"].append("R")


cur.execute("""
  SELECT
    reponse2.score score_reponse2,
    reponse1.score score_reponse1
    FROM dwh_contributes reponse1
         JOIN dwh_content contenu_question
             ON reponse1.dwh_reply_id = contenu_question.dwh_content_id
         JOIN dwh_contributes reponse2 
             ON reponse2.dwh_reply_id = contenu_question.dwh_content_id
         JOIN dwh_contributes question
             ON contenu_question.dwh_content_id=question.dwh_contains_id 
   WHERE contenu_question.tags LIKE '%<pandas>%'
     AND reponse1.rank = 1
     AND reponse2.rank = 2
"""
)
rows = cur.fetchall()
for i in rows:
    data["Second best answer score"].append(i[0])
    data["Best answer score"].append(i[1])
    data["Community"].append("Pandas")

abstractData={"x" : [], "y" : [], "Community" : []}

for i in range(1,2500):
    abstractData["x"].append(i)
    abstractData["y"].append(i / 2.0 )
    abstractData["Community"].append("s2(r) = s1(q)/2")

plot = sns.scatterplot(y="Second best answer score", x="Best answer score", hue="Community", data=data)#size="Number of answers"
plot = sns.lineplot(x="x", y="y", data=abstractData, hue="Community")#size="Number of answers",


plot.set(title = 'Second best answer score Vs. Best answer score')

fig=plot.get_figure()
fig.savefig("figures/scoreSecondeAnswerVsScoreBestAnswer.png", format="png")
cur.close()
conn.close()


