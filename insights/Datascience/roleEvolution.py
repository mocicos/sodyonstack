
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Données pour R
data_r = {
    'year': [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019],
    'percentage_camper_r': [53.22, 55.84, 58.54, 55.83, 55.09, 51.57, 49.64, 48.56, 46.00, 46.32, 45.26],
    'percentage_adviser_r': [57.43, 49.48, 34.99, 30.06, 27.20, 27.61, 28.94, 28.47, 28.58, 27.50, 27.36],
    'percentage_expert_r': [64.23, 52.43, 42.48, 36.32, 31.62, 28.88, 26.79, 25.37, 24.12, 22.90, 21.79]
}

# Données pour Pandas
data_pandas = {
    'year': [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019],
    'percentage_camper_pandas': [48.93, 49.06, 47.62, 47.72, 47.34, 47.20, 46.79, 46.01],
    'percentage_adviser_pandas': [35.05, 33.10, 36.07, 37.42, 35.35, 34.00, 34.61, 36.22],
    'percentage_expert_pandas': [36.91, 33.09, 32.27, 31.31, 29.19, 27.28, 25.88, 25.03]
}

# Créer les DataFrames
df_r = pd.DataFrame(data_r)
df_pandas = pd.DataFrame(data_pandas)

# Création du graphique combiné
plt.figure(figsize=(12, 6))

# Tracés pour R (Indigo Inclusif)
sns.lineplot(data=df_r, x='year', y='percentage_camper_r', label='% campeurs (R)', linestyle='solid', color='#5862ed')
sns.lineplot(data=df_r, x='year', y='percentage_adviser_r', label='% répondants (R)', linestyle='dashed', color='#5862ed')
sns.lineplot(data=df_r, x='year', y='percentage_expert_r', label='% experts (R)', linestyle='dotted', color='#5862ed')

# Tracés pour Pandas (Vert Avenir)
sns.lineplot(data=df_pandas, x='year', y='percentage_camper_pandas', label='% campeurs (Pandas)', linestyle='solid', color='#32a68c')
sns.lineplot(data=df_pandas, x='year', y='percentage_adviser_pandas', label='% répondants (Pandas)', linestyle='dashed', color='#32a68c')
sns.lineplot(data=df_pandas, x='year', y='percentage_expert_pandas', label='% experts (Pandas)', linestyle='dotted', color='#32a68c')


# Personnalisation avec augmentation de la taille des polices
plt.xlabel('Année', fontsize=16)
plt.ylabel('Pourcentage', fontsize=16)
plt.ylim(0, 100)  # Limite de l'axe Y (0 à 100%)
plt.xlim(2009, 2019)  # Limite de l'axe X
#plt.title("Évolution des rôles en R et Pandas", fontsize=16)
plt.legend(title='Catégories', title_fontsize=16, fontsize=14)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.grid(True)

# Sauvegarde de la figure
plt.savefig("figures/roleEvolution.png", format="png")
plt.close()
