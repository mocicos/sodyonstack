# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Time" : [], "Monthly accepted answer delay (hours)" : [], "Community" : []}

cur.execute("""
        SELECT TO_DATE(CONCAT(annee, '/', mois), 'YYYY/MM'), score
          FROM
            (
              SELECT dwh_date.year AS annee,
                     dwh_date.numberOfMonth AS mois,
                     AVG(delay/60/60) score
                FROM dwh_contributes
                     JOIN dwh_date
                         ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
                     JOIN dwh_content c
                         ON dwh_contributes.dwh_reply_id = c.dwh_content_id
               WHERE c.tags LIKE '%<r>%'
                 AND dwh_contributes.dwh_kind_of_id = 2
                 AND dwh_date.date < '2020-10-15'
                 AND dwh_contributes.is_accepted
               GROUP BY year, numberOfMonth
               ORDER BY year, numberOfMonth
            ) T;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Monthly accepted answer delay (hours)"].append(i[1])
    data["Community"].append("R")


cur.execute("""
        SELECT TO_DATE(CONCAT(annee, '/', mois), 'YYYY/MM'), score
          FROM
            (
              SELECT dwh_date.year AS annee,
                     dwh_date.numberOfMonth AS mois,
                     AVG(delay/60/60) score
                FROM dwh_contributes
                     JOIN dwh_date
                         ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
                     JOIN dwh_content c
                         ON dwh_contributes.dwh_reply_id = c.dwh_content_id
               WHERE c.tags LIKE '%<pandas>%'
                 AND dwh_contributes.dwh_kind_of_id = 2
                 AND dwh_date.date < '2020-10-15'
                 AND dwh_contributes.is_accepted
               GROUP BY year, numberOfMonth
               ORDER BY year, numberOfMonth
            ) T; 
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Monthly accepted answer delay (hours)"].append(i[1])
    data["Community"].append("Pandas")

plot = sns.lineplot(x="Time", y="Monthly accepted answer delay (hours)", hue="Community", data=data)
plot.set(title = 'Monthly accepted answer delay (hours)')
fig=plot.get_figure()
fig.savefig("figures/monthlyMeanDelayAcceptedAnswer.svg", format="svg")
cur.close()
conn.close()


