# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import pandas as pd

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Score" : [], "Community" : [], "Number of views" : [], "Number of answers" : []}

cur.execute("""
  SELECT
    question.score AS score,
    question.view_count /TRUNC(DATE_PART('day', '2020-12-06'::timestamp - date_question.date::timestamp)/7) AS traffic
    FROM dwh_contributes question
         INNER JOIN dwh_content contenu_question
         ON contenu_question.dwh_content_id=question.dwh_contains_id
         JOIN dwh_date date_question
         ON question.dwh_creation_date_id = date_question.dwh_date_id
   WHERE contenu_question.tags LIKE '%<r>%'
     AND date_question.date < '2020-03-01'
   ORDER BY score
"""
)
rows = cur.fetchall()
for i in rows:
    data["Score"].append(i[0])
    data["Number of views"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Community"].append("R")


cur.execute("""
  SELECT
    question.score AS score,
    question.view_count /TRUNC(DATE_PART('day', '2020-12-06'::timestamp - date_question.date::timestamp)/7) AS traffic
    FROM dwh_contributes question
         INNER JOIN dwh_content contenu_question
         ON contenu_question.dwh_content_id=question.dwh_contains_id
         JOIN dwh_date date_question
         ON question.dwh_creation_date_id = date_question.dwh_date_id
   WHERE contenu_question.tags LIKE '%<pandas>%'
     AND date_question.date < '2020-03-01'
   ORDER BY score
"""
)
rows = cur.fetchall()
for i in rows:
    data["Score"].append(i[0])
    data["Number of views"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Community"].append("Pandas")

plot = sns.scatterplot(x="Score", y="Number of views", size="Number of answers", hue="Community", data=data)

plot.set(title = 'Score Vs. Number of views')

fig=plot.get_figure()
fig.savefig("figures/scoreVsnbViews.png", format="png")
cur.close()
conn.close()


