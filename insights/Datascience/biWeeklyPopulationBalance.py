# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()
data={"Time" : [], "Bi-weekly population balance" : [], "Community" : []}

cur.execute("""
  SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1),'YYYY/MM/DD') AS timestep,
         SUM(entrance) - SUM(exit) AS population_balance
    FROM(
      SELECT EXTRACT(YEAR FROM first_post_date) AS annee,
             EXTRACT(MONTH FROM first_post_date) AS mois,
             CAST(EXTRACT(DAY FROM first_post_date)AS INT) /16  AS quinzaine,
             COUNT(*) as entrance,
             0 as exit
        FROM dwh_membership
       WHERE dwh_membership.dwh_topic_id = 1
         AND first_post_date IS NOT NULL
       GROUP BY annee, mois, quinzaine
       UNION
      SELECT EXTRACT(YEAR FROM last_post_date) AS annee,
           EXTRACT(MONTH FROM last_post_date) AS mois,
           CAST(EXTRACT(DAY FROM last_post_date)AS INT) /16  AS quinzaine,
             0 as entrance,
             COUNT(*) as exit
      FROM dwh_membership
     WHERE dwh_membership.dwh_topic_id = 1
       AND last_post_date IS NOT NULL
       GROUP BY annee, mois, quinzaine
    ) T
   GROUP BY timestep
   ORDER BY timestep;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly population balance"].append(i[1])
    data["Community"].append("R")


cur.execute("""
  SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1),'YYYY/MM/DD') AS timestep,
         SUM(entrance) - SUM(exit) AS population_balance
    FROM(
      SELECT EXTRACT(YEAR FROM first_post_date) AS annee,
             EXTRACT(MONTH FROM first_post_date) AS mois,
             CAST(EXTRACT(DAY FROM first_post_date)AS INT) /16  AS quinzaine,
             COUNT(*) as entrance,
             0 as exit
        FROM dwh_membership
       WHERE dwh_membership.dwh_topic_id = 2
         AND first_post_date IS NOT NULL
       GROUP BY annee, mois, quinzaine
       UNION
      SELECT EXTRACT(YEAR FROM last_post_date) AS annee,
           EXTRACT(MONTH FROM last_post_date) AS mois,
           CAST(EXTRACT(DAY FROM last_post_date)AS INT) /16  AS quinzaine,
             0 as entrance,
             COUNT(*) as exit
      FROM dwh_membership
     WHERE dwh_membership.dwh_topic_id = 2
       AND last_post_date IS NOT NULL
       GROUP BY annee, mois, quinzaine
    ) T
   GROUP BY timestep
   ORDER BY timestep;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly population balance"].append(i[1])
    data["Community"].append("Pandas")

plot = sns.lineplot(x="Time", y="Bi-weekly population balance", hue="Community", data=data)
plot.set(title = 'Bi-Weekly population balance')
fig=plot.get_figure()
fig.savefig("figures/biWeeklyPopulationBalance.svg", format="svg")
cur.close()
conn.close()


