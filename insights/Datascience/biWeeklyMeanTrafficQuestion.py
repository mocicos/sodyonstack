# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Time" : [], "Bi-weekly mean question traffic (nbViewsPerWeek)" : [], "Community" : []}

cur.execute("""
  SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD'),
         nb_views / TRUNC(DATE_PART('day', '2020-12-06'::timestamp - TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD')::timestamp)/7) AS traffic
    FROM
      (
        SELECT dwh_date.year AS annee,
               dwh_date.numberOfMonth AS mois,
               dwh_date.numberOfDay /16 AS quinzaine,
               AVG(view_count) AS nb_views
          FROM dwh_contributes
               JOIN dwh_date
                   ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
               JOIN dwh_content c
                   ON dwh_contributes.dwh_contains_id = c.dwh_content_id
         WHERE c.tags LIKE '%<r>%'
           AND dwh_contributes.dwh_kind_of_id = 1
           AND dwh_date.date < '2020-10-15'
         GROUP BY year, numberOfMonth, quinzaine
         ORDER BY year, numberOfMonth, quinzaine
      ) T;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly mean question traffic (nbViewsPerWeek)"].append(i[1])
    data["Community"].append("R")


cur.execute("""
SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD'), score
          FROM
            (
              SELECT dwh_date.year AS annee,
                     dwh_date.numberOfMonth AS mois,
                     dwh_date.numberOfDay /16 AS quinzaine,
                     AVG(SCORE) score
                FROM dwh_contributes
                     JOIN dwh_date
                         ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
                     JOIN dwh_content c
                         ON dwh_contributes.dwh_contains_id = c.dwh_content_id
               WHERE c.tags LIKE '%<pandas>%'
                 AND dwh_contributes.dwh_kind_of_id = 1
                 AND dwh_date.date < '2020-10-15'
               GROUP BY year, numberOfMonth, quinzaine
               ORDER BY year, numberOfMonth, quinzaine
            ) T;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly mean question traffic (nbViewsPerWeek)"].append(i[1])
    data["Community"].append("Pandas")

plot = sns.lineplot(x="Time", y="Bi-weekly mean question traffic (nbViewsPerWeek)", hue="Community", data=data)
plot.set(title = 'Bi-weekly mean question traffic')
fig=plot.get_figure()
fig.savefig("figures/biWeeklyMeanTrafficQuestion.svg", format="svg")
cur.close()
conn.close()


