# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import pandas as pd

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Question score" : [], "Community" : [], "Position (in time)" : [], "Number of answers" : [], "Limit" : []}

cur.execute("""
  SELECT
    rank() OVER (ORDER BY date_question.date ASC) AS position,
    question.score AS score,
    question.answer_count
    FROM dwh_contributes question
         INNER JOIN dwh_content contenu_question
         ON contenu_question.dwh_content_id=question.dwh_contains_id
         JOIN dwh_date date_question
         ON question.dwh_creation_date_id = date_question.dwh_date_id
   WHERE contenu_question.tags LIKE '%<r>%'
     AND date_question.date < '2020-03-01'
   ORDER BY position
"""
)
rows = cur.fetchall()
for i in rows:
    data["Position (in time)"].append(i[0])
    data["Question score"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Community"].append("R")


cur.execute("""
  SELECT
    rank() OVER (ORDER BY date_question.date ASC) AS position,
    question.score AS score,
    question.answer_count
    FROM dwh_contributes question
         INNER JOIN dwh_content contenu_question
         ON contenu_question.dwh_content_id=question.dwh_contains_id
         JOIN dwh_date date_question
         ON question.dwh_creation_date_id = date_question.dwh_date_id
   WHERE contenu_question.tags LIKE '%<pandas>%'
     AND date_question.date < '2020-03-01'
   ORDER BY position
"""
)
rows = cur.fetchall()
for i in rows:
    data["Position (in time)"].append(i[0])
    data["Question score"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Limit"].append(2500 * (i[0]) ** (-.25))
    data["Community"].append("Pandas")

abstractData={"x" : [], "y" : [], "Community" : []}

for i in range(1,320000):
    abstractData["x"].append(i)
    abstractData["y"].append(2500 * (i ** (-.3)))
    abstractData["Community"].append("ABM4DCoP")

plot = sns.scatterplot(x="Position (in time)", y="Question score", hue="Community", data=data)#size="Number of answers",
plot = sns.lineplot(x="x", y="y", data=abstractData, hue="Community")#size="Number of answers",


plot.set(title = 'Question score Vs. Position (in time)')
#plot.set(xscale="log")
fig=plot.get_figure()
fig.savefig("figures/scoreQuestionVsPosition.png", format="png")
cur.close()
conn.close()


