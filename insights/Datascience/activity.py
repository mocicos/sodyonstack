# Copyright Maxime MORGE, 2024
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import numpy as np

#Connection to the database
USER="morge"
DATABASE="rallstar"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
SELECT rank() OVER (ORDER BY EXTRACT(EPOCH FROM AGE(last_post_date, first_post_date)) ASC, dwh_member_id) AS Position,
EXTRACT(EPOCH FROM AGE(last_post_date, first_post_date)) / 88400 AS seniority
FROM DWH_Membership
WHERE dwh_topic_id = 1
AND nb_contributions >=10
ORDER BY seniority ASC;
"""
)
data = {"User (with more than 10 contributions) rank over activity period" : [], "Activity period (days)" : [], "Community" : []}
rows = cur.fetchall()

for i in rows:
     data["User (with more than 10 contributions) rank over activity period"].append(i[0])
     data["Activity period (days)"].append(i[1])
     data["Community"].append("R")


cur.execute("""
SELECT rank() OVER (ORDER BY EXTRACT(EPOCH FROM AGE(last_post_date, first_post_date)) ASC, dwh_member_id) AS Position,
EXTRACT(EPOCH FROM AGE(last_post_date, first_post_date)) / 88400 AS seniority
FROM DWH_Membership
WHERE dwh_topic_id = 2
AND nb_contributions >=10
ORDER BY seniority ASC;
""")
rows = cur.fetchall()
for i in rows:
    data["User (with more than 10 contributions) rank over activity period"].append(i[0])
    data["Activity period (days)"].append(i[1])
    data["Community"].append("Pandas")

plot = sns.lineplot(x="User (with more than 10 contributions) rank over activity period", y="Activity period (days)", hue="Community", data=data)
pal = sns.color_palette()

cur.execute("""
SELECT AVG(EXTRACT(EPOCH FROM AGE(last_post_date, first_post_date)) / 88400)
FROM DWH_Membership
WHERE dwh_topic_id = 1
AND nb_contributions >=10;
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[0])

cur.execute("""
SELECT AVG(EXTRACT(EPOCH FROM AGE(last_post_date, first_post_date)) / 88400)
FROM DWH_Membership
WHERE dwh_topic_id = 2
AND nb_contributions >=10;
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[1])
#plot.set(xscale="log")
#plot.set(yscale="log")
plot.set(title = 'Activity period of users')
fig=plot.get_figure()
fig.savefig("figures/activity.svg", format="svg")
cur.close()
conn.close()
