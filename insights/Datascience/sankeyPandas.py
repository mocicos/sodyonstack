import plotly.graph_objects as go

# Données
sources = [
    "2012", "2013", "2014", "2015", "2016", "2017", "2018", 
    "incoming", "incoming", "incoming", "incoming", "incoming", "incoming", "incoming",
    "2012", "2013", "2014", "2015", "2016", "2017", "2018"
]
targets = [
    "2013", "2014", "2015", "2016", "2017", "2018", "2019", 
    "2013", "2014", "2015", "2016", "2017", "2018", "2019",
    "outcoming", "outcoming", "outcoming", "outcoming", "outcoming", "outcoming", "outcoming"
]
values = [
    227, 588, 1081, 1803, 2852, 4214, 5549, 
    1522, 2675, 4732, 7674, 12288, 17378, 22264, 
    342, 1161, 2182, 4010, 6625, 10926, 16043
]

# Palette de couleurs
color_palette = {
    "incoming": "rgba(50, 166, 140, 0.2)",  # vertAvenir
    "outcoming": "rgba(252, 83, 92, 0.2)",  # rougeAction
    "default": "rgba(86, 98, 237, 01)"     # indigoInclusif
}

# Création des couleurs des liens
link_colors = []
for source, target in zip(sources, targets):
    if source == "incoming":
        link_colors.append(color_palette["incoming"])  # Flux provenant de "incoming"
    elif target == "outcoming":
        link_colors.append(color_palette["outcoming"])  # Flux menant à "outcoming"
    else:
        link_colors.append(color_palette["default"])  # Flux par défaut

# Création des nœuds uniques
nodes = list(set(sources + targets))
node_indices = {node: i for i, node in enumerate(nodes)}

# Conversion des labels en indices
source_indices = [node_indices[source] for source in sources]
target_indices = [node_indices[target] for target in targets]

# Création du Sankey
fig = go.Figure(data=[go.Sankey(
    node=dict(
        pad=15,
        thickness=20,
        line=dict(color="black", width=0.5),
        label=nodes
    ),
    link=dict(
        source=source_indices,
        target=target_indices,
        value=values,
        color=link_colors
    )
)])

# Mise en forme
fig.update_layout(
    font_size=10
)#     title_text="Diagramme de Sankey avec Mise en Évidence des Flux",

# Enregistrement en PNG
fig.write_image("figures/sankeyPandas.png", format="png")

