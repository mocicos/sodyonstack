# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import pandas as pd

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Answer score" : [], "Community" : [], "Position (in time)" : [], "Limit" : []}

cur.execute("""
  SELECT
    rank() OVER (ORDER BY date_answer.date ASC) AS position,
    answer.score AS score
    FROM dwh_contributes answer
         INNER JOIN dwh_content contenu_question
         ON contenu_question.dwh_content_id=answer.dwh_reply_id
         JOIN dwh_date date_answer
         ON answer.dwh_creation_date_id = date_answer.dwh_date_id
   WHERE contenu_question.tags LIKE '%<r>%'
     AND date_answer.date < '2020-03-01'
   ORDER BY position
"""
)
rows = cur.fetchall()
for i in rows:
    data["Position (in time)"].append(i[0])
    data["Answer score"].append(i[1])
    data["Community"].append("R")


cur.execute("""
  SELECT
    rank() OVER (ORDER BY date_answer.date ASC) AS position,
    answer.score AS score
    FROM dwh_contributes answer
         INNER JOIN dwh_content contenu_question
         ON contenu_question.dwh_content_id=answer.dwh_reply_id
         JOIN dwh_date date_answer
         ON answer.dwh_creation_date_id = date_answer.dwh_date_id
   WHERE contenu_question.tags LIKE '%<pandas>%'
     AND date_answer.date < '2020-03-01'
   ORDER BY position
"""
)
rows = cur.fetchall()
for i in rows:
    data["Position (in time)"].append(i[0])
    data["Answer score"].append(i[1])
    data["Community"].append("Pandas")

plot = sns.scatterplot(x="Position (in time)", y="Answer score", hue="Community", data=data)


plot.set(title = 'Answer score Vs. Position (in time)')
fig=plot.get_figure()
fig.savefig("figures/scoreAnswerVsPosition.png", format="png")
cur.close()
conn.close()


