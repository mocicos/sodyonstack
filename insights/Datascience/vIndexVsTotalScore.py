# Copyright Maxime MORGE, 2025
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import pandas as pd

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"V-index" : [], "Community" : [], "Total score" : []}

cur.execute("""
    SELECT
        DWH_Membership.dwh_member_id,
        DWH_Membership.v_index,
        DWH_Membership.total_score
    FROM DWH_Membership
      INNER JOIN DWH_COP
        ON DWH_COP.dwh_topic_id = DWH_Membership.dwh_topic_id
  WHERE DWH_COP.topic = 'r'
"""
)
rows = cur.fetchall()
for i in rows:
    data["V-index"].append(i[1])
    data["Total score"].append(i[2])
    data["Community"].append("R")


cur.execute("""
    SELECT
        DWH_Membership.dwh_member_id,
        DWH_Membership.v_index,
        DWH_Membership.total_score
    FROM DWH_Membership
      INNER JOIN DWH_COP
        ON DWH_COP.dwh_topic_id = DWH_Membership.dwh_topic_id
  WHERE DWH_COP.topic = 'pandas';
"""
)
rows = cur.fetchall()
for i in rows:
    data["V-index"].append(i[1])
    data["Total score"].append(i[2])
    data["Community"].append("Pandas")

# for i in range(1,8000):
#     abstractData["x"].append(i)
#     abstractData["y"].append(i / 4)
#     abstractData["Community"].append("s(q) = t/4")

plot = sns.scatterplot(y="V-index", x="Total score", hue="Community", data=data)#size="Number of answers",
# plot = sns.lineplot(x="x", y="y", data=abstractData, hue="Community")#size="Number of answers",

plot.set(title = 'V-index Vs. Total score')

fig=plot.get_figure()
fig.savefig("figures/vIndexVsTotalScore.png", format="png")
cur.close()
conn.close()


