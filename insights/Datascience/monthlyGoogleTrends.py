# Copyright Maxime MORGER, 2023
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

file_path = '../../data/GoogleTrends/googleTrendRvsPandas.csv'  
df = pd.read_csv(file_path, index_col='Month', parse_dates=True)

# Data from
# https://trends.google.fr/trends/explore?date=2008-01-01%202020-01-02&q=R%20tutorial,Pandas%20tutorial&hl=en
# The methodology is similar to https://pypl.github.io/PYPL.html
# An alternative way could be used https://www.gharchive.org/

# Use Seaborn for datavizualisation
# sns.set(style="whitegrid")
plt.figure(figsize=(10, 6))
# Tracez les courbes avec Seaborn
#sns.lineplot(data=df[['R', 'Pandas']], markers=False, dashes=False)
sns.lineplot(x=df.index, y='R', data=df, markers=True, dashes=False, label='R')
sns.lineplot(x=df.index, y='Pandas', data=df, markers=True, dashes=False, label='Pandas')
plt.title('Google search interest relative to the highest point on the chart for the given time')
plt.xlabel('Month')
plt.ylabel('Interest over time')
plt.legend()
plt.savefig("figures/monthlyGoogleTrends.svg", format="svg")


