# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Year" : [], "Number of questions in the top 100" : [], "Community" : []}

cur.execute("""
  WITH
    Period AS (SELECT generate_series AS year FROM generate_series(2008,2020)),
    PositionYear AS(
    SELECT
      rank() OVER (ORDER BY dwh_contributes.score DESC) AS position,
      d.year AS year_of_question
      FROM
        dwh_contributes 
        JOIN dwh_date d
            ON dwh_contributes.dwh_creation_date_id = d.dwh_date_id
        JOIN dwh_content c
            ON dwh_contributes.dwh_contains_id = c.dwh_content_id
     WHERE c.tags LIKE '%<r>%'
       AND dwh_contributes.dwh_kind_of_id = 1 -- Question
     ORDER BY dwh_contributes.score DESC
     LIMIT 100
  )
  SELECT period.year, COUNT(year_of_question) AS number_of_topquestions
  FROM PositionYear
       RIGHT JOIN period
          ON period.year >= PositionYear.year_of_question
  GROUP BY period.year
  ORDER BY period.year
"""
)
rows = cur.fetchall()
for i in rows:
    data["Year"].append(i[0])
    data["Number of questions in the top 100"].append(i[1])
    data["Community"].append("R")


cur.execute("""
  WITH
    Period AS (SELECT generate_series AS year FROM generate_series(2008,2020)),
    PositionYear AS(
    SELECT
      rank() OVER (ORDER BY dwh_contributes.score DESC) AS position,
      d.year AS year_of_question
      FROM
        dwh_contributes 
        JOIN dwh_date d
            ON dwh_contributes.dwh_creation_date_id = d.dwh_date_id
        JOIN dwh_content c
            ON dwh_contributes.dwh_contains_id = c.dwh_content_id
     WHERE c.tags LIKE '%<pandas>%'
       AND dwh_contributes.dwh_kind_of_id = 1 -- Question
     ORDER BY dwh_contributes.score DESC
     LIMIT 100
  )
  SELECT period.year, COUNT(year_of_question) AS number_of_topquestions
  FROM PositionYear
       RIGHT JOIN period
          ON period.year >= PositionYear.year_of_question
  GROUP BY period.year
  ORDER BY period.year
"""
)
rows = cur.fetchall()
for i in rows:
    data["Year"].append(i[0])
    data["Number of questions in the top 100"].append(i[1])
    data["Community"].append("Pandas")

plot = sns.barplot(x="Year", y="Number of questions in the top 100", hue="Community", data=data)
plot.set(title = 'Number of questions in the top 100')
fig=plot.get_figure()
fig.savefig("figures/nbTopQuestionsPerYear.svg", format="svg")
cur.close()
conn.close()


