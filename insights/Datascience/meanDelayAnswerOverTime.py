# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Time" : [], "Answer delay (weeks)" : [], "Community" : []}

cur.execute("""
  SELECT mois, AVG(delay)
  FROM
    (
  SELECT
    TO_DATE(CONCAT(question_date.year, '/', question_date.numberOfMonth, '/', 1), 'YYYY/MM/DD') AS mois,
    question.dwh_contains_id,
      AVG(reponse.delay/60/60/24/7) AS delay
      FROM dwh_contributes reponse
           JOIN dwh_content question_content
               ON reponse.dwh_reply_id = question_content.dwh_content_id
           JOIN dwh_contributes question
               ON question.dwh_contains_id = question_content.dwh_content_id
           JOIN dwh_date question_date
               ON question.dwh_creation_date_id = question_date.dwh_date_id
     WHERE question_content.tags LIKE '%<r>%'
       AND reponse.dwh_kind_of_id = 2
       AND question.answer_count != 0
       AND question_date.date < '2020-10-15'
     GROUP BY question.dwh_contains_id, mois 
     ORDER BY question.dwh_contains_id, mois
         ) T
   GROUP BY mois
   ORDER BY mois
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Answer delay (weeks)"].append(i[1])
    data["Community"].append("R")


cur.execute("""
  SELECT mois, AVG(delay)
  FROM
    (
  SELECT
    TO_DATE(CONCAT(question_date.year, '/', question_date.numberOfMonth, '/', 1), 'YYYY/MM/DD') AS mois,
    question.dwh_contains_id,
      AVG(reponse.delay/60/60/24/7) AS delay
      FROM dwh_contributes reponse
           JOIN dwh_content question_content
               ON reponse.dwh_reply_id = question_content.dwh_content_id
           JOIN dwh_contributes question
               ON question.dwh_contains_id = question_content.dwh_content_id
           JOIN dwh_date question_date
               ON question.dwh_creation_date_id = question_date.dwh_date_id
     WHERE question_content.tags LIKE '%<pandas>%'
       AND reponse.dwh_kind_of_id = 2
       AND question.answer_count != 0
       AND question_date.date < '2020-10-15'
     GROUP BY question.dwh_contains_id, mois 
     ORDER BY question.dwh_contains_id, mois
         ) T
   GROUP BY mois
   ORDER BY mois
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Answer delay (weeks)"].append(i[1])
    data["Community"].append("Pandas")

plot = sns.lineplot(x="Time", y="Answer delay (weeks)", hue="Community", data=data)
plot.set(title = 'Answer delay (weeks)')
fig=plot.get_figure()
fig.savefig("figures/meanDelayAnswer.svg", format="svg")
cur.close()
conn.close()


