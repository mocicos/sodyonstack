# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import pandas as pd

#Connection to the database
USER="morge"
DATABASE="rallstar"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Question score" : [], "Community" : [], "Best answer score" : [], "Number of answers" : []}

cur.execute("""
  SELECT
    question.score score_question,
    reponse.score score_reponse,
    question.answer_count answer_count 
    FROM dwh_contributes reponse
         JOIN dwh_date date_reponse
             ON reponse.dwh_creation_date_id = date_reponse.dwh_date_id
         JOIN dwh_content contenu_question
             ON reponse.dwh_reply_id = contenu_question.dwh_content_id
         JOIN dwh_contributes question
             ON contenu_question.dwh_content_id=question.dwh_contains_id 
   WHERE contenu_question.tags LIKE '%<r>%'
     AND reponse.rank = 1
"""
)
rows = cur.fetchall()
for i in rows:
    data["Question score"].append(i[0])
    data["Best answer score"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Community"].append("R")


cur.execute("""
  SELECT
    question.score score_question,
    reponse.score score_reponse,
    question.answer_count answer_count 
    FROM dwh_contributes reponse
         JOIN dwh_date date_reponse
             ON reponse.dwh_creation_date_id = date_reponse.dwh_date_id
         JOIN dwh_content contenu_question
             ON reponse.dwh_reply_id = contenu_question.dwh_content_id
         JOIN dwh_contributes question
             ON contenu_question.dwh_content_id=question.dwh_contains_id 
   WHERE contenu_question.tags LIKE '%<pandas>%'
     AND reponse.rank = 1
"""
)
rows = cur.fetchall()
for i in rows:
    data["Question score"].append(i[0])
    data["Best answer score"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Community"].append("Pandas")

abstractData={"x" : [], "y" : [], "Community" : []}

for i in range(1,2500):
    abstractData["x"].append(i)
    abstractData["y"].append(i * 1.5)
    abstractData["Community"].append("s(r) = 1.5 s(q)")

plot = sns.scatterplot(x="Question score", y="Best answer score", hue="Community", data=data)#size="Number of answers"
plot = sns.lineplot(x="x", y="y", data=abstractData, hue="Community")#size="Number of answers",


plot.set(title = 'Question score Vs. Best answer score')

fig=plot.get_figure()
fig.savefig("figures/scoreQuestionVsScoreBestAnswer.png", format="png")
cur.close()
conn.close()


