// Copyright (C) Maxime MORGE, 2021
import $ivy.`com.lihaoyi::os-lib:0.7.1`

val debug = true

val fieldSeparator = ";"
val workingDirectory = os.pwd
val userCsvFile = os.pwd/"users.csv"
val networkCSVFile = os.pwd/"advisorNetwork.csv"
val networkGVFile = os.pwd/"advisorNetwork.dot"
val extension = "svg" //alternatively png
val overlap = "prism"

val head = s"""/*
   |dot -Ksfdp -Goverlap=prism -oadvisorNetwork.$extension -T$extension advisorNetwork.dot
   |*/
   |digraph G {
    """.stripMargin
val tail = "}"

/**
 * Class representing a User with an id and a name
 */
case class User(val userId : Int, val username: String){
  override def toString: String = s"""\t$userId [label="$username", shape=${User.shape}, style=${User.style}, color=${User.color}];\n"""
}

/**
 * Companion object
 * See https://graphviz.org/doc/info/shapes.html
 */
object User{
  val shape = "plaintext"
  val style = "filled"
  val color = "blue"
}

val users : Seq[User] =  os.read.lines(userCsvFile).drop(1).map{ line =>
  val array = line.split(fieldSeparator).map(_.trim)
  User(array(0).toInt, array(1))
}


/**
 * Class representing an Edge with an advisor, an inquirer
 * which could accepted a date and a delay
 */
case class Edge(val advisor : Int, val inquirer : Int,
val accepted :Boolean, date : String, delay : BigInt){
  def color : String = if (accepted)  Edge.colorAccepted
  else Edge.colorNotAccepted
  override def toString: String = s"""\t$advisor -> $inquirer [label="$date", color=${color}, arrowhead=${Edge.arrowhead}];\n"""
}
/**
 * Companion object
 * See https://graphviz.org/doc/info/arrows.html
 */
object Edge{
  val arrowhead = "normal"
  val colorAccepted = "green"
  val colorNotAccepted = "red"
}

val edges : Seq[Edge] =  os.read.lines(networkCSVFile).drop(1).map{ line =>
  val array = line.split(fieldSeparator).map(_.trim)
  Edge(array(0).toInt, array(1).toInt,
    (array(2) == "t"), array(3), array(4).toInt)
}

if (os.exists(networkGVFile)) os.remove(networkGVFile)
os.write(networkGVFile, head)
users.foreach( user => os.write.append(networkGVFile, user.toString))
edges.foreach( edge => os.write.append(networkGVFile, edge.toString))
os.write.append(networkGVFile, tail)
