  set terminal svg
  set output 'nbContributionsPerMonth.svg'
  set style data histograms
  bleuSombreFroid = "#1d428a";
  set style histogram rowstacked
  set boxwidth 0.5
  set xlabel "Month"
  set ylabel "Number of contributions"  
  set style fill solid
  set xtics 100 rotate by 45 right nomirror
  plot "nbContributionsPerMonth.tsv" using 2:xticlabels(int($0)%10== 0 ? stringcolumn(1) : '') notitle linecolor rgb bleuSombreFroid 
