# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()
data={"Time" : [], "Bi-weekly number of exits" : [], "Community" : []}

cur.execute("""
  SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1),'YYYY/MM/DD') AS timestep,
         SUM(entrance)
    FROM(
      SELECT EXTRACT(YEAR FROM last_post_date) AS annee,
             EXTRACT(MONTH FROM last_post_date) AS mois,
             CAST(EXTRACT(DAY FROM last_post_date)AS INT) /16  AS quinzaine,
             1 as entrance
        FROM dwh_membership
       WHERE dwh_membership.dwh_topic_id = 2
         AND last_post_date IS NOT NULL
    ) T
   GROUP BY timestep
   ORDER BY timestep;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly number of exits"].append(i[1])
    data["Community"].append("hive")

cur.execute("""
  SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1),'YYYY/MM/DD') AS timestep,
         SUM(entrance)
    FROM(
      SELECT EXTRACT(YEAR FROM last_post_date) AS annee,
             EXTRACT(MONTH FROM last_post_date) AS mois,
             CAST(EXTRACT(DAY FROM last_post_date)AS INT) /16  AS quinzaine,
             1 as entrance
        FROM dwh_membership
       WHERE dwh_membership.dwh_topic_id = 3
         AND last_post_date IS NOT NULL
    ) T
   GROUP BY timestep
   ORDER BY timestep;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly number of exits"].append(i[1])
    data["Community"].append("pig")

plot = sns.lineplot(x="Time", y="Bi-weekly number of exits", hue="Community", data=data)
plot.set(title = 'Bi-Weekly number of exits')
fig=plot.get_figure()
fig.savefig("figures/biWeeklyNbExitsPigVsHive.svg", format="svg")
cur.close()
conn.close()
