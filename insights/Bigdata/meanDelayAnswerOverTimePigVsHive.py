# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Time" : [], "Answer delay (weeks)" : [], "Community" : []}

cur.execute("""
SELECT dwh_date.year AS annee,
AVG(delay/60/60/24/7) score
FROM dwh_contributes
JOIN dwh_date
ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
JOIN dwh_content c
ON dwh_contributes.dwh_reply_id = c.dwh_content_id
WHERE c.tags LIKE '%<apache-pig>%'
AND dwh_contributes.dwh_kind_of_id = 2
AND dwh_date.date < '2020-10-15'
GROUP BY year
ORDER BY year;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Answer delay (weeks)"].append(i[1])
    data["Community"].append("Pig")


cur.execute("""
SELECT dwh_date.year AS annee,
AVG(delay/60/60/24/7) score
FROM dwh_contributes
JOIN dwh_date
ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
JOIN dwh_content c
ON dwh_contributes.dwh_reply_id = c.dwh_content_id
WHERE c.tags LIKE '%<hive>%'
AND dwh_contributes.dwh_kind_of_id = 2
AND dwh_date.date < '2020-10-15'
GROUP BY year
ORDER BY year;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Answer delay (weeks)"].append(i[1])
    data["Community"].append("Hive")

plot = sns.lineplot(x="Time", y="Answer delay (weeks)", hue="Community", data=data)
plot.set(title = 'Answer delay (weeks)')
fig=plot.get_figure()
fig.savefig("figures/meanDelayAnswerPigVsHive.svg", format="svg")
cur.close()
conn.close()


