# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Time" : [], "Bi-weekly accepted answer delay (hours)" : [], "Community" : []}

cur.execute("""
        SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD'), score
          FROM
            (
              SELECT dwh_date.year AS annee,
                     dwh_date.numberOfMonth AS mois,
                     dwh_date.numberOfDay /16 AS quinzaine,
                     AVG(delay/60/60) score
                FROM dwh_contributes
                     JOIN dwh_date
                         ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
                     JOIN dwh_content c
                         ON dwh_contributes.dwh_reply_id = c.dwh_content_id
               WHERE c.tags LIKE '%<hadoop>%'
                 AND dwh_contributes.dwh_kind_of_id = 2
                 AND dwh_date.date < '2020-10-15'
                 AND dwh_contributes.is_accepted
               GROUP BY year, numberOfMonth, quinzaine
               ORDER BY year, numberOfMonth, quinzaine
            ) T;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly accepted answer delay (hours)"].append(i[1])
    data["Community"].append("hadoop")


cur.execute("""
        SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD'), score
          FROM
            (
              SELECT dwh_date.year AS annee,
                     dwh_date.numberOfMonth AS mois,
                     dwh_date.numberOfDay /16 AS quinzaine,
                     AVG(delay/60/60) score
                FROM dwh_contributes
                     JOIN dwh_date
                         ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
                     JOIN dwh_content c
                         ON dwh_contributes.dwh_reply_id = c.dwh_content_id
               WHERE c.tags LIKE '%<apache-spark>%'
                 AND dwh_contributes.dwh_kind_of_id = 2
                 AND dwh_date.date < '2020-10-15'
                 AND dwh_contributes.is_accepted
               GROUP BY year, numberOfMonth, quinzaine
               ORDER BY year, numberOfMonth, quinzaine
            ) T; 
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly accepted answer delay (hours)"].append(i[1])
    data["Community"].append("spark")

plot = sns.lineplot(x="Time", y="Bi-weekly accepted answer delay (hours)", hue="Community", data=data)
plot.set(title = 'Bi-weekly accepted answer delay (hours)')
fig=plot.get_figure()
fig.savefig("figures/biWeeklyMeanDelayAcceptedAnswerHadoopVsSpark.svg", format="svg")
cur.close()
conn.close()


