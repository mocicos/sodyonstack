# Copyright Maxime MORGE, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import numpy as np

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
  SELECT rank() OVER (ORDER BY mean_score_answers ASC, dwh_member_id) AS Position, mean_score_answers 
    FROM DWH_Membership
   WHERE dwh_topic_id = 1
   ORDER BY mean_score_answers
 ASC;      
"""
)
data = {"User rank over mean score answer" : [], "Mean score answer" : [], "Community" : []}
rows = cur.fetchall()

for i in rows:
     data["User rank over mean score answer"].append(i[0])
     data["Mean score answer"].append(i[1])
     data["Community"].append("hadoop")


cur.execute("""
  SELECT rank() OVER (ORDER BY mean_score_answers ASC, dwh_member_id) AS Position, mean_score_answers 
    FROM DWH_Membership
   WHERE dwh_topic_id = 4
   ORDER BY mean_score_answers
 ASC;      
""")
rows = cur.fetchall()
for i in rows:
    data["User rank over mean score answer"].append(i[0])
    data["Mean score answer"].append(i[1])
    data["Community"].append("spark")

plot = sns.lineplot(x="User rank over mean score answer", y="Mean score answer", hue="Community", data=data)
pal = sns.color_palette()

cur.execute("""
    SELECT AVG(mean_score_answers)
      FROM DWH_Membership
     WHERE dwh_topic_id = 1;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[0])

cur.execute("""
    SELECT AVG(mean_score_answers)
      FROM DWH_Membership
     WHERE dwh_topic_id = 4;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[1])
#plot.set(xscale="log")
#plot.set(yscale="log")
plot.set(title = 'Expertise of users')
fig=plot.get_figure()
fig.savefig("figures/scoreAnswerHadoopVsSpark.svg", format="svg")
cur.close()
conn.close()
