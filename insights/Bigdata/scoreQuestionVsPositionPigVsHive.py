# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import pandas as pd

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Question score" : [], "Community" : [], "Position (in time)" : [], "Number of answers" : []}

cur.execute("""
  SELECT
    rank() OVER (ORDER BY date_question.date ASC) AS position,
    question.score AS score,
    question.answer_count
    FROM dwh_contributes question
         INNER JOIN dwh_content contenu_question
         ON contenu_question.dwh_content_id=question.dwh_contains_id
         JOIN dwh_date date_question
         ON question.dwh_creation_date_id = date_question.dwh_date_id
   WHERE contenu_question.tags LIKE '%<apache-pig>%'
     AND date_question.date < '2020-03-01'
   ORDER BY position
"""
)
rows = cur.fetchall()
for i in rows:
    data["Position (in time)"].append(i[0])
    data["Question score"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Community"].append("Pig")


cur.execute("""
  SELECT
    rank() OVER (ORDER BY date_question.date ASC) AS position,
    question.score AS score,
    question.answer_count
    FROM dwh_contributes question
         INNER JOIN dwh_content contenu_question
         ON contenu_question.dwh_content_id=question.dwh_contains_id
         JOIN dwh_date date_question
         ON question.dwh_creation_date_id = date_question.dwh_date_id
   WHERE contenu_question.tags LIKE '%<hive>%'
     AND date_question.date < '2020-03-01'
   ORDER BY position
"""
)
rows = cur.fetchall()
for i in rows:
    data["Position (in time)"].append(i[0])
    data["Question score"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Community"].append("Hive")

plot = sns.scatterplot(x="Position (in time)", y="Question score", hue="Community", data=data)#size="Number of answers",
plot.set(title = 'Question score Vs. Position (in time)')
#plot.set(xscale="log")
fig=plot.get_figure()
fig.savefig("figures/scoreQuestionVsPositionPigVsHive.png", format="png")
cur.close()
conn.close()


