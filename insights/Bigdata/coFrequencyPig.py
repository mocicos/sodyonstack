# Copyright Maxime MORGE, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import numpy as np

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
   SELECT rank() OVER (ORDER BY probability_reply ASC, probability_query ASC) AS Position, probability_reply
    FROM DWH_Membership
   WHERE dwh_topic_id = 2
   ORDER BY probability_reply ASC, probability_query ASC;         
"""
)
data = {"User rank over answers" : [], "Bi-weekly number of posts" : [], "Post type" : []}
rows = cur.fetchall()

for i in rows:
     data["User rank over answers"].append(i[0])
     data["Bi-weekly number of posts"].append(i[1])
     data["Post type"].append("Answer")


cur.execute("""
   SELECT rank() OVER (ORDER BY probability_reply ASC, probability_query ASC) AS Position, probability_query 
    FROM DWH_Membership
   WHERE dwh_topic_id = 2
   ORDER BY probability_reply ASC, probability_query ASC;      
""")
rows = cur.fetchall()
for i in rows:
    data["User rank over answers"].append(i[0])
    data["Bi-weekly number of posts"].append(i[1])
    data["Post type"].append("Question")


plot = sns.lineplot(x="User rank over answers", y="Bi-weekly number of posts", hue="Post type", data=data)
pal = sns.color_palette()

plot.set(title = 'Propensity of pig users to contribute regularly')
fig=plot.get_figure()
fig.savefig("figures/coFrequencyPig.svg", format="svg")
cur.close()
conn.close()
