# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import pandas as pd

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Question score" : [], "Community" : [], "Traffic (nb views per week)" : [], "Number of answers" : []}

cur.execute("""
  SELECT
    question.score AS score,
    question.view_count /TRUNC(DATE_PART('day', '2020-12-06'::timestamp - date_question.date::timestamp)/7) AS traffic,
    question.answer_count
    FROM dwh_contributes question
         INNER JOIN dwh_content contenu_question
         ON contenu_question.dwh_content_id=question.dwh_contains_id
         JOIN dwh_date date_question
         ON question.dwh_creation_date_id = date_question.dwh_date_id
   WHERE contenu_question.tags LIKE '%<apache-pig>%'
     AND date_question.date < '2020-03-01'
   ORDER BY score
"""
)
rows = cur.fetchall()
for i in rows:
    data["Question score"].append(i[0])
    data["Traffic (nb views per week)"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Community"].append("Pig")


cur.execute("""
  SELECT
    question.score AS score,
    question.view_count /TRUNC(DATE_PART('day', '2020-12-06'::timestamp - date_question.date::timestamp)/7) AS traffic,
    question.answer_count
    FROM dwh_contributes question
         INNER JOIN dwh_content contenu_question
         ON contenu_question.dwh_content_id=question.dwh_contains_id
         JOIN dwh_date date_question
         ON question.dwh_creation_date_id = date_question.dwh_date_id
   WHERE contenu_question.tags LIKE '%<hive>%'
     AND date_question.date < '2020-03-01'
   ORDER BY score
"""
)
rows = cur.fetchall()
for i in rows:
    data["Question score"].append(i[0])
    data["Traffic (nb views per week)"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Community"].append("Hive")

plot = sns.scatterplot(x="Question score", y="Traffic (nb views per week)", hue="Community", data=data)#size="Number of answers",
plot.set(title = 'Question score Vs. Traffic (nb views per week)')

fig=plot.get_figure()
fig.savefig("figures/scoreQuestionVsTrafficPigVsHive.png", format="png")
cur.close()
conn.close()


