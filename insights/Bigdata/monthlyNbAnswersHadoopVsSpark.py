# Copyright Maxime MORGE, 2023
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Temps" : [], "Nombre mensuel de réponses" : [], "Communauté" : []}

cur.execute("""
        SELECT TO_DATE(CONCAT(annee, '/', mois, '/'), 'YYYY/MM'), nbReponses
          FROM
            (
              SELECT dwh_date.year AS annee,
                     dwh_date.numberOfMonth AS mois,
                     COUNT(dwh_contributes.dwh_contains_id) nbReponses
                FROM dwh_contributes
                     JOIN dwh_date
                         ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
                     JOIN dwh_content c
                         ON dwh_contributes.dwh_reply_id = c.dwh_content_id
               WHERE c.tags LIKE '%<hadoop>%'
                 AND dwh_date.date < '2020-03-01'
               GROUP BY year, numberOfMonth
               ORDER BY year, numberOfMonth
            ) T;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Temps"].append(i[0])
    data["Nombre mensuel de réponses"].append(i[1])
    data["Communauté"].append("Hadoop")

cur.execute("""
        SELECT TO_DATE(CONCAT(annee, '/', mois, '/'), 'YYYY/MM'), nbReponses
          FROM
            (
              SELECT dwh_date.year AS annee,
                     dwh_date.numberOfMonth AS mois,
                     COUNT(dwh_contributes.dwh_contains_id) nbReponses
                FROM dwh_contributes
                     JOIN dwh_date
                         ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
                     JOIN dwh_content c
                         ON dwh_contributes.dwh_reply_id = c.dwh_content_id
               WHERE c.tags LIKE '%<apache-spark>%'
                 AND dwh_date.date < '2020-03-01'
               GROUP BY year, numberOfMonth
               ORDER BY year, numberOfMonth
            ) T;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Temps"].append(i[0])
    data["Nombre mensuel de réponses"].append(i[1])
    data["Communauté"].append("Spark")

plot = sns.lineplot(x="Temps", y="Nombre mensuel de réponses", hue="Communauté", data=data)

# cur.execute("""
#   WITH TOP80 AS (
#     SELECT
#       d.date AS date_of_question
#       FROM
#         dwh_contributes 
#         JOIN dwh_date d
#             ON dwh_contributes.dwh_creation_date_id = d.dwh_date_id
#         JOIN dwh_content c
#             ON dwh_contributes.dwh_contains_id = c.dwh_content_id
#      WHERE c.tags LIKE '%<hadoop>%'
#        AND dwh_contributes.dwh_kind_of_id = 1 -- Question
#      ORDER BY dwh_contributes.score DESC
#      LIMIT 100),
#     HISTORY AS(
#     SELECT date_of_question
#       FROM TOP80
#      ORDER BY date_of_question
#      LIMIT 80)
#     SELECT MAX(date_of_question)
#       FROM HISTORY
# """
# )
# result = cur.fetchone()[0]

# plot.axvline(result, linestyle = '--', color=sns.color_palette()[0])
# plot.text(result, 800, "Saturation", rotation=90, color=sns.color_palette()[0])

cur.execute("""
  WITH TOP80 AS (
    SELECT
      d.date AS date_of_answer
      FROM
        dwh_contributes 
        JOIN dwh_date d
            ON dwh_contributes.dwh_creation_date_id = d.dwh_date_id
        JOIN dwh_content c
            ON dwh_contributes.dwh_reply_id = c.dwh_content_id
     WHERE c.tags LIKE '%<hadoop>%'
       AND dwh_contributes.dwh_kind_of_id = 2 -- Answer
     ORDER BY dwh_contributes.score DESC
     LIMIT 100),
    HISTORY AS(
    SELECT date_of_answer
      FROM TOP80
     ORDER BY date_of_answer
     LIMIT 80)
    SELECT MAX(date_of_answer)
      FROM HISTORY
"""
)
result = cur.fetchone()[0]

plot.axvline(result, linestyle = '--', color=sns.color_palette()[0])
plot.text(result, 100, "Sécession", rotation=90, color=sns.color_palette()[0])

# cur.execute("""
#   WITH TOP80 AS (
#     SELECT
#       d.date AS date_of_question
#       FROM
#         dwh_contributes 
#         JOIN dwh_date d
#             ON dwh_contributes.dwh_creation_date_id = d.dwh_date_id
#         JOIN dwh_content c
#             ON dwh_contributes.dwh_contains_id = c.dwh_content_id
#      WHERE c.tags LIKE '%<apache-spark>%'
#        AND dwh_contributes.dwh_kind_of_id = 1 -- Question
#      ORDER BY dwh_contributes.score DESC
#      LIMIT 100),
#     HISTORY AS(
#     SELECT date_of_question
#       FROM TOP80
#      ORDER BY date_of_question
#      LIMIT 80)
#     SELECT MAX(date_of_question)
#       FROM HISTORY
# """
# )
# result = cur.fetchone()[0]

# plot.axvline(result, linestyle = '--', color=sns.color_palette()[1])
# plot.text(result, 800, "Saturation", rotation=90, color=sns.color_palette()[1])

cur.execute("""
  WITH TOP80 AS (
    SELECT
      d.date AS date_of_answer
      FROM
        dwh_contributes 
        JOIN dwh_date d
            ON dwh_contributes.dwh_creation_date_id = d.dwh_date_id
        JOIN dwh_content c
            ON dwh_contributes.dwh_reply_id = c.dwh_content_id
     WHERE c.tags LIKE '%<apache-spark>%'
       AND dwh_contributes.dwh_kind_of_id = 2 -- Answer
     ORDER BY dwh_contributes.score DESC
     LIMIT 100),
    HISTORY AS(
    SELECT date_of_answer
      FROM TOP80
     ORDER BY date_of_answer
     LIMIT 80)
    SELECT MAX(date_of_answer)
      FROM HISTORY
"""
)
result = cur.fetchone()[0]

plot.axvline(result, linestyle = '--', color=sns.color_palette()[1])
plot.text(result, 100, "Sécession", rotation=90, color=sns.color_palette()[1])

fig=plot.get_figure()
fig.savefig("figures/monthlyNbAnswersHadoopVsSpark.svg", format="svg")
cur.close()
conn.close()


