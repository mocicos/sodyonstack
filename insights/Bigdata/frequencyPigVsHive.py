# Copyright Maxime MORGE, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import numpy as np

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
  SELECT rank() OVER (ORDER BY probability_post ASC, dwh_member_id) AS Position, probability_post 
    FROM DWH_Membership
   WHERE dwh_topic_id = 2
   ORDER BY probability_post ASC;      
"""
)
data = {"User rank over posts" : [], "Bi-weekly number of posts" : [], "Community" : []}
rows = cur.fetchall()

for i in rows:
     data["User rank over posts"].append(i[0])
     data["Bi-weekly number of posts"].append(i[1])
     data["Community"].append("pig")


cur.execute("""
  SELECT rank() OVER (ORDER BY probability_post ASC, dwh_member_id) AS Position, probability_post 
    FROM DWH_Membership
   WHERE dwh_topic_id = 3
   ORDER BY probability_post ASC;      
""")
rows = cur.fetchall()
for i in rows:
    data["User rank over posts"].append(i[0])
    data["Bi-weekly number of posts"].append(i[1])
    data["Community"].append("hive")

#N = 100000
#s = 0.00001
#h = sum([1/(n**s) for n in range(1, N)]) 
#for k in range(N):
#    data["User rank over posts"].append(k)
#    data["Bi-weekly number of posts"].append(1/h * 1 /((k+1)**s))
#    data["Community"].append("Zipf law")
#a = 2
#k = 5
#for x in range(N):
#    data["User rank over posts"].append(x)
#    data["Bi-weekly number of posts"].append(a *  x**k)
#    data["Community"].append("Power law")

plot = sns.lineplot(x="User rank over posts", y="Bi-weekly number of posts", hue="Community", data=data)
pal = sns.color_palette()

cur.execute("""
    SELECT AVG(probability_post)
      FROM DWH_Membership
     WHERE dwh_topic_id = 2;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[0])

cur.execute("""
    SELECT AVG(probability_post)
      FROM DWH_Membership
     WHERE dwh_topic_id = 3;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[1])
#plot.set(xscale="log")
#plot.set(yscale="log")
plot.set(title = 'Propensity of users to contribute regularly')
fig=plot.get_figure()
fig.savefig("figures/frequencyPigVsHive.svg", format="svg")
cur.close()
conn.close()
