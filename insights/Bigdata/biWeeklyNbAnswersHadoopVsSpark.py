# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Time" : [], "Bi-weekly number of answers" : [], "Community" : []}

cur.execute("""
        SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD'), nbAnswers
          FROM
            (
              SELECT dwh_date.year AS annee,
                     dwh_date.numberOfMonth AS mois,
                     dwh_date.numberOfDay /16 AS quinzaine,
                     COUNT(dwh_contributes.dwh_contains_id) nbAnswers
                FROM dwh_contributes
                     JOIN dwh_date
                         ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
                     JOIN dwh_content r
                         ON dwh_contributes.dwh_reply_id = r.dwh_content_id
               WHERE r.tags LIKE '%<hadoop>%'
               GROUP BY year, numberOfMonth, quinzaine
               ORDER BY year, numberOfMonth, quinzaine
            ) T
            WHERE TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD') < '2020-11-15';
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly number of answers"].append(i[1])
    data["Community"].append("hadoop")


cur.execute("""
        SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD'), nbAnswers
          FROM
            (
              SELECT dwh_date.year AS annee,
                     dwh_date.numberOfMonth AS mois,
                     dwh_date.numberOfDay /16 AS quinzaine,
                     COUNT(dwh_contributes.dwh_contains_id) nbAnswers
                FROM dwh_contributes
                     JOIN dwh_date
                         ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
                     JOIN dwh_content r
                         ON dwh_contributes.dwh_reply_id = r.dwh_content_id 
               WHERE r.tags LIKE '%<apache-spark>%'
               GROUP BY year, numberOfMonth, quinzaine
               ORDER BY year, numberOfMonth, quinzaine
            ) T
            WHERE TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD') < '2020-11-15';
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly number of answers"].append(i[1])
    data["Community"].append("spark")

plot = sns.lineplot(x="Time", y="Bi-weekly number of answers", hue="Community", data=data)
plot.set(title = 'Bi-weekly number of answers')
fig=plot.get_figure()
fig.savefig("figures/biWeeklyNbAnswersHadoopVsSpark.svg", format="svg")
cur.close()
conn.close()


