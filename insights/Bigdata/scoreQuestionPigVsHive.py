# Copyright Maxime MORGE, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import numpy as np

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
  SELECT rank() OVER (ORDER BY mean_score_questions ASC, dwh_member_id) AS Position, mean_score_questions 
    FROM DWH_Membership
   WHERE dwh_topic_id = 2
   ORDER BY mean_score_questions
 ASC;      
"""
)
data = {"User rank over mean score question" : [], "Mean score question" : [], "Community" : []}
rows = cur.fetchall()

for i in rows:
     data["User rank over mean score question"].append(i[0])
     data["Mean score question"].append(i[1])
     data["Community"].append("pig")


cur.execute("""
  SELECT rank() OVER (ORDER BY mean_score_questions ASC, dwh_member_id) AS Position, mean_score_questions 
    FROM DWH_Membership
   WHERE dwh_topic_id = 3
   ORDER BY mean_score_questions
 ASC;      
""")
rows = cur.fetchall()
for i in rows:
    data["User rank over mean score question"].append(i[0])
    data["Mean score question"].append(i[1])
    data["Community"].append("hive")

plot = sns.lineplot(x="User rank over mean score question", y="Mean score question", hue="Community", data=data)
pal = sns.color_palette()

cur.execute("""
    SELECT AVG(mean_score_questions
)
      FROM DWH_Membership
     WHERE dwh_topic_id = 2;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[0])

cur.execute("""
    SELECT AVG(mean_score_questions
)
      FROM DWH_Membership
     WHERE dwh_topic_id = 3;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[1])
#plot.set(xscale="log")
#plot.set(yscale="log")
plot.set(title = 'Revelance of users')
fig=plot.get_figure()
fig.savefig("figures/scoreQuestionPigVsHive.svg", format="svg")
cur.close()
conn.close()
