# Copyright Maxime MORGE, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import numpy as np

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
  SELECT rank() OVER (ORDER BY z_score ASC, dwh_member_id) AS Position, z_score 
    FROM DWH_Membership
   WHERE dwh_topic_id = 2
   ORDER BY z_score
 ASC;      
"""
)
data = {"User rank over z-score" : [], "Z-score" : [], "Community" : []}
rows = cur.fetchall()

for i in rows:
     data["User rank over z-score"].append(i[0])
     data["Z-score"].append(i[1])
     data["Community"].append("pig")


cur.execute("""
  SELECT rank() OVER (ORDER BY z_score ASC, dwh_member_id) AS Position, z_score 
    FROM DWH_Membership
   WHERE dwh_topic_id = 3
   ORDER BY z_score
 ASC;      
""")
rows = cur.fetchall()
for i in rows:
    data["User rank over z-score"].append(i[0])
    data["Z-score"].append(i[1])
    data["Community"].append("hive")

plot = sns.lineplot(x="User rank over z-score", y="Z-score", hue="Community", data=data)
pal = sns.color_palette()

cur.execute("""
    SELECT AVG(z_score)
      FROM DWH_Membership
     WHERE dwh_topic_id = 2;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[0])

cur.execute("""
    SELECT AVG(z_score)
      FROM DWH_Membership
     WHERE dwh_topic_id = 3;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[1])
#plot.set(xscale="log")
#plot.set(yscale="log")
plot.set(title = 'Propension of users to produce rather than consume')
fig=plot.get_figure()
fig.savefig("figures/z_scorePigVsHive.svg", format="svg")
cur.close()
conn.close()
