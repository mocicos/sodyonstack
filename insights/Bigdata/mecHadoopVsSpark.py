# Copyright Maxime MORGE, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import numpy as np

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
  SELECT rank() OVER (ORDER BY mec ASC, dwh_member_id) AS Position, mec 
    FROM DWH_Membership
   WHERE dwh_topic_id = 1
   ORDER BY mec
 ASC;      
"""
)
data = {"User rank over MEC" : [], "Mean Expertise Contribution (MEC)" : [], "Community" : []}
rows = cur.fetchall()

for i in rows:
     data["User rank over MEC"].append(i[0])
     data["Mean Expertise Contribution (MEC)"].append(i[1])
     data["Community"].append("hadoop")


cur.execute("""
  SELECT rank() OVER (ORDER BY mec ASC, dwh_member_id) AS Position, mec 
    FROM DWH_Membership
   WHERE dwh_topic_id = 4
   ORDER BY mec
 ASC;      
""")
rows = cur.fetchall()
for i in rows:
    data["User rank over MEC"].append(i[0])
    data["Mean Expertise Contribution (MEC)"].append(i[1])
    data["Community"].append("spark")

plot = sns.lineplot(x="User rank over MEC", y="Mean Expertise Contribution (MEC)", hue="Community", data=data)
pal = sns.color_palette()

cur.execute("""
    SELECT AVG(mec)
      FROM DWH_Membership
     WHERE dwh_topic_id = 1;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[0])

cur.execute("""
    SELECT AVG(mec)
      FROM DWH_Membership
     WHERE dwh_topic_id = 4;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[1])
#plot.set(xscale="log")
#plot.set(yscale="log")
plot.set(title = 'Expertise of users')
fig=plot.get_figure()
fig.savefig("figures/mecHadoopVsSpark.svg", format="svg")
cur.close()
conn.close()
