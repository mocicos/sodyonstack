# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()
data={"Time" : [], "Bi-weekly number of posts" : [], "Community" : []}

cur.execute("""
        SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD'), nbPosts
          FROM
            (
              SELECT dwh_date.year AS annee,
                     dwh_date.numberOfMonth AS mois,
                     dwh_date.numberOfDay /16 AS quinzaine,
                     COUNT(dwh_contributes.dwh_contains_id) nbPosts
                FROM dwh_contributes
                     JOIN dwh_date
                         ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
                     JOIN dwh_content c
                         ON dwh_contributes.dwh_contains_id = c.dwh_content_id
                     JOIN dwh_content r
                         ON dwh_contributes.dwh_reply_id = r.dwh_content_id
           WHERE (c.tags LIKE '%<apache-pig>%'
              OR r.tags LIKE '%<apache-pig>%')
             AND dwh_date.date < '2020-10-15'
               GROUP BY year, numberOfMonth, quinzaine
               ORDER BY year, numberOfMonth, quinzaine
            ) T;
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly number of posts"].append(i[1])
    data["Community"].append("pig")


cur.execute("""
        SELECT TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD'), nbPosts
          FROM
            (
              SELECT dwh_date.year AS annee,
                     dwh_date.numberOfMonth AS mois,
                     dwh_date.numberOfDay /16 AS quinzaine,
                     COUNT(dwh_contributes.dwh_contains_id) nbPosts
                FROM dwh_contributes
                     JOIN dwh_date
                         ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
                     JOIN dwh_content c
                         ON dwh_contributes.dwh_contains_id = c.dwh_content_id
                     JOIN dwh_content r
                         ON dwh_contributes.dwh_reply_id = r.dwh_content_id
               WHERE (c.tags LIKE '%<hive>%'
                  OR r.tags LIKE '%<hive>%')
                 AND dwh_date.date < '2020-10-15'
               GROUP BY year, numberOfMonth, quinzaine
               ORDER BY year, numberOfMonth, quinzaine
            ) T
            WHERE TO_DATE(CONCAT(annee, '/', mois, '/', quinzaine*14+1), 'YYYY/MM/DD') < '2020-11-15';
"""
)
rows = cur.fetchall()
for i in rows:
    data["Time"].append(i[0])
    data["Bi-weekly number of posts"].append(i[1])
    data["Community"].append("hive")

plot = sns.lineplot(x="Time", y="Bi-weekly number of posts", hue="Community", data=data)
plot.set(title = 'Bi-Weekly number of posts')
fig=plot.get_figure()
fig.savefig("figures/biWeeklyNbPostsPigVsHive.svg", format="svg")
cur.close()
conn.close()


