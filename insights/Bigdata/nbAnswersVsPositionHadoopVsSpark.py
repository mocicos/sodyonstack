# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import pandas as pd

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Number of answers" : [], "Community" : [], "Position (in time)" : [], "Number of answers" : []}

cur.execute("""
  SELECT
    rank() OVER (ORDER BY date_question.date ASC) AS position,
    question.answer_count AS number_answers
    FROM dwh_contributes question
         JOIN dwh_date date_question
             ON question.dwh_creation_date_id = date_question.dwh_date_id
         JOIN dwh_content question_content
             ON question.dwh_contains_id = question_content.dwh_content_id
   WHERE question_content.tags LIKE '%<hadoop>%'
     AND question.dwh_kind_of_id = 1
     AND date_question.date < '2020-03-01'
   ORDER BY position
"""
)
rows = cur.fetchall()
for i in rows:
    data["Position (in time)"].append(i[0])
    data["Number of answers"].append(i[1])
    data["Community"].append("Hadoop")


cur.execute("""
  SELECT
    rank() OVER (ORDER BY date_question.date ASC) AS position,
    question.answer_count AS number_answers
    FROM dwh_contributes question
         JOIN dwh_date date_question
             ON question.dwh_creation_date_id = date_question.dwh_date_id
         JOIN dwh_content question_content
             ON question.dwh_contains_id = question_content.dwh_content_id
   WHERE question_content.tags LIKE '%<apache-spark>%'
     AND question.dwh_kind_of_id = 1
     AND date_question.date < '2020-03-01'
   ORDER BY position
"""
)
rows = cur.fetchall()
for i in rows:
    data["Position (in time)"].append(i[0])
    data["Number of answers"].append(i[1])
    data["Community"].append("Spark")

plot = sns.scatterplot(x="Position (in time)", y="Number of answers", hue="Community", data=data)#size="Number of answers",
plot.set(title = 'Number of answers Vs. Position (in time)')
#plot.set(xscale="log")
fig=plot.get_figure()
fig.savefig("figures/nbAnswersVsPositionHadoopVsSpark.png", format="png")
cur.close()
conn.close()
