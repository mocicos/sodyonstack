# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
      SELECT User_Percentage, MAX(Cumulative_Answer_Percentage)
      FROM
        (
          SELECT dwh_member_id,
                 SUM(CAST(out_degree_accepted AS FLOAT) / (SELECT SUM(out_degree_accepted) FROM DWH_Membership WHERE dwh_topic_id = 2) * 100) 
                   OVER (ORDER BY CAST(out_degree_accepted AS FLOAT) / (SELECT SUM(out_degree_accepted) FROM DWH_Membership WHERE dwh_topic_id = 2) ASC) AS Cumulative_Answer_Percentage,
                 ntile(100)
                   OVER (ORDER BY CAST(out_degree_accepted AS FLOAT) / (SELECT SUM(out_degree_accepted) FROM DWH_Membership WHERE dwh_topic_id = 2) ASC) AS User_Percentage
            FROM DWH_Membership
           WHERE dwh_topic_id = 2
           ORDER BY out_degree_accepted ASC
          ) AS Cumul
       GROUP BY User_Percentage
       ORDER BY User_Percentage ASC;      
"""
)
data = {"Percentage of users" : [], "Percentage of accepted answers" : [], "Community" : []}
rows = cur.fetchall()
for i in rows:
    data["Percentage of users"].append(i[0])
    data["Percentage of accepted answers"].append(i[1])
    data["Community"].append("pig")


cur.execute("""
      SELECT User_Percentage, MAX(Cumulative_Answer_Percentage)
      FROM
        (
          SELECT dwh_member_id,
                 SUM(CAST(out_degree_accepted AS FLOAT) / (SELECT SUM(out_degree_accepted) FROM DWH_Membership WHERE dwh_topic_id = 3) * 100) 
                   OVER (ORDER BY CAST(out_degree_accepted AS FLOAT) / (SELECT SUM(out_degree_accepted) FROM DWH_Membership WHERE dwh_topic_id = 3) ASC) AS Cumulative_Answer_Percentage,
                 ntile(100)
                   OVER (ORDER BY CAST(out_degree_accepted AS FLOAT) / (SELECT SUM(out_degree_accepted) FROM DWH_Membership WHERE dwh_topic_id = 3) ASC) AS User_Percentage
            FROM DWH_Membership
           WHERE dwh_topic_id = 3
           ORDER BY out_degree_accepted ASC
          ) AS Cumul
       GROUP BY User_Percentage
       ORDER BY User_Percentage ASC;      
""")
rows = cur.fetchall()
for i in rows:
    data["Percentage of users"].append(i[0])
    data["Percentage of accepted answers"].append(i[1])
    data["Community"].append("hive")

for i in range(100):
    data["Percentage of users"].append(i)
    data["Percentage of accepted answers"].append(i)
    data["Community"].append("Line of equality")

plot = sns.lineplot(x="Percentage of users", y="Percentage of accepted answers", hue="Community", data=data)
plot.set(title = 'Cumulative accepted helps of users from lowest to highest accepted outdegrees')
fig=plot.get_figure()
fig.savefig("figures/lorenzCurveOutdegreeAcceptedPigVsHive.svg", format="svg")
cur.close()
conn.close()
