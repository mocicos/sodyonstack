# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import pandas as pd

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Question score" : [], "Community" : [], "Best answer score" : [], "Number of answers" : []}

cur.execute("""
  SELECT
    question.score score_question,
    reponse.score score_reponse,
    question.answer_count answer_count 
    FROM dwh_contributes reponse
         JOIN dwh_date date_reponse
             ON reponse.dwh_creation_date_id = date_reponse.dwh_date_id
         JOIN dwh_content contenu_question
             ON reponse.dwh_reply_id = contenu_question.dwh_content_id
         JOIN dwh_contributes question
             ON contenu_question.dwh_content_id=question.dwh_contains_id 
   WHERE contenu_question.tags LIKE '%<hadoop>%'
     AND reponse.rank = 1
"""
)
rows = cur.fetchall()
for i in rows:
    data["Question score"].append(i[0])
    data["Best answer score"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Community"].append("Hadoop")


cur.execute("""
  SELECT
    question.score score_question,
    reponse.score score_reponse,
    question.answer_count answer_count 
    FROM dwh_contributes reponse
         JOIN dwh_date date_reponse
             ON reponse.dwh_creation_date_id = date_reponse.dwh_date_id
         JOIN dwh_content contenu_question
             ON reponse.dwh_reply_id = contenu_question.dwh_content_id
         JOIN dwh_contributes question
             ON contenu_question.dwh_content_id=question.dwh_contains_id 
   WHERE contenu_question.tags LIKE '%<apache-spark>%'
     AND reponse.rank = 1
"""
)
rows = cur.fetchall()
for i in rows:
    data["Question score"].append(i[0])
    data["Best answer score"].append(i[1])
    data["Number of answers"].append(i[2])
    data["Community"].append("Spark")

plot = sns.scatterplot(x="Question score", y="Best answer score", size="Number of answers", hue="Community", data=data)

plot.set(title = 'Question score Vs. Best answer score')

fig=plot.get_figure()
fig.savefig("figures/scoreQuestionVsScoreBestAnswerHadoopVsSpark.png", format="png")
cur.close()
conn.close()


