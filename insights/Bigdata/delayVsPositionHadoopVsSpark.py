# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import pandas as pd

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
#PSWD = "arthur"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

data={"Mean delay of answers (weeks)" : [], "Community" : [], "Position (in time)" : [], "Number of answers" : []}

cur.execute("""
  SELECT
    rank() OVER (ORDER BY date_reponse.date ASC) AS position,
    reponse.delay/60/60/24/7 AS delay 
    FROM dwh_contributes reponse
         JOIN dwh_date date_reponse
             ON reponse.dwh_creation_date_id = date_reponse.dwh_date_id
         JOIN dwh_content question_content
             ON reponse.dwh_reply_id = question_content.dwh_content_id
   WHERE question_content.tags LIKE '%<hadoop>%'
     AND reponse.dwh_kind_of_id = 2
     AND date_reponse.date < '2020-03-01'
   ORDER BY position
"""
)
rows = cur.fetchall()
for i in rows:
    data["Position (in time)"].append(i[0])
    data["Mean delay of answers (weeks)"].append(i[1])
    data["Community"].append("Hadoop")


cur.execute("""
  SELECT
    rank() OVER (ORDER BY date_reponse.date ASC) AS position,
    reponse.delay/60/60/24/7 AS delay 
    FROM dwh_contributes reponse
         JOIN dwh_date date_reponse
             ON reponse.dwh_creation_date_id = date_reponse.dwh_date_id
         JOIN dwh_content question_content
             ON reponse.dwh_reply_id = question_content.dwh_content_id
   WHERE question_content.tags LIKE '%<apache-spark>%'
     AND reponse.dwh_kind_of_id = 2
     AND date_reponse.date < '2020-03-01'
   ORDER BY position
"""
)
rows = cur.fetchall()
for i in rows:
    data["Position (in time)"].append(i[0])
    data["Mean delay of answers (weeks)"].append(i[1])
    data["Community"].append("Spark")

plot = sns.scatterplot(x="Position (in time)", y="Mean delay of answers (weeks)", hue="Community", data=data)#size="Number of answers",
plot.set(title = 'Mean delay of answers (weeks) Vs. Position (in time)')
#plot.set(xscale="log")
fig=plot.get_figure()
fig.savefig("figures/delayVsPositionHadoopVsSpark.png", format="png")
cur.close()
conn.close()


