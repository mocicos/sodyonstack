# Copyright Maxime MORGE, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import numpy as np

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
  SELECT rank() OVER (ORDER BY probability_post DESC, dwh_member_id) AS Position, probability_post 
    FROM DWH_Membership
   WHERE dwh_topic_id = 1
   ORDER BY probability_post DESC;      
"""
)
data = {"User rank over posts" : [], "Bi-weekly number of posts" : [], "Community" : []}
rows = cur.fetchall()

for i in rows:
     data["User rank over posts"].append(i[0])
     data["Bi-weekly number of posts"].append(i[1])
     data["Community"].append("hadoop")


cur.execute("""
  SELECT rank() OVER (ORDER BY probability_post DESC, dwh_member_id) AS Position, probability_post 
    FROM DWH_Membership
   WHERE dwh_topic_id = 4
   ORDER BY probability_post DESC;
""")
rows = cur.fetchall()

for i in rows:
    data["User rank over posts"].append(i[0])
    data["Bi-weekly number of posts"].append(i[1])
    data["Community"].append("Spark")

plot = sns.lineplot(x="User rank over posts", y="Bi-weekly number of posts", hue="Community", data=data)
pal = sns.color_palette()

cur.execute("""
    SELECT AVG(probability_post)
      FROM DWH_Membership
     WHERE dwh_topic_id = 1;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[0])

cur.execute("""
    SELECT AVG(probability_post)
      FROM DWH_Membership
     WHERE dwh_topic_id = 4;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[1])

plot.set(title = 'Propensity of users to contribute regularly')
fig=plot.get_figure()
fig.savefig("figures/reverseFrequencyHadoopVsSpark.svg", format="svg")
cur.close()
conn.close()
