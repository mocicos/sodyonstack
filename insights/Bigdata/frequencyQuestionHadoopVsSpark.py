# Copyright Maxime MORGE, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns
import numpy as np

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
  SELECT rank() OVER (ORDER BY probability_query ASC, dwh_member_id) AS Position, probability_query 
    FROM DWH_Membership
   WHERE dwh_topic_id = 1
   ORDER BY probability_query ASC;      
"""
)
data = {"User rank over question" : [], "Bi-weekly number of questions" : [], "Community" : []}
rows = cur.fetchall()

for i in rows:
     data["User rank over question"].append(i[0])
     data["Bi-weekly number of questions"].append(i[1])
     data["Community"].append("hadoop")


cur.execute("""
  SELECT rank() OVER (ORDER BY probability_query ASC, dwh_member_id) AS Position, probability_query 
    FROM DWH_Membership
   WHERE dwh_topic_id = 4
   ORDER BY probability_query ASC;      
""")
rows = cur.fetchall()
for i in rows:
    data["User rank over question"].append(i[0])
    data["Bi-weekly number of questions"].append(i[1])
    data["Community"].append("spark")

#N = 100000
#s = 0.00001
#h = sum([1/(n**s) for n in range(1, N)]) 
#for k in range(N):
#    data["User rank over question"].append(k)
#    data["Bi-weekly number of questions"].append(1/h * 1 /((k+1)**s))
#    data["Community"].append("Zipf law")
#a = 2
#k = 5
#for x in range(N):
#    data["User rank over question"].append(x)
#    data["Bi-weekly number of questions"].append(a *  x**k)
#    data["Community"].append("Power law")

plot = sns.lineplot(x="User rank over question", y="Bi-weekly number of questions", hue="Community", data=data)
pal = sns.color_palette()

cur.execute("""
    SELECT AVG(probability_query)
      FROM DWH_Membership
     WHERE dwh_topic_id = 1;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[0])

cur.execute("""
    SELECT AVG(probability_query)
      FROM DWH_Membership
     WHERE dwh_topic_id = 2;          
""")
mean = cur.fetchall()
plot.axhline(mean[0], color=pal[1])
#plot.set(xscale="log")
#plot.set(yscale="log")
plot.set(title = 'Propensity of users to query regularly')
fig=plot.get_figure()
fig.savefig("figures/frequencyQuestionHadoopVsSpark.svg", format="svg")
cur.close()
conn.close()
