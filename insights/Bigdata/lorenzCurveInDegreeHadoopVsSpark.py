# Copyright Arthur ASSIMA, 2022
from matplotlib.pyplot import title
import psycopg2
import seaborn as sns

#Connection to the database
USER="ubuntu"
DATABASE="sodyonstack"
conn=psycopg2.connect(" dbname=%s user=%s" %( DATABASE, USER))
cur=conn.cursor()

cur.execute("""
      SELECT User_Percentage, MAX(Cumulative_Question_Percentage)
      FROM
        (
          SELECT dwh_member_id,
                 SUM(CAST(in_degree AS FLOAT) / (SELECT SUM(in_degree) FROM DWH_Membership WHERE dwh_topic_id = 1) * 100) 
                   OVER (ORDER BY CAST(in_degree AS FLOAT) / (SELECT SUM(in_degree) FROM DWH_Membership WHERE dwh_topic_id = 1) ASC) AS Cumulative_Question_Percentage,
                 ntile(100)
                   OVER (ORDER BY CAST(in_degree AS FLOAT) / (SELECT SUM(in_degree) FROM DWH_Membership WHERE dwh_topic_id = 1) ASC) AS User_Percentage
            FROM DWH_Membership
           WHERE dwh_topic_id = 1
           ORDER BY in_degree ASC
          ) AS Cumul
       GROUP BY User_Percentage
       ORDER BY User_Percentage ASC;
"""
)
data = {"Percentage of users" : [], "Percentage of questions" : [], "Community" : []}
rows = cur.fetchall()
for i in rows:
    data["Percentage of users"].append(i[0])
    data["Percentage of questions"].append(i[1])
    data["Community"].append("hadoop")


cur.execute("""
      SELECT User_Percentage, MAX(Cumulative_Question_Percentage)
      FROM
        (
          SELECT dwh_member_id,
                 SUM(CAST(in_degree AS FLOAT) / (SELECT SUM(in_degree) FROM DWH_Membership WHERE dwh_topic_id = 4) * 100) 
                   OVER (ORDER BY CAST(in_degree AS FLOAT) / (SELECT SUM(in_degree) FROM DWH_Membership WHERE dwh_topic_id = 4) ASC) AS Cumulative_Question_Percentage,
                 ntile(100)
                   OVER (ORDER BY CAST(in_degree AS FLOAT) / (SELECT SUM(in_degree) FROM DWH_Membership WHERE dwh_topic_id = 4) ASC) AS User_Percentage
            FROM DWH_Membership
           WHERE dwh_topic_id = 4
           ORDER BY in_degree ASC
          ) AS Cumul
       GROUP BY User_Percentage
       ORDER BY User_Percentage ASC;
""")
rows = cur.fetchall()
for i in rows:
    data["Percentage of users"].append(i[0])
    data["Percentage of questions"].append(i[1])
    data["Community"].append("spark")

for i in range(100):
    data["Percentage of users"].append(i)
    data["Percentage of questions"].append(i)
    data["Community"].append("Line of equality")

plot = sns.lineplot(x="Percentage of users", y="Percentage of questions", hue="Community", data=data)
plot.set(title = 'Cumulative queries of users from lowest to highest indegrees')
fig=plot.get_figure()
fig.savefig("figures/lorenzCurveIndegreeHadoopVsSpark.svg", format="svg")
cur.close()
conn.close()
