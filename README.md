# What is SoDyOnStack ?

SoDyOnStack is a tool for analyzing the social dynamics on [Stack Overflow](https://stackoverflow.com/).

## Context

This project adopts an interdisciplinary approach, combining Computer Science
with Sociology, for introspection of the design choices of sociotechnical tools
by identifying the meso processes of limited solidarity where some cohesive
exchanges create cooperative links.


## Scientific challenge

Beyond a simple question-and-answer web site on computer programming, Stack
Overflow is a collaborative platform which structure the communities based by
promoting of the knowledge of individuals and empower them within a new
redistribution model. Participants can
[vote](https://stackoverflow.blog/2019/11/13/were-rewarding-the-question-askers/)
questions and answers up or down and edit questions and answers. They can earn
[reputation](https://stackoverflow.com/help/whats-reputation) points and
"badges" for their valued contributions.

Our goals is to extract, transform and load [open
data](https://archive.org/details/stackexchange) from Stack Overflow to analyze
social facts in order to better apprehend the dynamic of the developer
communities.

## Deliverables

The deliverables are:

1. The analysis of restitution needs and available data

2. The design of the multidimensional data model

3. The implementation of the multidimensional logical model of data, the data
   flows, and the dashboard


## Contributors

Copyright (C) Maxime MORGE 2021

## Additional contributors

Sébastien DELARRE, Fabien ELOIRE, Antoine NONGAILLARD, Arthur ASSIMA, Audrey DEWAELE

## Requirements

Some tools:

- [p7zip](http://p7zip.sourceforge.net/)
- [Go](https://golang.org/)
- [GNU Awk](https://www.gnu.org/software/gawk/)
- [stackexchange-xml-converter](https://github.com/SkobelevIgor/stackexchange-xml-converter)
- [PostgreSQL](https://www.postgresql.org/) 

Eventually:

- [Mocodo](http://mocodo.net/)
- [Gnuplot](http://www.gnuplot.info/)
- [Graphviz](https://graphviz.org/)
- [Ammonite](https://ammonite.io/)

At least 150 Gio free disk space
