-- Copyright (C) Maxime MORGE, Salwa FADEL 2021, Audrey DEWAELE 2022
-- Data integrity verification

--- Coherence of the date of comments
-- Is the date of a comments is always later than the date of the contribution ?
SELECT DWH_CONTRIBUTES.dwh_contains_id,
       DWH_CONTRIBUTES.stamp,
       DWH_COMMENTS.dwh_contains_id,
       DWH_COMMENTS.stamp
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
         INNER JOIN DWH_COMMENTS ON DWH_CONTENT.dwh_content_id=DWH_COMMENTS.dwh_reacts_id
 WHERE DWH_CONTRIBUTES.stamp > DWH_COMMENTS.stamp
 ORDER BY DWH_CONTRIBUTES.dwh_contains_id;
--  dwh_contains_id |          stamp          | dwh_contains_id |          stamp
-- -----------------+-------------------------+-----------------+-------------------------
--           159033 | 2013-07-17 17:04:10.55 |           81950 | 2013-07-17 14:37:57.64
-- 1 row
-- Is the creation date of a comment is always later than the creation date of the
-- author ?
SELECT --DWH_USER.dwh_user_id user_id,
  COUNT(DWH_COMMENTS.dwh_contains_id) -- content_id
  FROM DWH_COMMENTS
         INNER JOIN DWH_USER ON dwh_user_id=dwh_author_id
 WHERE DWH_COMMENTS.stamp < DWH_USER.creation_date;
-- 1 content
-- Remove these comments
DROP TABLE IF EXISTS REJECTED_COMMENTS;
CREATE TABLE REJECTED_COMMENTS AS (
  SELECT DWH_COMMENTS.dwh_author_id, DWH_COMMENTS.dwh_reacts_id, DWH_COMMENTS.dwh_contains_id,
         DWH_COMMENTS.dwh_creation_date_id, DWH_COMMENTS.dwh_interlocutor_id,
         DWH_COMMENTS.score, DWH_COMMENTS.delay, DWH_COMMENTS.stamp
    FROM DWH_CONTRIBUTES
           INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
           INNER JOIN DWH_COMMENTS ON DWH_CONTENT.dwh_content_id=DWH_COMMENTS.dwh_reacts_id
   WHERE DWH_CONTRIBUTES.stamp > DWH_COMMENTS.stamp
 UNION
  SELECT DWH_COMMENTS.dwh_author_id, DWH_COMMENTS.dwh_reacts_id, DWH_COMMENTS.dwh_contains_id,
         DWH_COMMENTS.dwh_creation_date_id, DWH_COMMENTS.dwh_interlocutor_id,
         DWH_COMMENTS.score, DWH_COMMENTS.delay, DWH_COMMENTS.stamp
    FROM DWH_COMMENTS
           INNER JOIN DWH_USER ON dwh_user_id=dwh_author_id
   WHERE DWH_COMMENTS.stamp < DWH_USER.creation_date);
-- 2 comments
DELETE FROM DWH_COMMENTS
 WHERE dwh_contains_id IN (SELECT dwh_contains_id  FROM REJECTED_COMMENTS);
-- DELETE 2 COMMENTS

--- Coherence of the date of comments
-- Is the date of an answer always later than the date of a question ?
SELECT INQUIRY.dwh_contains_id inquiry_content_id,
       DATE_INQUIRY.date inquiry_date,
       ANSWER.dwh_contains_id answer_content_id,
       DATE_ANSWER.date answer_date
  FROM DWH_CONTRIBUTES AS INQUIRY
         INNER JOIN DWH_DATE AS DATE_INQUIRY ON INQUIRY.dwh_creation_date_id = DATE_INQUIRY.dwh_date_id
         INNER JOIN DWH_CONTRIBUTES AS ANSWER ON ANSWER.dwh_reply_id = INQUIRY.dwh_contains_id
         INNER JOIN DWH_DATE AS DATE_ANSWER ON ANSWER.dwh_creation_date_id = DATE_ANSWER.dwh_date_id
 WHERE INQUIRY.dwh_kind_of_id=1
   AND DATE_INQUIRY.date > DATE_ANSWER.date
 ORDER BY inquiry_content_id;
-- inquiry_content_id |      inquiry_date       | answer_content_id |       answer_date
-- --------------------+-------------------------+-------------------+-------------------------
--               74538 | 2010-07-13 09:07:51.283  |            157484 | 2010-07-13 08:02:22.65
-- 1 row
-- Is the creation date of a contribution is always later than the creation date of the
-- author ?
SELECT --DWH_USER.dwh_user_id user_id,
  COUNT(DWH_CONTRIBUTES.dwh_contains_id) -- content_id
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_USER ON dwh_user_id=dwh_author_id
         INNER JOIN DWH_DATE ON DWH_CONTRIBUTES.dwh_creation_date_id = DWH_DATE.dwh_date_id
 WHERE DWH_DATE.date < DWH_USER.creation_date;
-- 69 contents
-- Remove these questions/answers/replies and the corresponding contents and users if required.
DROP TABLE IF EXISTS REJECTED_CONTRIBUTES;
CREATE TABLE REJECTED_CONTRIBUTES AS (
  SELECT ANSWER.dwh_contains_id content_id,
         ANSWER.dwh_creation_date_id date_id,
         ANSWER.dwh_author_id author_id,
         2 post_type,
         ANSWER.dwh_interlocutor_id interlocutor_id,
         ANSWER.dwh_reply_id reply_id,
         ANSWER.dwh_last_activity_id last_activity_id,
         ANSWER.dwh_last_edit_date_id last_edit_date_id
  FROM DWH_CONTRIBUTES AS INQUIRY
         INNER JOIN DWH_DATE AS DATE_INQUIRY ON INQUIRY.dwh_creation_date_id = DATE_INQUIRY.dwh_date_id
         INNER JOIN DWH_CONTRIBUTES AS ANSWER ON ANSWER.dwh_reply_id = INQUIRY.dwh_contains_id
         INNER JOIN DWH_DATE AS DATE_ANSWER ON ANSWER.dwh_creation_date_id = DATE_ANSWER.dwh_date_id
 WHERE INQUIRY.dwh_kind_of_id=1
   AND DATE_INQUIRY.date > DATE_ANSWER.date
 UNION
  SELECT INQUIRY.dwh_contains_id content_id,
         INQUIRY.dwh_creation_date_id date_id,
         INQUIRY.dwh_author_id author_id,
         1 post_type,
         INQUIRY.dwh_interlocutor_id interlocutor_id,
         INQUIRY.dwh_reply_id reply_id,
         INQUIRY.dwh_last_activity_id last_activity_id,
         INQUIRY.dwh_last_edit_date_id last_edit_date_id
    FROM DWH_CONTRIBUTES AS INQUIRY
         INNER JOIN DWH_DATE AS DATE_INQUIRY ON INQUIRY.dwh_creation_date_id = DATE_INQUIRY.dwh_date_id
         INNER JOIN DWH_CONTRIBUTES AS ANSWER ON ANSWER.dwh_reply_id = INQUIRY.dwh_contains_id
         INNER JOIN DWH_DATE AS DATE_ANSWER ON ANSWER.dwh_creation_date_id = DATE_ANSWER.dwh_date_id
 WHERE INQUIRY.dwh_kind_of_id=1
   AND DATE_INQUIRY.date > DATE_ANSWER.date
   UNION
  SELECT DWH_CONTRIBUTES.dwh_contains_id content_id,
         DWH_CONTRIBUTES.dwh_creation_date_id date_id,
         DWH_CONTRIBUTES.dwh_author_id author_id,
         DWH_CONTRIBUTES.dwh_kind_of_id post_type,
         DWH_CONTRIBUTES.dwh_interlocutor_id interlocutor_id,
         DWH_CONTRIBUTES.dwh_reply_id reply_id,
         DWH_CONTRIBUTES.dwh_last_activity_id last_activity_id,
         DWH_CONTRIBUTES.dwh_last_edit_date_id last_edit_date_id
    FROM DWH_CONTRIBUTES
           INNER JOIN DWH_USER ON dwh_user_id=dwh_author_id
           INNER JOIN DWH_DATE ON DWH_CONTRIBUTES.dwh_creation_date_id = DWH_DATE.dwh_date_id
   WHERE DWH_DATE.date < DWH_USER.creation_date
);
-- 71 rejected contributions
INSERT INTO REJECTED_CONTRIBUTES (content_id, date_id, author_id, post_type, interlocutor_id, reply_id, last_activity_id, last_edit_date_id)
SELECT DWH_CONTRIBUTES.dwh_contains_id content_id,
       DWH_CONTRIBUTES.dwh_creation_date_id date_id,
       DWH_CONTRIBUTES.dwh_author_id author_id,
       DWH_CONTRIBUTES.dwh_kind_of_id post_type,
       DWH_CONTRIBUTES.dwh_interlocutor_id interlocutor_id,
       DWH_CONTRIBUTES.dwh_reply_id reply_id,
       DWH_CONTRIBUTES.dwh_last_activity_id last_activity_id,
       DWH_CONTRIBUTES.dwh_last_edit_date_id last_edit_date_id
  FROM DWH_CONTRIBUTES
         INNER JOIN REJECTED_CONTRIBUTES ON REJECTED_CONTRIBUTES.content_id=DWH_CONTRIBUTES.dwh_reply_id
 WHERE DWH_CONTRIBUTES.dwh_contains_id NOT IN (SELECT content_id FROM REJECTED_CONTRIBUTES) ;
-- 0 60 additional replies to these rejected contributions
DELETE FROM DWH_CONTRIBUTES
 WHERE dwh_contains_id IN (SELECT content_id  FROM REJECTED_CONTRIBUTES);
-- DELETE 131 contributions
DELETE FROM DWH_COMMENTS
 WHERE dwh_reacts_id IN (SELECT content_id  FROM REJECTED_CONTRIBUTES);
-- DELETE 58 comments
DELETE FROM DWH_CONTENT
 WHERE dwh_content_id IN (SELECT content_id  FROM REJECTED_CONTRIBUTES);
-- DELETE 131 contents
DELETE FROM DWH_USER
 WHERE dwh_user_id NOT IN (
   SELECT dwh_author_id FROM DWH_CONTRIBUTES
    UNION
   SELECT dwh_interlocutor_id FROM DWH_CONTRIBUTES
    UNION
   SELECT dwh_author_id FROM DWH_COMMENTS
 );
-- DELETE O users

-- Is the last access date of a user is always later than the creation date of a user ?
SELECT COUNT(*)
  FROM dwh_user
 WHERE creation_date > last_access_date;
-- O users

