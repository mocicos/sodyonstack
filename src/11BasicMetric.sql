-- Copyright (C) Maxime MORGE, Salwa FADEL 2021, Audrey DEWAELE 2022
-- Basic metrics

-- The date of the first contribution on <hadoop>
SELECT MIN(stamp)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<hadoop>%';
--  2008-08-20 10:43:13.227

-- The date of the first contribution on <hive>
SELECT MIN(stamp)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<hive>%';
--  2008-08-23 12:22:04.993

-- The date of the first contribution on <apache-pig>
SELECT MIN(stamp)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<apache-pig>%';
--  2008-12-15 13:57:23.883

-- The date of the first contribution on <apache-spark>
SELECT MIN(stamp)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<apache-spark>%';
--  2011-11-30 18:41:01.22



-- The date of the last contribution on <hadoop>
SELECT MAX(stamp)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<hadoop>%';
--  2021-12-05 04:15:33.17

-- The date of the last contribution on <hive>
SELECT MAX(stamp)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<hive>%';
-- 2021-12-05 04:15:33.17

-- The date of the last contribution on <apache-pig>
SELECT MAX(stamp)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<apache-pig>%';
--  2021-11-25 21:34:53.76

-- The date of the last contribution on <apache-spark>
SELECT MAX(stamp)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<apache-spark>%';
-- 2020-12-05 05:22:22.207



-- The proportion of users having a biography
SELECT (SELECT COUNT(dwh_user_id) FROM DWH_USER WHERE about_me IS NOT NULL)/
       COUNT(dwh_user_id)::float * 100 pencentage_bio
  FROM DWH_USER;
-- 24 %

-- The proportion of users having a website
SELECT (SELECT COUNT(dwh_user_id) FROM DWH_USER WHERE website_url IS NOT NULL)/
         COUNT(dwh_user_id)::float * 100 pencentage_website
  FROM DWH_USER;
-- 13.66 %



-- The number of questions on <hadoop>
SELECT COUNT(dwh_contains_id)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<hadoop>%' and dwh_kind_of_id = 1 ;
-- 43 120

-- The number of answers on <hadoop>
SELECT COUNT(dwh_contains_id)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_reply_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<hadoop>%' and dwh_kind_of_id = 2 ;
-- 53 803

-- The number of questions on <hive>
SELECT COUNT(dwh_contains_id)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<hive>%' and dwh_kind_of_id = 1 ;
-- 20 427

-- The number of answers on <hive>
SELECT COUNT(dwh_contains_id)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_reply_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<hive>%' and dwh_kind_of_id = 2 ;
-- 24 281

-- The number of questions on <apache-pig>
SELECT COUNT(dwh_contains_id)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<apache-pig>%' and dwh_kind_of_id = 1 ;
-- 5 138

-- The number of answers on <apache-pig>
SELECT COUNT(dwh_contains_id)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_reply_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<apache-pig>%' and dwh_kind_of_id = 2 ;
-- 6 313

-- The number of questions on <apache-spark>
SELECT COUNT(dwh_contains_id)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_contains_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<apache-spark>%' and dwh_kind_of_id = 1 ;
-- 71 338

-- The number of answers on <apache-spark>
SELECT COUNT(dwh_contains_id)
  FROM DWH_CONTRIBUTES
         INNER JOIN DWH_CONTENT ON DWH_CONTRIBUTES.dwh_reply_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<apache-spark>%' and dwh_kind_of_id = 2 ;
-- 81 180




-- The number of comments on <hadoop>
SELECT COUNT(dwh_contains_id)
  FROM DWH_COMMENTS
         INNER JOIN DWH_CONTENT ON DWH_COMMENTS.dwh_reacts_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<hadoop>%' and dwh_kind_of_id = 1 ;
-- 

-- The number of comments on <hive>
SELECT COUNT(dwh_contains_id)
  FROM DWH_COMMENTS
         INNER JOIN DWH_CONTENT ON DWH_COMMENTS.dwh_reacts_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<hive>%' and dwh_kind_of_id = 1 ;
--

-- The number of comments on <apache-pig>
SELECT COUNT(dwh_contains_id)
  FROM DWH_COMMENTS
         INNER JOIN DWH_CONTENT ON DWH_COMMENTS.dwh_reacts_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<apache-pig>%' and dwh_kind_of_id = 1 ;
--

-- The number of comments on <apache-spark>
SELECT COUNT(dwh_contains_id)
  FROM DWH_COMMENTS
         INNER JOIN DWH_CONTENT ON DWH_COMMENTS.dwh_reacts_id=DWH_CONTENT.dwh_content_id
 WHERE tags LIKE '%<apache-spark>%' and dwh_kind_of_id = 1 ;
--



