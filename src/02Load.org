#+TITLE: Notebook for loading the data in the RDMS
#+AUTHOR: Maxime MORGE Audrey DEWAELE 
#+DATE: <2021-01-19 Tue>

** DONE Creation of the user/database

#+BEGIN_SRC sh
sudo -u postgres psql
#+END_SRC

#+BEGIN_SRC sql
  CREATE TABLESPACE myspace location '/mnt/postgresql/space';
  CREATE USER ubuntu;
  ALTER ROLE ubuntu WITH CREATEDB;
  DROP DATABASE IF EXISTS sodyonstack;
  CREATE DATABASE sodyonstack OWNER ubuntu TABLESPACE myspace;
  \q
#+END_SRC

* DONE Tags

** DONE Create the schema  
   
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack"
   DROP TABLE IF EXISTS tags;
   CREATE TABLE tags (
     id SERIAL PRIMARY KEY,
     excerpt_post_id INTEGER,
     wiki_post_id INTEGER,
     tag_name VARCHAR(255) NOT NULL,
     count INTEGER DEFAULT 0
   );
 #+END_SRC

 #+RESULTS:
 | DROP TABLE   |
 |--------------|
 | CREATE TABLE |

** DONE Load the data

The COPY command copy data between a file and a table 
- CSV, selects the data format 
- NULL specifies the string that represents a null value
- HEADER specifies that the file contains a header line 
- DELIMITER specifies the character that separates columns
     
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack"
  DELETE FROM tags;
  \COPY tags from /mnt/sodyonstack/data/stackoverflow/csv/Tags.csv CSV HEADER DELIMITER ',' QUOTE '"';
#+END_SRC

#+RESULTS:
| DELETE 0     |
|--------------|
| COPY 62226   |

* DONE Users

** DONE Create the schema  
   
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack"
   DROP TABLE IF EXISTS users;
   CREATE TABLE users (
     id SERIAL PRIMARY KEY,
     account_id INTEGER,
     reputation INTEGER NOT NULL,
     views INTEGER DEFAULT 0,
     down_votes INTEGER DEFAULT 0,
     up_votes INTEGER DEFAULT 0,
     display_name VARCHAR(255) NOT NULL,
     location VARCHAR(512),
     profile_image_url VARCHAR(255),
     website_url VARCHAR(255),
     about_me TEXT,
     creation_date TIMESTAMP NOT NULL,
     last_access_date TIMESTAMP NOT NULL
   );
 #+END_SRC

 #+RESULTS:
 | DROP TABLE   |
 |--------------|
 | CREATE TABLE |

** DONE Load the data
     
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  DELETE FROM users;
  \COPY users from /mnt/sodyonstack/data/stackoverflow/csv/Users.csv CSV HEADER DELIMITER ',' QUOTE '"' ESCAPE '"';
#+END_SRC

#+RESULTS:
| DELETE 0      |
|---------------|
| COPY 16279655 |

* DONE Badges

** DONE Create the schema  
   
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   DROP TABLE IF EXISTS badges;
   CREATE TABLE badges (
     id SERIAL PRIMARY KEY,
     user_id INTEGER NOT NULL,
     class SMALLINT NOT NULL,
     name VARCHAR(64) NOT NULL,
     tag_based BOOL NOT NULL,
     date TIMESTAMP NOT NULL
   );
 #+END_SRC

 #+RESULTS:
 | DROP TABLE   |
 |--------------|
 | CREATE TABLE |

** DONE Load the data
     
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  DELETE FROM badges;
  \COPY badges from /mnt/sodyonstack/data/stackoverflow/csv/Badges.csv CSV HEADER DELIMITER ',' QUOTE '"' ESCAPE '"';
#+END_SRC

#+RESULTS:
| DELETE 0        |
|-----------------|
| COPY 42642206   |

* DONE Posts

** DONE Create the schema  
   
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack"
   DROP TABLE IF EXISTS posts;
   CREATE TABLE posts (
     id SERIAL PRIMARY KEY,
     owner_user_id INTEGER,
     last_editor_user_id INTEGER,
     post_type_id SMALLINT NOT NULL,
     accepted_answer_id INTEGER,
     score INTEGER NOT NULL,
     parent_id INTEGER,
     view_count INTEGER,
     answer_count INTEGER DEFAULT 0,
     comment_count INTEGER DEFAULT 0,
     owner_display_name VARCHAR(64),
     last_editor_display_name VARCHAR(64),
     title VARCHAR(512),
     tags VARCHAR(512),
     content_license VARCHAR(64) NOT NULL,
     body TEXT,
     favorite_count INTEGER,
     creation_date TIMESTAMP NOT NULL,
     community_owned_date TIMESTAMP,
     closed_date TIMESTAMP,
     last_edit_date TIMESTAMP,
     last_activity_date TIMESTAMP
   );
 #+END_SRC

 #+RESULTS:
 | DROP TABLE   |
 |--------------|
 | CREATE TABLE |

** DONE Load the data
     
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  DELETE FROM posts;
  \COPY posts from /mnt/sodyonstack/data/stackoverflow/csv/Posts.csv CSV HEADER DELIMITER ',' QUOTE '"' ESCAPE '"';
#+END_SRC

#+RESULTS:
| DELETE 0      |
|---------------|
| COPY 54741615 |


* DONE Comments

** DONE Create the schema  
   
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack"
DROP TABLE IF EXISTS comments;
CREATE TABLE comments (
	id SERIAL PRIMARY KEY,
	post_id INTEGER NOT NULL,
	user_id INTEGER,
	score SMALLINT NOT NULL,
	content_license VARCHAR(64) NOT NULL,
	user_display_name VARCHAR(64),
	text TEXT,
	creation_date TIMESTAMP NOT NULL
);
 #+END_SRC

 #+RESULTS:
 | DROP TABLE   |
 |--------------|
 | CREATE TABLE |


** DONE Load the data
     
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  DELETE FROM comments;
  \COPY comments from /mnt/sodyonstack/data/stackoverflow/csv/Comments.csv CSV HEADER DELIMITER ',' QUOTE '"' ESCAPE '"';
#+END_SRC

#+RESULTS:
| DELETE 0      |
|---------------|
| COPY 83160601 |


* OPTIONAL Remove csv to save disk

#+BEGIN_SRC shell :async
rm -rf data/stackoverflow/csv/*
#+END_SRC

#+RESULTS:

