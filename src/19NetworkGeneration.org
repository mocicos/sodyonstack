#+TITLE: Notebook for generating the pioneer network for R (2008-2009) and for Pandas (2011)
#+AUTHOR: Maxime MORGE
#+DATE: <2023-01-10 Tue>

* Reminder

As a reminder, PostgresQL can be locally started with the following command line:

#+BEGIN_SRC sh
psql -d rallstar
#+END_SRC

As a reminder, when the server is woke up, PostgresQL can be remotely started
with the following command line:

#+BEGIN_SRC sh
  ssh -i ~/.ssh/MoCiCoS.pem ubuntu@193.54.101.85
  psql -U ubuntu sodyonstack
#+END_SRC


* Objectives

Create a pioneer network for each community (R/Pandas) in each directory :
- data/RNetwork
- data/PandsNetwork

Each network is described with 2 files:
- edgesR.csv | edgesPandas.csv for the list of relations of the community
- nodesR.csv | edgesPandas.csv for the list of nodes of the community

** Edges

There is a relation from a source to a destination iff the source user answers
(DWH_CONTRIBUTES.dwh_kind_of_id = 2) at least once to a question of the destination user.

There is no loop, i.e an edge that joins a vertex to itself.

The relations are store in a CSV file with one relation per line and the following header

#+BEGIN_SRC csv
Source,Target,Weight,Type,Community
#+END_SRC

Where:
  - Source is the dwh_user_id of the helper, i.e. DWH_Contributes.dwh_author_id of an answer
  - Target is the dwh_user_id of the asker, i.e. DWH_Contributes.dwh_interlocutor_id of an answer
  - Weight is the number of answers from the helper to the asker
  - Type is "directed" since the graph is directed
  - Community is "r" or "pandas" if DWH_Contributes.dwh_topic_id=1 or DWH_Contributes.dwh_topic_id=2

** Nodes

A node is a user involved in the community

#+BEGIN_SRC csv
Id,Label,Reputation,Community,Zscore,MEC,Entrance_year,Exit_year
#+END_SRC

Where:
- Id is the dwh_user_id of the member, i.e. DWH_Membership.dwh_member_id
- Label is the full name on SO, i.e DWH_User.display_name
- Reputation is a rough measurement of how much SO trusts the user i.e. DWH_User.reputation
- Community is "r" or "pandas" if DWH_Content.tags LIKE '%<r>%' or '%<pandas>%' respectively
- Zscore is postive if the member is a responder and negative otherwise during the community life, i.e. DWH_Membership.z_score
- MEC is a measure of the expertise of the member during the community life, i.e. DWH_Membership.mec
- Entrance_year is the year of the member's first contribution
  or answer) in the community, i.e EXTRACT(year FROM DWH_Membership.first_post_date)
- Exit_year is the year of the member's first contribution
  or answer) in the community, i.e. EXTRACT(year FROM DWH_Membership.last_post_date)


* DONE R Community

** DONE R Edges

Extract the data

964 advice links for the first year over 399 820 in the  R community

#+BEGIN_SRC sql :engine postgresql :cmdline "-d rallstar" :async
  WITH Advice AS (
    SELECT dwh_author_id AS Source,
           dwh_interlocutor_id AS Target
    FROM DWH_Contributes
    JOIN DWH_Content
      ON DWH_Contributes.dwh_reply_id = DWH_Content.dwh_content_id
    WHERE DWH_Content.tags LIKE '%<r>%'
    AND dwh_kind_of_id =2 -- answer
    AND dwh_author_id != dwh_interlocutor_id -- no loop
    AND EXTRACT(year FROM DWH_Contributes.stamp ) <= 2009 -- pioneer
      )
  SELECT Advice.Source,
         Advice.Target,
         COUNT(*) AS Weight,
         -- COUNT(DISTINCT (Advice.Source||' '||Advice.Target)) AS Weight,
         '"directed"' AS Type,
         '"r"' AS Community
    FROM Advice
   GROUP BY Advice.Source, Advice.Target
   ORDER BY Advice.Source, Advice.Target ASC;
#+END_SRC

#+RESULTS:
: f75280921264acdb265e31417ad76b7c

Export the data (Use COPY instead of \COPY: the COPY command is a server command
executed in the server, and \COPY is a psql command with the same interface. So
while \COPY does not support multi-line queries, COPY does!)

#+BEGIN_SRC sql :engine postgresql :cmdline "-d rallstar" :async
  COPY(
    WITH Advice AS (
      SELECT dwh_author_id AS Source,
             dwh_interlocutor_id AS Target
        FROM DWH_Contributes
             JOIN DWH_Content
                 ON DWH_Contributes.dwh_reply_id = DWH_Content.dwh_content_id
       WHERE DWH_Content.tags LIKE '%<r>%'
         AND dwh_kind_of_id =2 -- answer
         AND dwh_author_id != dwh_interlocutor_id -- no loop
         AND EXTRACT(year FROM DWH_Contributes.stamp ) <= 2009 -- pioneer
    )
    SELECT Advice.Source,
           Advice.Target,
           COUNT(DISTINCT (Advice.Source||' '||Advice.Target)) AS Weight,
           'directed' AS Type,
           'r' AS Community
      FROM Advice
     GROUP BY Advice.Source, Advice.Target
     ORDER BY Advice.Source, Advice.Target ASC
  ) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/RNetwork/2009/edgesR2009.csv' WITH (DELIMITER ',', FORCE_QUOTE(Type,Community), FORMAT CSV, HEADER);
#+END_SRC

 #+RESULTS:
 | COPY 964 |
 |----------|

** DONE R Nodes

Extract the data

411 nodes

#+BEGIN_SRC sql :engine postgresql :cmdline "-d rallstar" :async
  WITH Advice AS (
    SELECT dwh_author_id AS Source,
           dwh_interlocutor_id AS Target
    FROM DWH_Contributes
    JOIN DWH_Content
      ON DWH_Contributes.dwh_reply_id = DWH_Content.dwh_content_id
    WHERE DWH_Content.tags LIKE '%<r>%'
    AND dwh_kind_of_id =2 -- answer
    AND dwh_author_id != dwh_interlocutor_id -- no loop
    AND EXTRACT(year FROM DWH_Contributes.stamp ) <= 2009 -- pioneer
      ),
    Node AS (
      SELECT Advice.Source AS Id
        FROM Advice
       UNION
      SELECT Advice.Target AS Id
        FROM Advice)
  SELECT Node.Id AS Id,
         DWH_User.display_name AS Label,
         DWH_User.reputation AS Reputation,
         'r' AS Community,
         DWH_Membership.z_score AS Zscore,
         DWH_Membership.mec AS MEC,
         EXTRACT(year FROM DWH_Membership.first_post_date) AS Entrance_year,
         EXTRACT(year FROM DWH_Membership.last_post_date) AS Exit_year
    FROM Node
         JOIN DWH_User
             ON Node.Id = DWH_User.dwh_user_id
         JOIN DWH_Membership
             ON Node.Id = DWH_Membership.dwh_member_id
   WHERE DWH_Membership.dwh_topic_id = 1 -- R
   ORDER BY Node.Id ASC;
#+END_SRC

Export the data

#+BEGIN_SRC sql :engine postgresql :cmdline "-d rallstar" :async
  COPY(
      WITH Advice AS (
    SELECT dwh_author_id AS Source,
           dwh_interlocutor_id AS Target
    FROM DWH_Contributes
    JOIN DWH_Content
      ON DWH_Contributes.dwh_reply_id = DWH_Content.dwh_content_id
    WHERE DWH_Content.tags LIKE '%<r>%'
    AND dwh_kind_of_id =2 -- answer
    AND dwh_author_id != dwh_interlocutor_id -- no loop
    AND EXTRACT(year FROM DWH_Contributes.stamp ) <= 2009 -- pioneer
      ),
    Node AS (
      SELECT Advice.Source AS Id
        FROM Advice
       UNION
      SELECT Advice.Target AS Id
        FROM Advice)
  SELECT Node.Id AS Id,
         DWH_User.display_name AS Label,
         DWH_User.reputation AS Reputation,
         'r' AS Community,
         DWH_Membership.z_score AS Zscore,
         DWH_Membership.mec AS MEC,
         EXTRACT(year FROM DWH_Membership.first_post_date) AS Entrance_year,
         EXTRACT(year FROM DWH_Membership.last_post_date) AS Exit_year
    FROM Node
         JOIN DWH_User
             ON Node.Id = DWH_User.dwh_user_id
         JOIN DWH_Membership
             ON Node.Id = DWH_Membership.dwh_member_id
   WHERE DWH_Membership.dwh_topic_id = 1 -- R
   ORDER BY Node.Id ASC
  ) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/RNetwork/2009/nodesR2009.csv' WITH (DELIMITER ',', FORCE_QUOTE(Label,Community), FORMAT CSV, HEADER);
#+END_SRC

#+RESULTS:
| COPY 411 |
|----------|

** DONE Pandas edges

875 edges

#+BEGIN_SRC sql :engine postgresql :cmdline "-d rallstar" :async
  COPY(
    WITH Advice AS (
      SELECT dwh_author_id AS Source,
             dwh_interlocutor_id AS Target
        FROM DWH_Contributes
             JOIN DWH_Content
                 ON DWH_Contributes.dwh_reply_id = DWH_Content.dwh_content_id
       WHERE DWH_Content.tags LIKE '%<pandas>%'
         AND dwh_kind_of_id =2 -- answer
         AND dwh_author_id != dwh_interlocutor_id -- no loop
         AND EXTRACT(year FROM DWH_Contributes.stamp ) <= 2012 -- pioneer
    )
    SELECT Advice.Source,
           Advice.Target,
           COUNT(DISTINCT (Advice.Source||' '||Advice.Target)) AS Weight,
           'directed' AS Type,
           'pandas' AS Community
      FROM Advice
     GROUP BY Advice.Source, Advice.Target
     ORDER BY Advice.Source, Advice.Target ASC
  ) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/PandasNetwork/2012/edgesR2012.csv' WITH (DELIMITER ',', FORCE_QUOTE(Type,Community), FORMAT CSV, HEADER);
#+END_SRC


** DONE Pandas nodes

569 nodes

#+BEGIN_SRC sql :engine postgresql :cmdline "-d rallstar" :async
  COPY(
      WITH Advice AS (
    SELECT dwh_author_id AS Source,
           dwh_interlocutor_id AS Target
    FROM DWH_Contributes
    JOIN DWH_Content
      ON DWH_Contributes.dwh_reply_id = DWH_Content.dwh_content_id
    WHERE DWH_Content.tags LIKE '%<pandas>%'
    AND dwh_kind_of_id =2 -- answer
    AND dwh_author_id != dwh_interlocutor_id -- no loop
    AND EXTRACT(year FROM DWH_Contributes.stamp ) <= 2012 -- pioneer
      ),
    Node AS (
      SELECT Advice.Source AS Id
        FROM Advice
       UNION
      SELECT Advice.Target AS Id
        FROM Advice)
  SELECT Node.Id AS Id,
         DWH_User.display_name AS Label,
         DWH_User.reputation AS Reputation,
         'pandas' AS Community,
         DWH_Membership.z_score AS Zscore,
         DWH_Membership.mec AS MEC,
         EXTRACT(year FROM DWH_Membership.first_post_date) AS Entrance_year,
         EXTRACT(year FROM DWH_Membership.last_post_date) AS Exit_year
    FROM Node
         JOIN DWH_User
             ON Node.Id = DWH_User.dwh_user_id
         JOIN DWH_Membership
             ON Node.Id = DWH_Membership.dwh_member_id
   WHERE DWH_Membership.dwh_topic_id = 2 -- Pandas
   ORDER BY Node.Id ASC
  ) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/PandasNetwork/2012/nodesR2012.csv' WITH (DELIMITER ',', FORCE_QUOTE(Label,Community), FORMAT CSV, HEADER);
#+END_SRC




** DONE R Community generate by year from 2010 until 2019

See documentation:
- https://docs.postgresql.fr/9.6/plpgsql-control-structures.html
- https://www.postgresqltutorial.com/postgresql-plpgsql/

*** DONE R edges

#+BEGIN_SRC sql :engine postgresql :cmdline "-d rallstar" :async
  do $$
  declare
    annee_courante integer;
  begin
    for annee_courante in 2010 .. 2019 loop    
      execute '
              COPY(
              WITH Advice AS (
              SELECT dwh_author_id AS Source,
              dwh_interlocutor_id AS Target
              FROM DWH_Contributes
              JOIN DWH_Content
              ON DWH_Contributes.dwh_reply_id = DWH_Content.dwh_content_id
              WHERE DWH_Content.tags LIKE ''%<r>%''
              AND dwh_kind_of_id = 2 -- answer
              AND dwh_author_id != dwh_interlocutor_id -- no loop
              AND EXTRACT(year FROM DWH_Contributes.stamp ) = ' || annee_courante || ' 
                                                                                     )
                                                                                     SELECT Advice.Source,
                                                                                     Advice.Target,
                                                                                     COUNT(*) AS Weight,
                                                                                     ''directed'' AS Type,
                                                                                     ''r'' AS Community
                                                                                     FROM Advice
                                                                                     GROUP BY Advice.Source, Advice.Target
                                                                                     ORDER BY Advice.Source, Advice.Target ASC
                                                                                     ) TO ''/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/RNetwork/' || annee_courante || '/edgesR' || annee_courante || '.csv'' WITH (DELIMITER '','', FORCE_QUOTE(Type,Community), FORMAT CSV, HEADER);
                                                                                                                                                                                                  ';
      -- display a notice
      raise notice 'Edge: %', annee_courante;
    end loop;
  end$$ LANGUAGE plpgsql;
#+END_SRC

#+RESULTS:
| DO |
|----|


*** DONE R nodes

#+BEGIN_SRC sql :engine postgresql :cmdline "-d rallstar" :async
  do $$
  declare
    annee_courante integer;
  begin
    for annee_courante in 2009 .. 2019 loop    
      execute '
  COPY(
      WITH Advice AS (
    SELECT dwh_author_id AS Source,
           dwh_interlocutor_id AS Target
    FROM DWH_Contributes
    JOIN DWH_Content
      ON DWH_Contributes.dwh_reply_id = DWH_Content.dwh_content_id
    WHERE DWH_Content.tags LIKE ''%<r>%''
    AND dwh_kind_of_id =2 -- answer
    AND dwh_author_id != dwh_interlocutor_id -- no loop
    AND EXTRACT(year FROM DWH_Contributes.stamp ) = ' || annee_courante || ' -- pioneer
      ),
    Node AS (
      SELECT Advice.Source AS Id
        FROM Advice
       UNION
      SELECT Advice.Target AS Id
        FROM Advice)
  SELECT Node.Id AS Id,
         DWH_User.display_name AS Label,
         DWH_User.reputation AS Reputation,
         ''r'' AS Community,
         DWH_Membership.z_score AS Zscore,
         DWH_Membership.mec AS MEC,
         DWH_Membership.in_degree AS in_degree,
         DWH_Membership.out_degree AS out_degree,
         EXTRACT(year FROM DWH_Membership.first_post_date) AS Entrance_year,
         EXTRACT(year FROM DWH_Membership.last_post_date) AS Exit_year
    FROM Node
         JOIN DWH_User
             ON Node.Id = DWH_User.dwh_user_id
         JOIN DWH_Membership
             ON Node.Id = DWH_Membership.dwh_member_id
   WHERE DWH_Membership.dwh_topic_id = 1 -- R
   ORDER BY Node.Id ASC
  ) TO ''/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/RNetwork/' || annee_courante || '/nodesR' || annee_courante || '.csv'' WITH (DELIMITER '','', FORCE_QUOTE(Label,Community), FORMAT CSV, HEADER);
                                                                                                                   ';
      -- display a notice
      raise notice 'Nodes: %', annee_courante;
    end loop;
  end$$ LANGUAGE plpgsql;
#+END_SRC

#+RESULTS:
| DO |
|----|



** DONE Pandas Community

*** DONE Pandas Edges

#+BEGIN_SRC sql :engine postgresql :cmdline "-d rallstar" :async
  do $$
  declare
    annee_courante integer;
  begin
    for annee_courante in 2012 .. 2019 loop    
      execute '
              COPY(
              WITH Advice AS (
              SELECT dwh_author_id AS Source,
              dwh_interlocutor_id AS Target
              FROM DWH_Contributes
              JOIN DWH_Content
              ON DWH_Contributes.dwh_reply_id = DWH_Content.dwh_content_id
              WHERE DWH_Content.tags LIKE ''%<pandas>%''
              AND dwh_kind_of_id = 2 -- answer
              AND dwh_author_id != dwh_interlocutor_id -- no loop
              AND EXTRACT(year FROM DWH_Contributes.stamp ) = ' || annee_courante || ' 
                                                                                     )
                                                                                     SELECT Advice.Source,
                                                                                     Advice.Target,
                                                                                     COUNT(*) AS Weight,
                                                                                     ''directed'' AS Type,
                                                                                     ''pandas'' AS Community
                                                                                     FROM Advice
                                                                                     GROUP BY Advice.Source, Advice.Target
                                                                                     ORDER BY Advice.Source, Advice.Target ASC
                                                                                     ) TO ''/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/PandasNetwork/' || annee_courante || '/edgesPandas' || annee_courante || '.csv'' WITH (DELIMITER '','', FORCE_QUOTE(Type,Community), FORMAT CSV, HEADER);
                                                                                                                                                                                                  ';
      -- display a notice
      raise notice 'Edge: %', annee_courante;
    end loop;
  end$$ LANGUAGE plpgsql;
#+END_SRC

#+RESULTS:
| DO |
|----|

*** DONE Pandas Nodes

#+BEGIN_SRC sql :engine postgresql :cmdline "-d rallstar" :async
  do $$
  declare
    annee_courante integer;
  begin
    for annee_courante in 2012 .. 2019 loop    
      execute '
  COPY(
      WITH Advice AS (
    SELECT dwh_author_id AS Source,
           dwh_interlocutor_id AS Target
    FROM DWH_Contributes
    JOIN DWH_Content
      ON DWH_Contributes.dwh_reply_id = DWH_Content.dwh_content_id
    WHERE DWH_Content.tags LIKE ''%<pandas>%''
    AND dwh_kind_of_id =2 -- answer
    AND dwh_author_id != dwh_interlocutor_id -- no loop
    AND EXTRACT(year FROM DWH_Contributes.stamp ) = ' || annee_courante || ' -- pioneer
      ),
    Node AS (
      SELECT Advice.Source AS Id
        FROM Advice
       UNION
      SELECT Advice.Target AS Id
        FROM Advice)
  SELECT Node.Id AS Id,
         DWH_User.display_name AS Label,
         DWH_User.reputation AS Reputation,
         ''pandas'' AS Community,
         DWH_Membership.z_score AS Zscore,
         DWH_Membership.mec AS MEC,
         DWH_Membership.in_degree AS in_degree,
         DWH_Membership.out_degree AS out_degree,
         EXTRACT(year FROM DWH_Membership.first_post_date) AS Entrance_year,
         EXTRACT(year FROM DWH_Membership.last_post_date) AS Exit_year
    FROM Node
         JOIN DWH_User
             ON Node.Id = DWH_User.dwh_user_id
         JOIN DWH_Membership
             ON Node.Id = DWH_Membership.dwh_member_id
   WHERE DWH_Membership.dwh_topic_id = 2 -- Pandas
   ORDER BY Node.Id ASC
  ) TO ''/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/PandasNetwork/' || annee_courante || '/nodesPandas' || annee_courante || '.csv'' WITH (DELIMITER '','', FORCE_QUOTE(Label,Community), FORMAT CSV, HEADER);
                                                                                                                   ';
      -- display a notice
      raise notice 'Nodes: %', annee_courante;
    end loop;
  end$$ LANGUAGE plpgsql;
#+END_SRC

#+RESULTS:
| DO |
|----|
