#+TITLE: Notebook for describing the data with some statistics
#+AUTHOR: Maxime MORGE Audrey DEWAELE 
#+DATE: <2021-02-19 Fri>

* Source Database and relations size

  Database size
  
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
SELECT pg_size_pretty(pg_database_size('sodyonstack')) AS DBSize;
#+END_SRC

#+RESULTS:
| DBSize  |
|---------|
| 78 GB   |

  Relations size

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
SELECT pg_size_pretty(pg_relation_size(('Posts'))) AS Posts,
       pg_size_pretty(pg_relation_size(('Comments'))) AS Comments,
       pg_size_pretty(pg_relation_size(('Badges'))) AS Badges,
      pg_size_pretty(pg_relation_size(('Users'))) AS Users,
      pg_size_pretty(pg_relation_size(('Tags'))) AS Tags;
#+END_SRC


#+RESULTS:
| posts | comments | badges  | users   | tags    |
|-------+----------+---------+---------+---------|
| 46 GB | 18 GB    | 2558 MB | 3046 MB | 3520 kB |


  Relations size with index


#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  SELECT pg_size_pretty(pg_total_relation_size(('Posts'))) AS Posts,
         pg_size_pretty(pg_total_relation_size(('Comments'))) AS Comments,
         pg_size_pretty(pg_total_relation_size(('Badges'))) AS Badges,
         pg_size_pretty(pg_total_relation_size(('Users'))) AS Users,
         pg_size_pretty(pg_total_relation_size(('Tags'))) AS Tags;
#+END_SRC

#+RESULTS:
| posts | comments | badges  |  users  |  tags
| -------+----------+---------+---------+---------
| 52 GB  | 19 GB    | 3472 MB | 3411 MB | 4920 kB

[[https://www.bortzmeyer.org/postgresql-taille-bases.html][En savoir plus.]]

* Tags

** SAS
  
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack"
  SELECT tag_name, count
  FROM tags
  WHERE tag_name='sas'
  ORDER BY count DESC;
#+END_SRC

#+RESULTS:
| tag_name | count |
|----------+-------|
| sas      | 14023 |


** R

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack"
    SELECT tag_name, count
    FROM tags
    WHERE tag_name='r'
    OR tag_name='ggplot2' OR tag_name='r-caret' OR tag_name='mlr'
    ORDER BY count DESC;
#+END_SRC

#+RESULTS:
| tag_name |  count |
|----------+--------|
| r        | 428733 |
| ggplot2  |  44995 |
| r-caret  |   1979 |
| mlr      |    313 |

** Python

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" 
  SELECT tag_name, count
    FROM tags
   WHERE tag_name='python' OR tag_name='pandas' OR tag_name='numpy' OR tag_name='scipy'
      OR tag_name='matplotlib' OR tag_name='bokeh' OR tag_name='plotly'
      OR tag_name='scikit-learn';
#+END_SRC

#+RESULTS:
| tag_name     |   count |
|--------------+---------|
| python       | 1845706 |
| numpy        |   96647 |
| matplotlib   |   61893 |
| scipy        |   18895 |
| pandas       |  224971 |
| scikit-learn |   24757 |
| plotly       |   10139 |
| bokeh        |    4403 |

** Hadoop

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" 
  SELECT tag_name, count
    FROM tags
   WHERE tag_name='hadoop';
#+END_SRC

#+RESULTS
| tag_name | count |
|----------+-------|
| hadoop   | 43583 |


** Hive

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" 
  SELECT tag_name, count
    FROM tags
   WHERE tag_name='hive';
#+END_SRC

#+RESULTS
| tag_name | count |
|----------+-------|
| hive     | 20607 |


** Pig

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" 
  SELECT tag_name, count
    FROM tags
   WHERE tag_name='apache-pig';
#+END_SRC

#+RESULTS
| tag_name   | count |
|------------+-------|
| apache-pig |  5185 |


** Spark

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" 
  SELECT tag_name, count
    FROM tags
   WHERE tag_name='apache-spark';
#+END_SRC

#+RESULTS
|   tag_name   | count |
|--------------+-------|
| apache-spark | 71932 |

   
** Sample tags in Posts

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async 
  SELECT tags
    FROM Posts
   LIMIT 10;
#+END_SRC

#+RESULTS:
| tags                                                   |
|--------------------------------------------------------|
| <c++><directx-11><cgal>                                |
| <java>                                                 |
| <r><classification><party><predictive>                 |
|                                                        |
|                                                        |
| <node.js><apache><express><http-status-code-404>       |
|                                                        |
|                                                        |
|                                                        |
|                                                        |



#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async 
  SELECT replace(replace(replace(tags,'><',','),'<',''),'>','') AS tag 
    FROM Posts
   LIMIT 10;
#+END_SRC

#+RESULTS:
| tag                                                   |
|-------------------------------------------------------|
| c++,directx-11,cgal                                   |
| java                                                  |
| r,classification,party,predictive                     |
|                                                       |
|                                                       |
| node.js,apache,express,http-status-code-404           |
|                                                       |
|                                                       |
|                                                       |
|                                                       |



** Sample year in DATE

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async 
    SELECT DISTINCT EXTRACT( YEAR from creation_date) AS year 
      FROM posts;
#+END_SRC

#+RESULTS:
| year |
|------|
| 2008 |
| 2009 |
| 2010 |
| 2011 |
| 2012 |
| 2013 |
| 2014 |
| 2015 |
| 2016 |
| 2017 |
| 2018 |
| 2019 |
| 2020 |
| 2021 |

* DONE The number of posts with the tag <hadoop> 

♠  The number of inquiry with the tag <hadoop>
  
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(id)
     FROM Posts
    WHERE tags LIKE '%<hadoop>%' AND post_type_id=1;
 #+END_SRC
 
 #+RESULTS:
 : 43 582

It is worth noticing that the solution has no tag

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(id)
     FROM Posts
    WHERE tags LIKE '%<hadoop>%' AND (post_type_id=2);
 #+END_SRC
 
 #+RESULTS:
 : 0

The number of solutions for the inquiry with the tag <hadoop>
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(Solution.id)
     FROM Posts AS Inquiry INNER JOIN Posts AS Solution ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<hadoop>%' AND Solution.post_type_id=2;
 #+END_SRC

 #+RESULTS: 
 : 54 695
 
 The number of users for the inquiries with the tag <hadoop>
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT Users.id)
     FROM Users
            INNER JOIN Posts AS Inquiry ON Inquiry.owner_user_id = Users.id
    WHERE Inquiry.tags LIKE '%<hadoop>%';
 #+END_SRC

 #+RESULTS: 
 : 23 266

 The number of users the solutions of the inquiries with the tag <hadoop>

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT Users.id)
     FROM Users
            INNER JOIN Posts AS Solution ON Solution.owner_user_id = Users.id
            INNER JOIN Posts AS Inquiry ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<hadoop>%' AND Solution.post_type_id=2;
 #+END_SRC

 #+RESULTS: 
 : 19 490

 
 The number of users for the inquiries with the tag <hadoop>
 and the solutions for these inquiry
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT id) FROM (
   SELECT Users.id
     FROM Users
            INNER JOIN Posts AS Inquiry ON Inquiry.owner_user_id = Users.id
    WHERE Inquiry.tags LIKE '%<hadoop>%'
   UNION
   SELECT Users.id
     FROM Users
            INNER JOIN Posts AS Solution ON Solution.owner_user_id = Users.id
            INNER JOIN Posts AS Inquiry ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<hadoop>%' AND Solution.post_type_id=2
          ) AS all_users;
 #+END_SRC

 #+RESULTS: 
:  36 477



* DONE The number of posts with the tag <hive>


♠  The number of inquiry with the tag <hive>
  
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(id)
     FROM Posts
    WHERE tags LIKE '%<hive>%' AND post_type_id=1;
 #+END_SRC
 
 #+RESULTS:
 : 20 607

It is worth noticing that the solution has no tag

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(id)
     FROM Posts
    WHERE tags LIKE '%<hive>%' AND (post_type_id=2);
 #+END_SRC
 
 #+RESULTS:
 : 0

The number of solutions for the inquiry with the tag <hive>
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(Solution.id)
     FROM Posts AS Inquiry INNER JOIN Posts AS Solution ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<hive>%' AND Solution.post_type_id=2;
 #+END_SRC

 #+RESULTS: 
 : 24 611
 
 The number of users for the inquiries with the tag <hive>
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT Users.id)
     FROM Users
            INNER JOIN Posts AS Inquiry ON Inquiry.owner_user_id = Users.id
    WHERE Inquiry.tags LIKE '%<hive>%';
 #+END_SRC

 #+RESULTS: 
 : 11 775

 The number of users the solutions of the inquiries with the tag <hive>

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT Users.id)
     FROM Users
            INNER JOIN Posts AS Solution ON Solution.owner_user_id = Users.id
            INNER JOIN Posts AS Inquiry ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<hive>%' AND Solution.post_type_id=2;
 #+END_SRC

 #+RESULTS: 
 : 9 144

 
  The number of users for the inquiries with the tag <hive>
  and the solutions for these inquiry
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT id) FROM (
   SELECT Users.id
     FROM Users
            INNER JOIN Posts AS Inquiry ON Inquiry.owner_user_id = Users.id
    WHERE Inquiry.tags LIKE '%<hive>%'
   UNION
   SELECT Users.id
     FROM Users
            INNER JOIN Posts AS Solution ON Solution.owner_user_id = Users.id
            INNER JOIN Posts AS Inquiry ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<hive>%' AND Solution.post_type_id=2
          ) AS all_users;
 #+END_SRC

 #+RESULTS: 
:  18 167



* DONE The number of posts with the tag <apache-pig>


♠  The number of inquiry with the tag <apache-pig>
  
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(id)
     FROM Posts
    WHERE tags LIKE '%<apache-pig>%' AND post_type_id=1;
 #+END_SRC
 
 #+RESULTS:
 : 5 185

It is worth noticing that the solution has no tag

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(id)
     FROM Posts
    WHERE tags LIKE '%<apache-pig>%' AND (post_type_id=2);
 #+END_SRC
 
 #+RESULTS:
 : 0

The number of solutions for the inquiry with the tag <apache-pig>
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(Solution.id)
     FROM Posts AS Inquiry INNER JOIN Posts AS Solution ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<apache-pig>%' AND Solution.post_type_id=2;
 #+END_SRC

 #+RESULTS: 
 : 6 401
 
 The number of users for the inquiries with the tag <apache-pig>
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT Users.id)
     FROM Users
            INNER JOIN Posts AS Inquiry ON Inquiry.owner_user_id = Users.id
    WHERE Inquiry.tags LIKE '%<apache-pig>%';
 #+END_SRC

 #+RESULTS: 
 : 3 120

 The number of users the solutions of the inquiries with the tag <apache-pig>

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT Users.id)
     FROM Users
            INNER JOIN Posts AS Solution ON Solution.owner_user_id = Users.id
            INNER JOIN Posts AS Inquiry ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<apache-pig>%' AND Solution.post_type_id=2;
 #+END_SRC

 #+RESULTS: 
 : 2 282

 
  The number of users for the inquiries with the tag <apache-pig>
 and the solutions for these inquiry
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT id) FROM (
   SELECT Users.id
     FROM Users
            INNER JOIN Posts AS Inquiry ON Inquiry.owner_user_id = Users.id
    WHERE Inquiry.tags LIKE '%<apache-pig>%'
   UNION
   SELECT Users.id
     FROM Users
            INNER JOIN Posts AS Solution ON Solution.owner_user_id = Users.id
            INNER JOIN Posts AS Inquiry ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<apache-pig>%' AND Solution.post_type_id=2
          ) AS all_users;
 #+END_SRC

 #+RESULTS: 
:  4 686


* DONE The number of posts with the tag <apache-spark>
 

♠  The number of inquiry with the tag <apache-spark>
  
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(id)
     FROM Posts
    WHERE tags LIKE '%<apache-spark>%' AND post_type_id=1;
 #+END_SRC
 
 #+RESULTS:
 : 71 932

It is worth noticing that the solution has no tag

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(id)
     FROM Posts
    WHERE tags LIKE '%<apache-spark>%' AND (post_type_id=2);
 #+END_SRC
 
 #+RESULTS:
 : 0

The number of solutions for the inquiry with the tag <apache-spark>
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(Solution.id)
     FROM Posts AS Inquiry INNER JOIN Posts AS Solution ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<apache-spark>%' AND Solution.post_type_id=2;
 #+END_SRC

 #+RESULTS: 
 : 82 515
 
 The number of users for the inquiries with the tag <apache-spark>
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT Users.id)
     FROM Users
            INNER JOIN Posts AS Inquiry ON Inquiry.owner_user_id = Users.id
    WHERE Inquiry.tags LIKE '%<apache-spark>%';
 #+END_SRC

 #+RESULTS: 
 : 31 019

 The number of users the solutions of the inquiries with the tag <apache-spark>

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT Users.id)
     FROM Users
            INNER JOIN Posts AS Solution ON Solution.owner_user_id = Users.id
            INNER JOIN Posts AS Inquiry ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<apache-spark>%' AND Solution.post_type_id=2;
 #+END_SRC

 #+RESULTS: 
 : 21 903

 
  The number of users for the inquiries with the tag <apache-spark>
 and the solutions for these inquiry
 
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   SELECT COUNT(DISTINCT id) FROM (
   SELECT Users.id
     FROM Users
            INNER JOIN Posts AS Inquiry ON Inquiry.owner_user_id = Users.id
    WHERE Inquiry.tags LIKE '%<apache-spark>%'
   UNION
   SELECT Users.id
     FROM Users
            INNER JOIN Posts AS Solution ON Solution.owner_user_id = Users.id
            INNER JOIN Posts AS Inquiry ON Inquiry.id = Solution.parent_id
    WHERE Inquiry.tags LIKE '%<apache-spark>%' AND Solution.post_type_id=2
          ) AS all_users;
 #+END_SRC

 #+RESULTS: 
:  44 037
