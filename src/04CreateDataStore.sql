-- Copyright Maxime MORGE, 2020, 2022
CREATE DATABASE SODYONSTACK;
\c SODYONSTACK;

DROP TABLE IF EXISTS SODY_USER CASCADE;
CREATE TABLE SODY_USER (
  id_sody_user SERIAL,
  id_user SERIAL,
  account_id INTEGER,
  reputation INTEGER,
  views INTEGER,
  down_votes INTEGER,
  up_votes INTEGER,
  display_name VARCHAR(255),
  location VARCHAR(512),
  website_url VARCHAR(255),
  about_me TEXT,
  creation_date TIMESTAMP,
  last_access_date TIMESTAMP,
  PRIMARY KEY (id_sody_user)
);

DROP TABLE IF EXISTS SODY_INQUIRY CASCADE;
CREATE TABLE SODY_INQUIRY (
  id_sody_inquiry SERIAL,
  id_post SERIAL,
  accepted_answer_id INTEGER,
  score INTEGER,
  view_count INTEGER,
  answer_count INTEGER,
  comment_count INTEGER,
  title VARCHAR(512),
  tags VARCHAR(512),
  body TEXT,
  favorite_count INTEGER,
  creation_date TIMESTAMP,
  community_owned_date TIMESTAMP,
  closed_date TIMESTAMP,
  last_edit_date TIMESTAMP,
  last_activity_date TIMESTAMP,
  id_sody_user SERIAL,
  PRIMARY KEY (id_sody_inquiry)
);
ALTER TABLE SODY_INQUIRY ADD FOREIGN KEY (id_sody_user) REFERENCES SODY_USER (id_sody_user);

DROP TABLE IF EXISTS SODY_SOLUTION CASCADE;
CREATE TABLE SODY_SOLUTION (
  id_sody_solution SERIAL,
  id_post SERIAL,
  score INTEGER,
  view_count INTEGER,
  answer_count INTEGER,
  comment_count INTEGER,
  title VARCHAR(512),
  tags VARCHAR(512),
  body TEXT,
  favorite_count INTEGER,
  creation_date TIMESTAMP,
  community_owned_date TIMESTAMP,
  closed_date TIMESTAMP,
  last_edit_date TIMESTAMP,
  last_activity_date TIMESTAMP,
  is_accepted BOOLEAN,
  id_sody_inquiry SERIAL,
  response_time BIGINT,
  id_sody_user SERIAL,
  PRIMARY KEY (id_sody_solution)
);
ALTER TABLE SODY_SOLUTION ADD FOREIGN KEY (id_sody_user) REFERENCES SODY_USER (id_sody_user);
ALTER TABLE SODY_SOLUTION ADD FOREIGN KEY (id_sody_inquiry) REFERENCES SODY_INQUIRY (id_sody_inquiry);

DROP TABLE IF EXISTS SODY_COMMENT_INQUIRY CASCADE;
CREATE TABLE SODY_COMMENT_INQUIRY (
  id_sody_comment_inquiry SERIAL,
	id_comment SERIAL,
	post_id INTEGER NOT NULL,
	user_id INTEGER,
	score SMALLINT NOT NULL,
	user_display_name VARCHAR(64),
	text TEXT,
	creation_date TIMESTAMP NOT NULL,
  response_time BIGINT,
  id_sody_user SERIAL,
  id_sody_inquiry SERIAL,
  Primary KEY (id_sody_comment_inquiry)
);
ALTER TABLE SODY_COMMENT_INQUIRY ADD FOREIGN KEY (id_sody_user) REFERENCES SODY_USER (id_sody_user);
ALTER TABLE SODY_COMMENT_INQUIRY ADD FOREIGN KEY (id_sody_inquiry) REFERENCES SODY_INQUIRY (id_sody_inquiry);

DROP TABLE IF EXISTS SODY_COMMENT_SOLUTION CASCADE;
CREATE TABLE SODY_COMMENT_SOLUTION (
  id_sody_comment_solution SERIAL,
	id_comment SERIAL,
	post_id INTEGER NOT NULL,
	user_id INTEGER,
	score SMALLINT NOT NULL,
	user_display_name VARCHAR(64),
	text TEXT,
	creation_date TIMESTAMP NOT NULL,
  response_time BIGINT,
  id_sody_user SERIAL,
  id_sody_solution SERIAL,
  PRIMARY KEY (id_sody_comment_solution)
);
ALTER TABLE SODY_COMMENT_SOLUTION ADD FOREIGN KEY (id_sody_user) REFERENCES SODY_USER (id_sody_user);
ALTER TABLE SODY_COMMENT_SOLUTION ADD FOREIGN KEY (id_sody_solution) REFERENCES SODY_SOLUTION (id_sody_solution);

DROP TABLE IF EXISTS SODY_COMMUNITY CASCADE;
CREATE TABLE SODY_COMMUNITY (
  id_sody_community SERIAL,
  tag_name VARCHAR(255),
  user_count INTEGER,
  inquiry_count INTEGER,
  solution_count INTEGER,
  comment_inquiry_count INTEGER,
  comment_solution_count INTEGER,
  creation_date TIMESTAMP,
  last_activity_date TIMESTAMP,
  PRIMARY KEY (id_sody_community)
);

DROP TABLE IF EXISTS SODY_BELONGS CASCADE;
CREATE TABLE SODY_BELONGS (
  id_sody_user SERIAL,
  id_sody_community SERIAL,
  PRIMARY KEY (id_sody_user, id_sody_community)
);
ALTER TABLE SODY_BELONGS ADD FOREIGN KEY (id_sody_community) REFERENCES SODY_COMMUNITY (id_sody_community);
ALTER TABLE SODY_BELONGS ADD FOREIGN KEY (id_sody_user) REFERENCES SODY_USER (id_sody_user);


