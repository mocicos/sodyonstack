#+TITLE: Notebook for feeding the data warehouse from the operational data store
#+AUTHOR: Maxime MORGE
#+DATE:  <2021-03-25 Thu>

* DONE Feed the table DWH_PostType

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  DELETE FROM DWH_PostType;
  INSERT INTO DWH_PostType (post_type_code,post_type)--is_accepted_answer)
  VALUES
      (1,'Question'),
      (2,'Answer');
#+END_SRC

 #+RESULTS:
 | DELETE 0   |
 |------------|
 | INSERT 0 2 |

 
* DONE Feed the table DWH_Date

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  INSERT INTO DWH_Date (date, day, month, year, epoch_date)
    SELECT DISTINCT stamp,
                    To_Char(stamp, 'Day'),
                    To_Char(stamp, 'Month'),
                    EXTRACT(YEAR FROM Stamp),
                    EXTRACT(EPOCH FROM Stamp)
         FROM (
           SELECT DISTINCT creation_date AS stamp
             FROM SODY_Inquiry
            UNION
           SELECT DISTINCT creation_date AS stamp
             FROM SODY_Solution
            UNION
           SELECT DISTINCT last_edit_date AS stamp
             FROM SODY_Inquiry
            UNION
           SELECT DISTINCT last_edit_date AS stamp
             FROM SODY_Solution
            UNION
           SELECT DISTINCT last_activity_date AS stamp
             FROM SODY_Inquiry
            UNION
           SELECT DISTINCT last_activity_date AS stamp
             FROM SODY_Solution
            UNION
           SELECT DISTINCT creation_date AS stamp
             FROM SODY_COMMENT_INQUIRY
            UNION
           SELECT DISTINCT creation_date AS stamp
             FROM SODY_COMMENT_SOLUTION
         ) AS Stamp;
 #+END_SRC

 #+RESULTS:
: INSERT 0 690 151
 
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  UPDATE DWH_Date
     SET numberOfDay = EXTRACT(DAY FROM date),
         numberOfMonth = EXTRACT(MONTH FROM date),
         numberOfWeek = EXTRACT(WEEK FROM date);
#+END_SRC


 #+RESULTS:
: UPDATE 690 151

* DONE Feed the table DWH_Content

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  DELETE FROM DWH_Content;
  INSERT INTO DWH_Content(id_post, id_sody_inquiry, id_sody_solution, id_sody_parent, id_sody_user, title, tags, body)
  SELECT id_post, id_sody_inquiry, null, null, id_sody_user, title, tags, body
    FROM SODY_Inquiry;

  INSERT INTO DWH_Content(id_post, id_sody_inquiry, id_sody_solution, id_sody_parent, id_sody_user, title, tags, body)
  SELECT id_post, null, id_sody_solution, id_sody_inquiry, id_sody_user, title, tags, body
    FROM SODY_Solution;
#+END_SRC

#+RESULTS
: INSERT  265 347 = 121 777 questions +  143 570 answers

* DONE Feed the table DWH_User

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  INSERT INTO DWH_User(id_user, id_sody, account_id, reputation, views,
                       down_votes, up_votes, display_name,
                       location, website_url, about_me,
                       creation_date, last_access_date)
  SELECT id_user, id_sody_user, account_id, reputation, views,
         down_votes, up_votes, display_name,
         location, website_url, about_me,
         creation_date, last_access_date
    FROM SODY_User;
#+END_SRC

 #+RESULTS:
 : INSERT 0 75 887 users


* DONE Feed the table DWH_BODY

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  DELETE FROM DWH_BODY;
  INSERT INTO DWH_BODY(id_comment, id_sody_comment_inquiry, id_sody_comment_solution, post_id, id_sody_inquiry, id_sody_solution, id_sody_user, text)
  SELECT id_comment, id_sody_comment_inquiry, null, post_id, id_sody_inquiry, null, id_sody_user, text
    FROM SODY_COMMENT_INQUIRY
           UNION
  SELECT id_comment, null, id_sody_comment_solution, post_id, null, id_sody_solution, id_sody_user, text
    FROM SODY_COMMENT_SOLUTION;
#+END_SRC

#+RESULTS
: INSERT  0 308 126  = 161 098 comments on question + 147 028 comments on answer 

 
* DONE Feed the table Contributes
    
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  DELETE FROM DWH_Contributes;
  INSERT INTO DWH_Contributes(
    dwh_author_id, dwh_kind_of_id, dwh_creation_date_id, dwh_contains_id,
                              -- PRIMARY KEY
                              -- dwh_interlocutor_id, dwh_last_edit_date_id, dwh_last_activity_id, ,dwh_reply_id,
                              score, view_count, favorite_count, answer_count, comment_count,
                              delay, is_accepted, stamp
  )
  SELECT
    DWH_User.dwh_user_id,
    DWH_PostType.dwh_post_type_id,
    DWH_Date.dwh_date_id,
    DWH_Content.dwh_content_id,
    SODY_Inquiry.score,
    SODY_Inquiry.view_count,
    SODY_Inquiry.favorite_count,
    SODY_Inquiry.answer_count,
    SODY_Inquiry.comment_count,
    0,
    null,
    SODY_Inquiry.creation_date
    FROM
        DWH_PostType, SODY_Inquiry
        INNER JOIN DWH_User ON SODY_Inquiry.id_sody_user = DWH_User.id_sody
        INNER JOIN DWH_Date ON SODY_Inquiry.creation_date = DWH_Date.date
        INNER JOIN DWH_Content ON SODY_Inquiry.id_post = DWH_Content.id_post
   WHERE DWH_PostType.post_type_code = 1;
  -- INSERT 0 121 777

  INSERT INTO DWH_Contributes(
    dwh_author_id, dwh_kind_of_id, dwh_creation_date_id, dwh_contains_id,
                              -- PRIMARY KEY
                              -- dwh_interlocutor_id, dwh_last_edit_date_id, dwh_last_activity_id, ,dwh_reply_id,
                              score, view_count, favorite_count, answer_count, comment_count,
                              delay, is_accepted, stamp
  )
  SELECT
    DWH_User.dwh_user_id,
    DWH_PostType.dwh_post_type_id,
    DWH_Date.dwh_date_id,
    DWH_Content.dwh_content_id,
    SODY_Solution.score,
    SODY_Solution.view_count,
    SODY_Solution.favorite_count,
    SODY_Solution.answer_count,
    SODY_Solution.comment_count,
    SODY_Solution.response_time,
    SODY_Solution.is_accepted,
    SODY_Solution.creation_date
    FROM
        DWH_PostType, SODY_Solution
        INNER JOIN DWH_User ON SODY_Solution.id_sody_user = DWH_User.id_sody
        INNER JOIN DWH_Date ON SODY_Solution.creation_date = DWH_Date.date
        INNER JOIN DWH_Content ON SODY_Solution.id_post = DWH_Content.id_post
   WHERE DWH_PostType.post_type_code = 2;
  -- INSERT 0  143 570
#+END_SRC

#+RESULT
 : INSERT 121 777 questions
 : INSERT 143 570 answers

** DONE Add dwh_reply_id to DWH_Content

*** DONE An answer replies to a question
   
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  UPDATE DWH_Contributes
     SET dwh_reply_id=Inquiry_Content.dwh_content_id
         FROM
         DWH_Content AS Solution_Content
         INNER JOIN
         DWH_Content AS Inquiry_Content
         ON Solution_Content.id_sody_parent = Inquiry_Content.id_sody_inquiry
   WHERE DWH_Contributes.dwh_kind_of_id = 2
     AND DWH_Contributes.dwh_contains_id = Solution_Content.dwh_content_id ;
 #+END_SRC

#+RESULT
: 143 570 replies

Please note that some answers are accepted

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
    SELECT COUNT(*) FROM DWH_Contributes WHERE is_accepted= true ;
 #+END_SRC

#+RESULT: 
: 50 037 accepted answers 

*** DONE A question "replies" to an accepted answer

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
  UPDATE DWH_Contributes
     SET dwh_reply_id=Solution_Content.dwh_content_id
         FROM
         DWH_Content AS Inquiry_Content
         INNER JOIN
         DWH_Content AS Solution_Content
         ON Solution_Content.id_sody_parent = Inquiry_Content.id_sody_inquiry
         INNER JOIN
         (SELECT * FROM DWH_Contributes
           WHERE DWH_Contributes.dwh_kind_of_id = 2 AND DWH_Contributes.is_accepted=true
         ) AS Solution_Contributes
         ON Solution_Contributes.dwh_contains_id = Solution_Content.dwh_content_id
   WHERE DWH_Contributes.dwh_kind_of_id = 1
     AND DWH_Contributes.dwh_contains_id = Inquiry_Content.dwh_content_id ;
 #+END_SRC

#+RESULT: 
: 50 037 accepted answers
 
** DONE WRITE Add dwh_interlocutor_id
   
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   UPDATE DWH_Contributes
      SET dwh_interlocutor_id=Interlocutor.dwh_user_id
          FROM
          DWH_Content AS Reply
          INNER JOIN
          DWH_Contributes AS Mirror
          ON Reply.dwh_content_id = Mirror.dwh_contains_id
          INNER JOIN
          DWH_User AS Interlocutor
          ON Interlocutor.dwh_user_id = Mirror.dwh_author_id
   WHERE DWH_Contributes.dwh_reply_id = Reply.dwh_content_id;
#+END_SRC

#+RESULT
: INSERT  193 607 interlocutors

** DONE Add dwh_last_activity_id
   
#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   UPDATE DWH_Contributes
      SET dwh_last_activity_id=DWH_Date.dwh_date_id
          FROM
          DWH_Content
          INNER JOIN
          (SELECT id_sody_solution AS id, last_activity_date FROM SODY_Solution) AS Solution
          ON Solution.id = DWH_Content.id_sody_solution
          INNER JOIN
          DWH_Date
          ON DWH_Date.date = Solution.last_activity_date
    WHERE DWH_Contributes.dwh_kind_of_id = 2
      AND DWH_Content.dwh_content_id = DWH_Contributes.dwh_contains_id ;
#+END_SRC

 #+RESULT
: INSERT 143 570 answers 

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   UPDATE DWH_Contributes
      SET dwh_last_activity_id=DWH_Date.dwh_date_id
          FROM
          DWH_Content
          INNER JOIN
          (SELECT id_sody_inquiry AS id, last_activity_date FROM SODY_Inquiry) AS Inquiry
          ON Inquiry.id = DWH_Content.id_sody_inquiry
          INNER JOIN
          DWH_Date
          ON DWH_Date.date = Inquiry.last_activity_date
    WHERE DWH_Contributes.dwh_kind_of_id = 1
      AND DWH_Content.dwh_content_id = DWH_Contributes.dwh_contains_id ;
 #+END_SRC

 #+RESULT
: INSERT 121 777 questions
   
** DONE Add dwh_last_edit_date_id
   
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   UPDATE DWH_Contributes
      SET dwh_last_edit_date_id=DWH_Date.dwh_date_id
          FROM
          DWH_Content
          INNER JOIN
          (SELECT id_sody_solution AS id, last_edit_date FROM SODY_Solution) AS Solution
          ON Solution.id = DWH_Content.id_sody_solution
          INNER JOIN
          DWH_Date
          ON DWH_Date.date = Solution.last_edit_date
    WHERE DWH_Contributes.dwh_kind_of_id = 2
      AND DWH_Content.dwh_content_id = DWH_Contributes.dwh_contains_id ;
 #+END_SRC

 #+RESULT
: INSERT 42 075 last edit date on answers

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   UPDATE DWH_Contributes
      SET dwh_last_edit_date_id=DWH_Date.dwh_date_id
          FROM
          DWH_Content
          INNER JOIN
          (SELECT id_sody_inquiry AS id, last_edit_date FROM SODY_Inquiry) AS Inquiry
          ON Inquiry.id = DWH_Content.id_sody_inquiry
          INNER JOIN
          DWH_Date
          ON DWH_Date.date = Inquiry.last_edit_date
    WHERE DWH_Contributes.dwh_kind_of_id = 1
      AND DWH_Content.dwh_content_id = DWH_Contributes.dwh_contains_id ;
 #+END_SRC

 #+RESULT
: INSERT 69 439 last edit date on questions

* DONE Feed the table Comments

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
    DELETE FROM DWH_Comments;
    INSERT INTO DWH_Comments(
      dwh_author_id,
      dwh_reacts_id,
      dwh_contains_id,
      dwh_creation_date_id,
      dwh_interlocutor_id,
      score, delay, stamp
    )
    SELECT
      Author.dwh_user_id,
      DWH_Content.dwh_content_id,
      DWH_Body.dwh_body_id,
      DWH_Date.dwh_date_id,
      Interlocuteur.dwh_user_id,
      SODY_Comment_Inquiry.Score,
      SODY_Comment_Inquiry.response_time,
      SODY_Comment_Inquiry.creation_date
      FROM
          SODY_Comment_Inquiry
          INNER JOIN DWH_Body ON DWH_Body.id_sody_comment_inquiry = SODY_Comment_Inquiry.id_sody_comment_inquiry
          INNER JOIN DWH_Content ON DWH_Content.id_sody_inquiry = SODY_Comment_Inquiry.id_sody_inquiry
          INNER JOIN DWH_User AS Author ON Author.id_sody = SODY_Comment_Inquiry.id_sody_user
          INNER JOIN DWH_Date ON SODY_Comment_Inquiry.creation_date = DWH_Date.date
          INNER JOIN DWH_Contributes ON DWH_Content.dwh_content_id = DWH_Contributes.dwh_contains_id
          INNER JOIN DWH_User AS Interlocuteur ON Interlocuteur.dwh_user_id = DWH_Contributes.dwh_author_id
          ;
  -- 161 098 comments on questions
    INSERT INTO DWH_Comments(
      dwh_author_id,
      dwh_reacts_id,
      dwh_contains_id,
      dwh_creation_date_id,
      dwh_interlocutor_id,
      score, delay, stamp
    )
    SELECT
      Author.dwh_user_id,
      DWH_Content.dwh_content_id,
      DWH_Body.dwh_body_id,
      DWH_Date.dwh_date_id,
      Interlocuteur.dwh_user_id,
      SODY_Comment_Solution.Score,
      SODY_Comment_Solution.response_time,
      SODY_Comment_Solution.creation_date
      FROM
          SODY_Comment_Solution
          INNER JOIN DWH_Body ON DWH_Body.id_sody_comment_solution = SODY_Comment_Solution.id_sody_comment_solution
          INNER JOIN DWH_Content ON DWH_Content.id_sody_solution = SODY_Comment_Solution.id_sody_solution
          INNER JOIN DWH_User AS Author ON Author.id_sody = SODY_Comment_Solution.id_sody_user
          INNER JOIN DWH_Date ON SODY_Comment_Solution.creation_date = DWH_Date.date
          INNER JOIN DWH_Contributes ON DWH_Content.dwh_content_id = DWH_Contributes.dwh_contains_id
          INNER JOIN DWH_User AS Interlocuteur ON Interlocuteur.dwh_user_id = DWH_Contributes.dwh_author_id
          ;
  -- 147 028 comments on solutions
#+END_SRC
  
