-- Copyright (C) Koboyoda Arthur ASSIMA, Maxime MORGE  2022
-- Add key indicators
\timing 
-- Add the seniority indicator which is the time interval between the account
-- creation date and the account last access date of users
ALTER TABLE dwh_user ADD seniority interval; 
UPDATE dwh_user
   SET seniority = AGE(last_access_date, creation_date);
-- UPDATE 242148
-- Time: 2113.891 ms (00:02.114)

-- Add the communities
DROP TABLE IF EXISTS DWH_COP;
CREATE TABLE DWH_COP(
  dwh_topic_id SERIAL,
  topic VARCHAR(64),
  mean_debatableness FLOAT,
  PRIMARY KEY (dwh_topic_id)
);
DELETE FROM DWH_COP;
INSERT INTO dwh_cop(dwh_topic_id, topic) VALUES (1, 'r');
INSERT INTO dwh_cop(dwh_topic_id, topic) VALUES (2, 'pandas');
-- DELETE 0
-- INSERT 0 1
-- Time: 15.653 ms
-- INSERT 0 1
-- Time: 8.145 ms

-- Add the mean debatableness indicator for the community R
 UPDATE dwh_cop
    SET mean_debatableness = (SELECT
                                (
                                  (SELECT SUM(answer_count)
                                    FROM dwh_contributes
                                   WHERE dwh_kind_of_id=1
                                     AND dwh_contains_id IN
					                               (SELECT dwh_content_id
                                            FROM dwh_content
                                           WHERE tags LIKE '%<r>%')
                                  ) * 1.0 /
					                        (SELECT COUNT(*)
                                     FROM dwh_contributes
                                    WHERE dwh_kind_of_id=1
                                      AND dwh_contains_id IN
					                                (SELECT dwh_content_id
                                             FROM dwh_content
                                            WHERE tags LIKE '%<r>%')
                                  )
                                )
    )
	WHERE dwh_topic_id=1;
-- UPDATE 1
-- Time: 1919.366 ms (00:01.919)

-- Add the mean debatableness indicator for the community pandas
UPDATE dwh_cop
   SET mean_debatableness = (SELECT
                               (
                                 (SELECT SUM(answer_count)
                                    FROM dwh_contributes
                                   WHERE dwh_kind_of_id=1
                                     AND dwh_contains_id IN
					                               (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<pandas>%')
                                 ) * 1.0 /
					                       (SELECT COUNT(*)
                                    FROM dwh_contributes
                                   WHERE dwh_kind_of_id=1
                                     AND dwh_contains_id IN
					                               (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<pandas>%')
                                 )
                               )
   )
 WHERE dwh_topic_id=2;
-- UPDATE 1
-- Time: 1845.499 ms (00:01.845)

-- Add membership between communities and users
DROP TABLE IF EXISTS DWH_MEMBERSHIP;
CREATE TABLE DWH_MEMBERSHIP(
  dwh_topic_id SERIAL,
  dwh_member_id SERIAL,
  nb_questions INTEGER,
  nb_answers INTEGER,
  mean_score_questions FLOAT,
  mean_score_answers FLOAT,
  probability_post FLOAT,
  probability_query FLOAT,
  probability_reply FLOAT,
  z_score FLOAT,
  mec FLOAT,
  degree INTEGER,
  in_degree INTEGER,
  out_degree INTEGER,
  in_degree_accepted INTEGER,
  out_degree_accepted INTEGER,
  in_degree_best INTEGER,
  out_degree_best INTEGER,
  PRIMARY KEY (dwh_topic_id, dwh_member_id),
  CONSTRAINT dwh_fk_topic
  FOREIGN KEY (dwh_topic_id)
  REFERENCES DWH_COP(dwh_topic_id),
  CONSTRAINT dwh_fk_member
  FOREIGN KEY (dwh_member_id)
  REFERENCES DWH_User(dwh_user_id)
);
DELETE FROM DWH_MEMBERSHIP;

-- Add membership instances for users who asked a question about <r>
INSERT INTO dwh_membership(dwh_member_id, dwh_topic_id) 
SELECT DISTINCT(dwh_author_id), 1
  FROM dwh_contributes 
 WHERE dwh_kind_of_id=1
   AND dwh_contains_id IN
       (SELECT dwh_content_id
          FROM dwh_content
         WHERE tags LIKE '%<r>%'
       );
-- INSERT 0 115056
-- Time: 1444,095 ms (00:01,444)

-- Add memberships for users who only replies to a question with the <r> tag 
INSERT INTO dwh_membership(dwh_member_id, dwh_topic_id) 
SELECT DISTINCT(dwh_author_id),1
  FROM dwh_contributes 
 WHERE NOT EXISTS
       (SELECT *
          FROM dwh_membership
         WHERE dwh_topic_id=1
           AND dwh_member_id=dwh_author_id)
   AND dwh_kind_of_id=2
   AND dwh_reply_id IN
       (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<r>%' );
-- INSERT 0 25169
-- Time: 2281,445 ms (00:02,281)

-- Add memberships for users who asked a question about <pandas>
INSERT INTO dwh_membership(dwh_member_id,dwh_topic_id) 
SELECT DISTINCT(dwh_author_id), 2
  FROM dwh_contributes 
 WHERE dwh_kind_of_id=1
   AND dwh_contains_id IN
       (SELECT dwh_content_id
          FROM dwh_content
         WHERE tags LIKE '%<pandas>%' );
-- INSERT 0 71802
-- Time: 1113,496 ms (00:01,113)

-- Add memberships for users who only replies to a question with the <pandas> tag 
INSERT INTO dwh_membership(dwh_member_id,dwh_topic_id) 
SELECT DISTINCT(dwh_author_id), 2
  FROM dwh_contributes 
 WHERE NOT EXISTS
       (SELECT *
          FROM dwh_membership
         WHERE dwh_topic_id=2
           AND dwh_member_id=dwh_author_id
       )
   AND dwh_kind_of_id=2
   AND dwh_reply_id IN
       (SELECT dwh_content_id
          FROM dwh_content
         WHERE tags LIKE '%<pandas>%' );
-- INSERT 0 28027
-- Time: 696,697 ms

-- Add the number of questions about r of an user
UPDATE dwh_membership
   SET nb_questions = (SELECT COUNT(*)
                         FROM dwh_contributes
					              WHERE dwh_author_id = dwh_membership.dwh_member_id
                          AND dwh_kind_of_id=1
					                AND dwh_contains_id IN
                              (SELECT dwh_content_id FROM dwh_content WHERE tags like '%<r>%'))
	WHERE dwh_topic_id=1;
-- UPDATE 140225
-- Time: 2855,727 ms (00:02,856)

-- Add the number of questions about pandas of an user
UPDATE dwh_membership
   SET nb_questions = (SELECT COUNT(*)
                         FROM dwh_contributes
					              WHERE dwh_author_id = dwh_membership.dwh_member_id
                          AND dwh_kind_of_id=1
					                AND dwh_contains_id IN
                              (SELECT dwh_content_id FROM dwh_content WHERE tags like '%<pandas>%'))
 WHERE dwh_topic_id=2;
-- UPDATE 99829
-- Time: 1310,613 ms (00:01,311)

-- Add the number of answers about r of an user
UPDATE dwh_membership
SET nb_answers = (SELECT COUNT(*)
                    FROM dwh_contributes
					         WHERE dwh_author_id = dwh_membership.dwh_member_id
                     AND dwh_kind_of_id=2
					           AND dwh_reply_id IN
                         (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<r>%'))
	WHERE dwh_topic_id=1;
-- UPDATE 140225
-- Time: 3125,753 ms (00:03,126)

-- Number of answers to questions related to the "Pandas" topic of an user
UPDATE dwh_membership
   SET nb_answers = (SELECT COUNT(*)
                       FROM dwh_contributes
					            WHERE dwh_author_id = dwh_membership.dwh_member_id
                        AND dwh_kind_of_id=2
					              AND dwh_reply_id IN
                            (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<pandas>%'))
 WHERE dwh_topic_id=2;
-- UPDATE 99829
-- Time: 2288,473 ms (00:02,288)

-- Add mean score of the questions for R
UPDATE dwh_membership
   SET mean_score_questions =
       CASE
       WHEN nb_questions=0 THEN 0
			 ELSE (SELECT AVG(score)
               FROM dwh_contributes
					    WHERE dwh_author_id = dwh_membership.dwh_member_id AND
                    dwh_kind_of_id=1
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<r>%'))
       END
 WHERE dwh_topic_id=1;
-- UPDATE 140225
-- Time: 2313,933 ms (00:02,314)

-- Add mean score the questions for Pandas
UPDATE dwh_membership
   SET mean_score_questions =
       CASE
       WHEN nb_questions=0 THEN 0
			 ELSE (SELECT AVG(score)
               FROM dwh_contributes
					   WHERE dwh_author_id = dwh_membership.dwh_member_id
               AND dwh_kind_of_id=1
					     AND dwh_contains_id IN
                   (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<pandas>%'))
       END
 WHERE dwh_topic_id=2;
-- UPDATE 99829
-- Time: 1542,903 ms (00:01,543)

-- Add mean score of the answers for R
UPDATE dwh_membership
   SET mean_score_answers =
       CASE
       WHEN nb_answers=0 THEN 0
			 ELSE (SELECT AVG(score)
               FROM dwh_contributes
					    WHERE dwh_author_id = dwh_membership.dwh_member_id
                AND dwh_kind_of_id=2
					      AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags like '%<r>%'))
       END
 WHERE dwh_topic_id=1;
-- UPDATE 140225
-- Time: 2904,779 ms (00:02,905)

-- Add mean score of the answers for Pandas
UPDATE dwh_membership
SET mean_score_answers =
    CASE
    WHEN nb_answers=0 THEN 0
		ELSE (SELECT AVG(score)
            FROM dwh_contributes
					 WHERE dwh_author_id = dwh_membership.dwh_member_id
             AND dwh_kind_of_id=2
					   AND dwh_reply_id IN
                 (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<pandas>%'))
    END
 WHERE dwh_topic_id=2;
-- UPDATE 99829
-- Time: 2091,222 ms (00:02,091)

-- Add the zscore indicator for each user
UPDATE dwh_membership
   SET z_score = (nb_answers-nb_questions)/sqrt(nb_answers+nb_questions);
-- UPDATE 240054
-- Time: 1442,048 ms (00:01,442)

-- Add rank of an answer by using the RANK() function 
ALTER TABLE dwh_contributes ADD rank INTEGER; 
UPDATE dwh_contributes ctr
SET rank = (SELECT rank from
                          (SELECT dwh_contains_id,RANK()
                                    OVER(PARTITION BY dwh_reply_id ORDER BY score DESC)
  				                   FROM dwh_contributes 
			                      WHERE dwh_reply_id=ctr.dwh_reply_id) as r
     	       WHERE r.dwh_contains_id=ctr.dwh_contains_id) 
WHERE dwh_kind_of_id=2 ;
--UPDATE 718316
-- Time: 1 min 52 secs.

-- Add utility of the answers based on their rank
ALTER TABLE dwh_contributes ADD utility FLOAT;
UPDATE dwh_contributes
   SET utility = (SELECT 1.0/rank)
 WHERE dwh_kind_of_id=2;
--UPDATE 718316
--Time: 1 min 35 secs


-- Add MEC (Mean Expert Contribution) of members for the R community
UPDATE dwh_membership m
   SET mec =
       (
         (
           SELECT(
		         (SELECT SUM(d1.utility*d2.answer_count)
                FROM dwh_contributes d1
                     JOIN dwh_contributes d2
                         ON d1.dwh_reply_id=d2.dwh_contains_id
		           WHERE d1.dwh_author_id=m.dwh_member_id
                 AND d1.dwh_reply_id IN
				             (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<r>%')) --sum of (utility*debatableness)
		                 / -- divided by
		                 (SELECT mean_debatableness
                        FROM dwh_cop
                       WHERE dwh_topic_id=1
                     )
           )
         ) -- mean debatableness of R community
	       /	 -- divided by the number of answers from the member
		     m.nb_answers
       )
 WHERE m.dwh_topic_id=1;
--UPDATE 140225
--Time: 11 min 21 secs.

-- Add MEC (Mean Expert Contribution) of members for the Pandas community
UPDATE dwh_membership m
   SET mec =
       (
         (
           SELECT(
		         (SELECT SUM(d1.utility*d2.answer_count)
                FROM dwh_contributes d1
                     JOIN dwh_contributes d2
                         ON d1.dwh_reply_id=d2.dwh_contains_id
		           WHERE d1.dwh_author_id=m.dwh_member_id
                 AND d1.dwh_reply_id IN
			               (SELECT dwh_content_id
                        FROM dwh_content
                       WHERE tags LIKE '%<pandas>%'
                     )
             ) --sum of (utility*debatableness)
		         / -- divided by
		         (SELECT mean_debatableness
                FROM dwh_cop
               WHERE dwh_topic_id=2
             )
           )
         ) -- mean debatableness of Pandas community
	       /	 -- divided by the number of answers from the member
		     m.nb_answers
       )
 WHERE m.dwh_topic_id=2;
--UPDATE 99829
--Time: 8 min 35 secs.

-- Add out-degree of members for the R community
UPDATE dwh_membership
   SET out_degree =  (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
      WHERE dwh_contains_id in
				    (SELECT (dwh_reply_id)
				       FROM dwh_contributes
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2 AND
				 	          dwh_reply_id
                    IN (SELECT dwh_content_id
                          FROM dwh_content
                         WHERE tags LIKE '%<r>%'
                    )
            )
   )
WHERE dwh_topic_id=1;
--UPDATE 140 225
--Time: 6 min 20 secs.


-- Add out-degree of members for the Pandas community
UPDATE dwh_membership
   SET out_degree =  (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
      WHERE dwh_contains_id in
				    (SELECT(dwh_reply_id)
				       FROM dwh_contributes
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2
                AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<pandas>%')
            )
   )
 WHERE dwh_topic_id=2;
--UPDATE 99 829
--Time: 4 min 19 secs.

-- Add in-degree of members from the R community
UPDATE dwh_membership
   SET in_degree = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
		  WHERE dwh_kind_of_id=2 AND dwh_reply_id IN 
				    (SELECT dwh_contains_id 
				       FROM dwh_contributes 
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<r>%')
            )
   )
 WHERE dwh_topic_id=1;
--UPDATE 140 225
--Time: 13 min 50 secs.

-- Add in-degree of members for the Pandas community
UPDATE dwh_membership
   SET in_degree = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
	    WHERE dwh_kind_of_id=2
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<pandas>%')
            )
   )
 WHERE dwh_topic_id=2;
--UPDATE 99829
--Time: 8 min 15 secs.

-- Add degree for members membership (in-degree + out-degree)
UPDATE dwh_membership
   SET degree=in_degree + out_degree;
--UPDATE 240054
--Time 13 secs 841 msec.


-- Add out-degree of accepted answers for members of the R community
UPDATE dwh_membership
   SET out_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
      WHERE dwh_contains_id in
				    (SELECT(dwh_reply_id)
               FROM dwh_contributes
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2
                AND is_accepted=true
                AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<r>%')
            )
   )
 WHERE dwh_topic_id=1;
--UPDATE 140225
--Time: in 3 min 35 secs.

-- Add out-degree of accepted answers for members of the Pandas community
UPDATE dwh_membership
   SET out_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
      WHERE dwh_contains_id in
				    (SELECT(dwh_reply_id) 
				       FROM dwh_contributes 
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2
                AND is_accepted=true
                AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<pandas>%')
            )
   )
 WHERE dwh_topic_id=2;
--UPDATE 99829
--Time: 1 min 40 secs.


-- Add in-degree of accepted answers for members of the R community
UPDATE dwh_membership
   SET in_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
		  WHERE dwh_kind_of_id=2
        AND is_accepted=true
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id 
				       FROM dwh_contributes 
				      WHERE dwh_author_id=dwh_member_id 
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<r>%')
            )
   )
 WHERE dwh_topic_id=1;
--UPDATE 140225
--Time: 12 min 3 secs.

-- Add in-degree of accepted answers for members of the Pandas community
UPDATE dwh_membership
   SET in_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
		  WHERE dwh_kind_of_id=2
        AND is_accepted=true
        AND dwh_reply_id IN
            (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<pandas>%')
            )
   )
 WHERE dwh_topic_id=2;
--UPDATE 99829
--Time: 7 min 16 secs.

-- Add out-degree of top-voted answers for members of the R community
UPDATE dwh_membership
   SET out_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
      WHERE dwh_contains_id in
				(SELECT(dwh_reply_id) 
				   FROM dwh_contributes 
          WHERE dwh_author_id=dwh_member_id
            AND dwh_kind_of_id=2
            AND rank=1
            AND dwh_reply_id IN
                (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<r>%')
        )
   )
 WHERE dwh_topic_id=1;
--UPDATE 140225
--Time: 4 min 49 secs.

-- Add out-degree of top-voted answers for members of the Pandas community
UPDATE dwh_membership
   SET out_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
      WHERE dwh_contains_id IN
				(SELECT(dwh_reply_id)
				   FROM dwh_contributes
          WHERE dwh_author_id=dwh_member_id
            AND dwh_kind_of_id=2
            AND rank=1
            AND	dwh_reply_id IN
                (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<pandas>%')
        )
   )
 WHERE dwh_topic_id=2;
--UPDATE 99829
--Time: 3 min 42 secs.

-- Add in-degree of top-voted answers for members of the R community
UPDATE dwh_membership
   SET in_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
		  WHERE dwh_kind_of_id=2
        AND rank=1
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<r>%')
            )
   )
 WHERE dwh_topic_id=1;
--UPDATE 140225
--Time: 11 min 33 secs.

-- Add in-degree of top-voted answers for members of the Pandas community
UPDATE dwh_membership
   SET in_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
		  WHERE dwh_kind_of_id=2
        AND rank=1
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<pandas>%')))
 WHERE dwh_topic_id=2;
--UPDATE 99829
--Time: 9 min 26 secs.

ALTER TABLE dwh_membership ALTER COLUMN seniority TYPE integer USING EXTRACT(EPOCH FROM seniority) / 86400::integer;
-- Add the seniority indicator for the memberships (time spent by an user in the community)
ALTER TABLE dwh_membership ADD seniority interval;
-- Start of the R community
SELECT MIN(stamp)
  FROM dwh_contributes
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content
				 WHERE tags LIKE '%<r>%');
-- 2008-09-16 21:40:29.927

-- End of the r community
SELECT MAX(stamp) FROM dwh_contributes
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content
				 WHERE tags LIKE '%<r>%');
-- 2020-12-06 05:21:40.917

-- Start of the Pandas community
SELECT MIN(stamp) FROM dwh_contributes
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content
				 WHERE tags LIKE '%<pandas>%');
-- 2011-03-30 12:26:50.063

-- End of the Pandas community
SELECT MAX(stamp) FROM dwh_contributes 
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content 
				 WHERE tags LIKE '%<pandas>%');
-- 2020-12-06 05:21:15.56

-- Add the seniority of the R community members
UPDATE dwh_membership
   SET seniority = (
     SELECT DATE_PART('DAY',
                      (SELECT
                         CASE
                         WHEN '2020-12-06 05:21:40.917' < last_access_date THEN '2020-12-06 05:21:40.917'
				                 ELSE last_access_date
				                 END AS MostEarlyDate FROM dwh_user
                        WHERE dwh_user_id=dwh_member_id)
                         -
			                   (SELECT
                            CASE WHEN '2008-09-16 21:40:29.927' > creation_date THEN '2008-09-16 21:40:29.927'
				                    ELSE creation_date
				                    END AS MostRecentDate FROM dwh_user
                           WHERE dwh_user_id=dwh_member_id))
              +1)
 WHERE dwh_topic_id=1;
--UPDATE 140225
--Time: 2 secs 584 msec.

-- Add the seniority of the Pandas community members
UPDATE dwh_membership
   SET seniority = (
     SELECT DATE_PART('DAY',
                      (SELECT
                         CASE WHEN '2020-12-06 05:21:15.56' < last_access_date THEN '2020-12-06 05:21:15.56'
				                 ELSE last_access_date
				                 END AS MostEarlyDate FROM dwh_user
                        WHERE dwh_user_id=dwh_member_id)
                        -
			   		            (SELECT
                           CASE WHEN '2011-03-30 12:26:50.063' > creation_date THEN '2011-03-30 12:26:50.063'
				   		             ELSE creation_date
				   		             END AS MostRecentDate
                           FROM dwh_user WHERE dwh_user_id=dwh_member_id))
              +1)
 WHERE dwh_topic_id=2;
--UPDATE 99829
--Time: 3 secs 76 msec.

-- Add probability to post for members of the R community, i.e
-- number of answers+questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
SET probability_post = (SELECT ((nb_answers+nb_questions)/(seniority/15.0)) 
		          FROM dwh_membership 
			 WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=1)
WHERE dwh_topic_id=1;
--UPDATE 140225
--Query returned successfully in 12 secs 283 msec.


-- WRONG Add probability to post for members of the Pandas community, i.e.
-- number of anwsers+questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
SET probability_post = (SELECT ((nb_answers+nb_questions)/(seniority/15.0)) 
		          FROM dwh_membership 
			 WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=2)
WHERE dwh_topic_id=2;
--UPDATE 99829
--Query returned successfully in 9 secs 999 msec.


-- Add probability to reply for members of the R community, i.e.
-- number of answers divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_reply =
       (SELECT (nb_answers/(seniority/15.0))
		      FROM dwh_membership
			   WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=1)
 WHERE dwh_topic_id=1;
--UPDATE 140225
--Time 4 secs 74 msec.

-- Add probability to reply for members of the Pandas community, i.e.
-- number of answers divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_reply =
       (SELECT (nb_answers/(seniority/15.0))
		      FROM dwh_membership
			   WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=2)
 WHERE dwh_topic_id=2;
--UPDATE 99829
--Time: 2 secs 330 msec.

-- Add probability of query for member of the R community, i.e.
-- numbers of questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
SET probability_query = (SELECT (nb_questions/(seniority/15.0)) 
		          FROM dwh_membership 
			 WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=1)
WHERE dwh_topic_id=1;
--UPDATE 140225
--Query returned successfully in 15 secs 755 msec.


-- Add probability of query for members of the Pandas community, i.e.
-- number of questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
SET probability_query = (SELECT (nb_questions/(seniority/15.0)) 
		          FROM dwh_membership 
			 WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=2)
WHERE dwh_topic_id=2;
--UPDATE 99829
--Query returned successfully in 4 secs 766 msec.


-- Add the first_post_date (resp. last_post_date) indicator which are the date
-- id of the first contribution (resp. last contribution) of the member
ALTER TABLE dwh_membership DROP COLUMN first_post_date;
ALTER TABLE dwh_membership DROP COLUMN last_post_date; 
ALTER TABLE dwh_membership ADD COLUMN first_post_date DATE;
ALTER TABLE dwh_membership ADD COLUMN last_post_date  DATE;

-- DONE bug correction
UPDATE dwh_membership
   SET first_post_date = (
     SELECT MIN(Contribution.stamp)
       FROM dwh_membership AS Member
            JOIN dwh_contributes AS Contribution
                ON Member.dwh_member_id = Contribution.dwh_author_id
            JOIN dwh_content AS Content
                ON Content.dwh_content_id = Contribution.dwh_contains_id 
            LEFT JOIN dwh_content AS Reply
                ON Reply.dwh_content_id = Contribution.dwh_reply_id   
      WHERE dwh_membership.dwh_member_id = Member.dwh_member_id
        AND (
          Content.tags LIKE '%<r>%'
          OR
          Reply.tags LIKE '%<r>%'
        )
   )
 WHERE dwh_topic_id = 1;
-- UPDATE 140225
-- Time: 599536,373 ms (09:59,536)

-- DONE bug correction
UPDATE dwh_membership
   SET first_post_date = (
     SELECT MIN(Contribution.stamp)
       FROM dwh_membership AS Member
            JOIN dwh_contributes AS Contribution
                ON Member.dwh_member_id = Contribution.dwh_author_id
            JOIN dwh_content AS Content
                ON Content.dwh_content_id = Contribution.dwh_contains_id 
            LEFT JOIN dwh_content AS Reply
                ON Reply.dwh_content_id = Contribution.dwh_reply_id   
      WHERE dwh_membership.dwh_member_id = Member.dwh_member_id
        AND (
          Content.tags LIKE '%<pandas>%'
          OR
          Reply.tags LIKE '%<pandas>%'
        )
   )
 WHERE dwh_topic_id = 2;
UPDATE 99829
       Time: 388624,286 ms (06:28,624)

-- DONE bug correction
UPDATE dwh_membership
   SET last_post_date = (
     SELECT MAX(Contribution.stamp)
       FROM dwh_membership AS Member
            JOIN dwh_contributes AS Contribution
                ON Member.dwh_member_id = Contribution.dwh_author_id
            JOIN dwh_content AS Content
                ON Content.dwh_content_id = Contribution.dwh_contains_id 
            LEFT JOIN dwh_content AS Reply
                ON Reply.dwh_content_id = Contribution.dwh_reply_id   
      WHERE dwh_membership.dwh_member_id = Member.dwh_member_id
        AND (
          Content.tags LIKE '%<r>%'
          OR
          Reply.tags LIKE '%<r>%'
        )
   )
 WHERE dwh_topic_id = 1;
-- UPDATE 140225
-- Time: 582297,703 ms (09:42,298)

-- DONE bug correction
UPDATE dwh_membership
   SET last_post_date = (
     SELECT MAX(Contribution.stamp)
       FROM dwh_membership AS Member
            JOIN dwh_contributes AS Contribution
                ON Member.dwh_member_id = Contribution.dwh_author_id
            JOIN dwh_content AS Content
                ON Content.dwh_content_id = Contribution.dwh_contains_id 
            LEFT JOIN dwh_content AS Reply
                ON Reply.dwh_content_id = Contribution.dwh_reply_id   
      WHERE dwh_membership.dwh_member_id = Member.dwh_member_id
        AND (
          Content.tags LIKE '%<pandas>%'
          OR
          Reply.tags LIKE '%<pandas>%'
        )
   )
 WHERE dwh_topic_id = 2;
-- UPDATE 99829
-- Time: 365218,744 ms (06:05,219)


UPDATE DWH_Membership
   SET nb_contributions = COALESCE(nb_questions, 0) + COALESCE(nb_answers, 0);

--UPDATE 240054;
