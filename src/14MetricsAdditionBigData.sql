-- Copyright (C) Koboyoda Arthur ASSIMA, Maxime MORGE  2022, Audrey DEWAELE
-- Add key indicators
\timing 
-- Add the seniority indicator which is the time interval between the account
-- creation date and the account last access date of users
ALTER TABLE dwh_user ADD seniority interval; 
UPDATE dwh_user
   SET seniority = AGE(last_access_date, creation_date);
-- UPDATE 75 887
-- Time: 511.434 ms

-- Add the communities
DROP TABLE IF EXISTS DWH_COP;
CREATE TABLE DWH_COP(
  dwh_topic_id SERIAL,
  topic VARCHAR(64),
  mean_debatableness FLOAT,
  PRIMARY KEY (dwh_topic_id)
);
DELETE FROM DWH_COP;
INSERT INTO dwh_cop(dwh_topic_id, topic) VALUES (1, 'hadoop');
INSERT INTO dwh_cop(dwh_topic_id, topic) VALUES (2, 'hive');
INSERT INTO dwh_cop(dwh_topic_id, topic) VALUES (3, 'apache-pig');
INSERT INTO dwh_cop(dwh_topic_id, topic) VALUES (4, 'apache-spark');

-- DELETE 0
-- INSERT 0 1
-- Time: 8.863 ms
-- INSERT 0 1
-- Time: 7.913 ms
-- INSERT 0 1
-- Time: 8.146 ms
-- INSERT 0 1
-- Time: 14.481 ms

-- Add the mean debatableness indicator for the community hadoop
 UPDATE dwh_cop
    SET mean_debatableness = (SELECT
                                (
                                  (SELECT SUM(answer_count)
                                    FROM dwh_contributes
                                   WHERE dwh_kind_of_id=1
                                     AND dwh_contains_id IN
					                               (SELECT dwh_content_id
                                            FROM dwh_content
                                           WHERE tags LIKE '%<hadoop>%')
                                  ) * 1.0 /
					                        (SELECT COUNT(*)
                                     FROM dwh_contributes
                                    WHERE dwh_kind_of_id=1
                                      AND dwh_contains_id IN
					                                (SELECT dwh_content_id
                                             FROM dwh_content
                                            WHERE tags LIKE '%<hadoop>%')
                                  )
                                )
    )
	WHERE dwh_topic_id=1;
-- UPDATE 1
-- Time: 398.331 ms 

-- Add the mean debatableness indicator for the community hive
UPDATE dwh_cop
   SET mean_debatableness = (SELECT
                               (
                                 (SELECT SUM(answer_count)
                                    FROM dwh_contributes
                                   WHERE dwh_kind_of_id=1
                                     AND dwh_contains_id IN
					                               (SELECT dwh_content_id 
                                          FROM dwh_content
                                          WHERE tags LIKE '%<hive>%')
                                 ) * 1.0 /
					                       (SELECT COUNT(*)
                                    FROM dwh_contributes
                                   WHERE dwh_kind_of_id=1
                                     AND dwh_contains_id IN
					                               (SELECT dwh_content_id 
                                    FROM dwh_content 
                                   WHERE tags LIKE '%<hive>%')
                                 )
                               )
   )
 WHERE dwh_topic_id=2;
-- UPDATE 1
-- Time: 340.242 ms 

-- Add the mean debatableness indicator for the community apache-pig
 UPDATE dwh_cop
    SET mean_debatableness = (SELECT
                                (
                                  (SELECT SUM(answer_count)
                                    FROM dwh_contributes
                                   WHERE dwh_kind_of_id=1
                                     AND dwh_contains_id IN
					                               (SELECT dwh_content_id
                                            FROM dwh_content
                                           WHERE tags LIKE '%<apache-pig>%')
                                  ) * 1.0 /
					                        (SELECT COUNT(*)
                                     FROM dwh_contributes
                                    WHERE dwh_kind_of_id=1
                                      AND dwh_contains_id IN
					                                (SELECT dwh_content_id
                                             FROM dwh_content
                                            WHERE tags LIKE '%<apache-pig>%')
                                  )
                                )
    )
	WHERE dwh_topic_id=3;
-- UPDATE 1
-- Time: 338.319 ms 

-- Add the mean debatableness indicator for the community  apache-spark
UPDATE dwh_cop
   SET mean_debatableness = (SELECT
                               (
                                 (SELECT SUM(answer_count)
                                    FROM dwh_contributes
                                   WHERE dwh_kind_of_id=1
                                     AND dwh_contains_id IN
					                               (SELECT dwh_content_id
                                            FROM dwh_content
                                           WHERE tags LIKE '%<apache-spark>%')
                                 ) * 1.0 /
					                       (SELECT COUNT(*)
                                    FROM dwh_contributes
                                   WHERE dwh_kind_of_id=1
                                     AND dwh_contains_id IN
					                               (SELECT dwh_content_id 
                                           FROM dwh_content
                                          WHERE tags LIKE '%<apache-spark>%')
                                 )
                               )
   )
 WHERE dwh_topic_id=4;
-- UPDATE 1
-- Time: 380.999 ms 

SELECT * FROM DWH_COP;
-- dwh_topic_id |    topic     | mean_debatableness
--------------+--------------+--------------------
--            1 | hadoop       | 1.2526205936920223
--            2 | hive         | 1.1936162921623341
--            3 | apache-pig   | 1.2341377968080964
--            4 | apache-spark | 1.1468361882867475
-- (4 rows)
-- Time: 1.683 ms

-- Add membership between communities and users
DROP TABLE IF EXISTS DWH_MEMBERSHIP;
CREATE TABLE DWH_MEMBERSHIP(
  dwh_topic_id SERIAL,
  dwh_member_id SERIAL,
  nb_questions INTEGER,
  nb_answers INTEGER,
  mean_score_questions FLOAT,
  mean_score_answers FLOAT,
  probability_post FLOAT,
  probability_query FLOAT,
  probability_reply FLOAT,
  z_score FLOAT,
  mec FLOAT,
  degree INTEGER,
  in_degree INTEGER,
  out_degree INTEGER,
  in_degree_accepted INTEGER,
  out_degree_accepted INTEGER,
  in_degree_best INTEGER,
  out_degree_best INTEGER,
  PRIMARY KEY (dwh_topic_id, dwh_member_id),
  CONSTRAINT dwh_fk_topic
  FOREIGN KEY (dwh_topic_id)
  REFERENCES DWH_COP(dwh_topic_id),
  CONSTRAINT dwh_fk_member
  FOREIGN KEY (dwh_member_id)
  REFERENCES DWH_User(dwh_user_id)
);
DELETE FROM DWH_MEMBERSHIP;

-- Add membership instances for users who asked a question about <hadoop>
INSERT INTO dwh_membership(dwh_member_id, dwh_topic_id) 
SELECT DISTINCT(dwh_author_id), 1
  FROM dwh_contributes 
 WHERE dwh_kind_of_id=1
   AND dwh_contains_id IN
       (SELECT dwh_content_id
          FROM dwh_content
         WHERE tags LIKE '%<hadoop>%'
       );
-- INSERT 0 23242
-- Time: 693,159 ms 

-- Add memberships for users who only replies to a question with the <hadoop> tag 
INSERT INTO dwh_membership(dwh_member_id, dwh_topic_id) 
SELECT DISTINCT(dwh_author_id),1
  FROM dwh_contributes 
 WHERE NOT EXISTS
       (SELECT *
          FROM dwh_membership
         WHERE dwh_topic_id=1
           AND dwh_member_id=dwh_author_id)
   AND dwh_kind_of_id=2
   AND dwh_reply_id IN
       (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hadoop>%' );
-- INSERT 0 13046
-- Time: 443,464 ms 

-- Add memberships for users who asked a question about <hive>
INSERT INTO dwh_membership(dwh_member_id,dwh_topic_id) 
SELECT DISTINCT(dwh_author_id), 2
  FROM dwh_contributes 
 WHERE dwh_kind_of_id=1
   AND dwh_contains_id IN
       (SELECT dwh_content_id
          FROM dwh_content
         WHERE tags LIKE '%<hive>%' );
-- INSERT 0 11775
-- Time: 426,024 ms 

-- Add memberships for users who only replies to a question with the <hive> tag 
INSERT INTO dwh_membership(dwh_member_id,dwh_topic_id) 
SELECT DISTINCT(dwh_author_id), 2
  FROM dwh_contributes 
 WHERE NOT EXISTS
       (SELECT *
          FROM dwh_membership
         WHERE dwh_topic_id=2
           AND dwh_member_id=dwh_author_id
       )
   AND dwh_kind_of_id=2
   AND dwh_reply_id IN
       (SELECT dwh_content_id
          FROM dwh_content
         WHERE tags LIKE '%<hive>%' );
-- INSERT 0 6339
-- Time: 423,805 ms

-- Add membership instances for users who asked a question about <apache-pig>
INSERT INTO dwh_membership(dwh_member_id, dwh_topic_id) 
SELECT DISTINCT(dwh_author_id), 3
  FROM dwh_contributes 
 WHERE dwh_kind_of_id=1
   AND dwh_contains_id IN
       (SELECT dwh_content_id
          FROM dwh_content
         WHERE tags LIKE '%<apache-pig>%'
       );
-- INSERT 0 3119
-- Time: 229,501 ms 

-- Add memberships for users who only replies to a question with the <apache-pig> tag 
INSERT INTO dwh_membership(dwh_member_id, dwh_topic_id) 
SELECT DISTINCT(dwh_author_id),3
  FROM dwh_contributes 
 WHERE NOT EXISTS
       (SELECT *
          FROM dwh_membership
         WHERE dwh_topic_id=3
           AND dwh_member_id=dwh_author_id)
   AND dwh_kind_of_id=2
   AND dwh_reply_id IN
       (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-pig>%' );
-- INSERT 0 1557
-- Time: 255,688 ms 

-- Add memberships for users who asked a question about <apache-spark>
INSERT INTO dwh_membership(dwh_member_id,dwh_topic_id) 
SELECT DISTINCT(dwh_author_id), 4
  FROM dwh_contributes 
 WHERE dwh_kind_of_id=1
   AND dwh_contains_id IN
       (SELECT dwh_content_id
          FROM dwh_content
         WHERE tags LIKE '%<apache-spark>%' );
-- INSERT 0 31018
-- Time: 811,497 ms 

-- Add memberships for users who only replies to a question with the <apache-spark> tag 
INSERT INTO dwh_membership(dwh_member_id,dwh_topic_id) 
SELECT DISTINCT(dwh_author_id), 4
  FROM dwh_contributes 
 WHERE NOT EXISTS
       (SELECT *
          FROM dwh_membership
         WHERE dwh_topic_id=4
           AND dwh_member_id=dwh_author_id
       )
   AND dwh_kind_of_id=2
   AND dwh_reply_id IN
       (SELECT dwh_content_id
          FROM dwh_content
         WHERE tags LIKE '%<apache-spark>%' );
-- INSERT 0 12955
-- Time: 481,742 ms

-- Add the number of questions about hadoop of an user
UPDATE dwh_membership
   SET nb_questions = (SELECT COUNT(*)
                         FROM dwh_contributes
					              WHERE dwh_author_id = dwh_membership.dwh_member_id
                          AND dwh_kind_of_id=1
					                AND dwh_contains_id IN
                              (SELECT dwh_content_id FROM dwh_content WHERE tags like '%<hadoop>%'))
	WHERE dwh_topic_id=1;
-- UPDATE 36288
-- Time: 1025,175 ms (00:01,025)

-- Add the number of questions about hive of an user
UPDATE dwh_membership
   SET nb_questions = (SELECT COUNT(*)
                         FROM dwh_contributes
					              WHERE dwh_author_id = dwh_membership.dwh_member_id
                          AND dwh_kind_of_id=1
					                AND dwh_contains_id IN
                              (SELECT dwh_content_id FROM dwh_content WHERE tags like '%<hive>%'))
 WHERE dwh_topic_id=2;
-- UPDATE 18114
-- Time: 563,575 ms 

-- Add the number of questions about apache-pig of an user
UPDATE dwh_membership
   SET nb_questions = (SELECT COUNT(*)
                         FROM dwh_contributes
					              WHERE dwh_author_id = dwh_membership.dwh_member_id
                          AND dwh_kind_of_id=1
					                AND dwh_contains_id IN
                              (SELECT dwh_content_id FROM dwh_content WHERE tags like '%<apache-pig>%'))
	WHERE dwh_topic_id=3;
-- UPDATE 4676
-- Time: 138,606 ms 

-- Add the number of questions about apache-spark of an user
UPDATE dwh_membership
   SET nb_questions = (SELECT COUNT(*)
                         FROM dwh_contributes
					              WHERE dwh_author_id = dwh_membership.dwh_member_id
                          AND dwh_kind_of_id=1
					                AND dwh_contains_id IN
                              (SELECT dwh_content_id FROM dwh_content WHERE tags like '%<apache-spark>%'))
 WHERE dwh_topic_id=4;
-- UPDATE 43973
-- Time: 983,475 ms 

-- Add the number of answers about hadoop of an user
UPDATE dwh_membership
SET nb_answers = (SELECT COUNT(*)
                    FROM dwh_contributes
					         WHERE dwh_author_id = dwh_membership.dwh_member_id
                     AND dwh_kind_of_id=2
					           AND dwh_reply_id IN
                         (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hadoop>%'))
	WHERE dwh_topic_id=1;
-- UPDATE 36299
-- Time: 1260,499 ms (00:01,260)

-- Add the number of answers about hive of an user
UPDATE dwh_membership
SET nb_answers = (SELECT COUNT(*)
                    FROM dwh_contributes
					         WHERE dwh_author_id = dwh_membership.dwh_member_id
                     AND dwh_kind_of_id=2
					           AND dwh_reply_id IN
                         (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hive>%'))
	WHERE dwh_topic_id=2;
-- UPDATE 18114
-- Time: 867,026 ms

-- Add the number of answers about apache-pig of an user
UPDATE dwh_membership
SET nb_answers = (SELECT COUNT(*)
                    FROM dwh_contributes
					         WHERE dwh_author_id = dwh_membership.dwh_member_id
                     AND dwh_kind_of_id=2
					           AND dwh_reply_id IN
                         (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-pig>%'))
	WHERE dwh_topic_id=3;
-- UPDATE 4676
-- Time: 284,671 ms

-- Add the number of answers about apache-spark of an user
UPDATE dwh_membership
SET nb_answers = (SELECT COUNT(*)
                    FROM dwh_contributes
					         WHERE dwh_author_id = dwh_membership.dwh_member_id
                     AND dwh_kind_of_id=2
					           AND dwh_reply_id IN
                         (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-spark>%'))
	WHERE dwh_topic_id=4;
-- UPDATE 43973
-- Time: 1389,743 ms (00:01,390)


-- Add mean score of the questions for hadoop
UPDATE dwh_membership
   SET mean_score_questions =
       CASE
       WHEN nb_questions=0 THEN 0
			 ELSE (SELECT AVG(score)
               FROM dwh_contributes
					    WHERE dwh_author_id = dwh_membership.dwh_member_id AND
                    dwh_kind_of_id=1
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hadoop>%'))
       END
 WHERE dwh_topic_id=1;
-- UPDATE 36288
-- Time: 1016,029 ms (00:01,016)

-- Add mean score the questions for hive
UPDATE dwh_membership
   SET mean_score_questions =
       CASE
       WHEN nb_questions=0 THEN 0
			 ELSE (SELECT AVG(score)
               FROM dwh_contributes
					   WHERE dwh_author_id = dwh_membership.dwh_member_id
               AND dwh_kind_of_id=1
					     AND dwh_contains_id IN
                   (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hive>%'))
       END
 WHERE dwh_topic_id=2;
-- UPDATE 18114
-- Time: 687,873 ms 

-- Add mean score of the questions for apache-pig
UPDATE dwh_membership
   SET mean_score_questions =
       CASE
       WHEN nb_questions=0 THEN 0
			 ELSE (SELECT AVG(score)
               FROM dwh_contributes
					    WHERE dwh_author_id = dwh_membership.dwh_member_id AND
                    dwh_kind_of_id=1
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-pig>%'))
       END
 WHERE dwh_topic_id=3;
-- UPDATE 4676
-- Time: 146,252 ms

-- Add mean score the questions for apache-spark
UPDATE dwh_membership
   SET mean_score_questions =
       CASE
       WHEN nb_questions=0 THEN 0
			 ELSE (SELECT AVG(score)
               FROM dwh_contributes
					   WHERE dwh_author_id = dwh_membership.dwh_member_id
               AND dwh_kind_of_id=1
					     AND dwh_contains_id IN
                   (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-spark>%'))
       END
 WHERE dwh_topic_id=4;
-- UPDATE 43973
-- Time: 1207,964 ms (00:01,208)

-- Add mean score of the answers for hadoop
UPDATE dwh_membership
   SET mean_score_answers =
       CASE
       WHEN nb_answers=0 THEN 0
			 ELSE (SELECT AVG(score)
               FROM dwh_contributes
					    WHERE dwh_author_id = dwh_membership.dwh_member_id
                AND dwh_kind_of_id=2
					      AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags like '%<hadoop>%'))
       END
 WHERE dwh_topic_id=1;
-- UPDATE 36288
-- Time: 1191,787 ms (00:01,192)

-- Add mean score of the answers for hive
UPDATE dwh_membership
SET mean_score_answers =
    CASE
    WHEN nb_answers=0 THEN 0
		ELSE (SELECT AVG(score)
            FROM dwh_contributes
					 WHERE dwh_author_id = dwh_membership.dwh_member_id
             AND dwh_kind_of_id=2
					   AND dwh_reply_id IN
                 (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hive>%'))
    END
 WHERE dwh_topic_id=2;
-- UPDATE 18114
-- Time: 927,253 ms 

-- Add mean score of the answers for apache-pig
UPDATE dwh_membership
   SET mean_score_answers =
       CASE
       WHEN nb_answers=0 THEN 0
			 ELSE (SELECT AVG(score)
               FROM dwh_contributes
					    WHERE dwh_author_id = dwh_membership.dwh_member_id
                AND dwh_kind_of_id=2
					      AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags like '%<apache-pig>%'))
       END
 WHERE dwh_topic_id=3;
-- UPDATE 4676
-- Time: 285,311 ms 

-- Add mean score of the answers for apache-spark
UPDATE dwh_membership
SET mean_score_answers =
    CASE
    WHEN nb_answers=0 THEN 0
		ELSE (SELECT AVG(score)
            FROM dwh_contributes
					 WHERE dwh_author_id = dwh_membership.dwh_member_id
             AND dwh_kind_of_id=2
					   AND dwh_reply_id IN
                 (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-spark>%'))
    END
 WHERE dwh_topic_idq=4;
-- UPDATE 43973
-- Time: 1281,290 ms (00:01,281)

-- Add the zscore indicator for each user
UPDATE dwh_membership
   SET z_score = (nb_answers-nb_questions)/sqrt(nb_answers+nb_questions);
-- UPDATE 103051
-- Time: 708,258 ms

-- Add rank of an answer by using the RANK() function 
ALTER TABLE dwh_contributes ADD rank INTEGER; 
UPDATE dwh_contributes ctr
SET rank = (SELECT rank from
                          (SELECT dwh_contains_id,RANK()
                                    OVER(PARTITION BY dwh_reply_id ORDER BY score DESC)
  				                   FROM dwh_contributes 
			                      WHERE dwh_reply_id=ctr.dwh_reply_id) as r
     	       WHERE r.dwh_contains_id=ctr.dwh_contains_id) 
WHERE dwh_kind_of_id=2 ;
--UPDATE 143477
-- Time: 4360852.171 ms (01:12:40.852).

-- Add utility of the answers based on their rank
ALTER TABLE dwh_contributes ADD utility FLOAT;
UPDATE dwh_contributes
   SET utility = (SELECT 1.0/rank)
 WHERE dwh_kind_of_id=2;
--UPDATE 143477
--Time: 1221.082 ms (00:01.221)


-- Add MEC (Mean Expert Contribution) of members for the hadoop community
UPDATE dwh_membership m
   SET mec =
       (
         (
           SELECT(
		         (SELECT SUM(d1.utility*d2.answer_count)
                FROM dwh_contributes d1
                     JOIN dwh_contributes d2
                         ON d1.dwh_reply_id=d2.dwh_contains_id
		           WHERE d1.dwh_author_id=m.dwh_member_id
                 AND d1.dwh_reply_id IN
				             (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hadoop>%')) --sum of (utility*debatableness)
		                 / -- divided by
		                 (SELECT mean_debatableness
                        FROM dwh_cop
                       WHERE dwh_topic_id=1
                     )
           )
         ) -- mean debatableness of hadoop community
	       /	 -- divided by the number of answers from the member
		     m.nb_answers
       )
 WHERE m.dwh_topic_id=1;
--UPDATE 36288
--Time: 272850.644 ms (04:32.851)

-- Add MEC (Mean Expert Contribution) of members for the hive community
UPDATE dwh_membership m
   SET mec =
       (
         (
           SELECT(
		         (SELECT SUM(d1.utility*d2.answer_count)
                FROM dwh_contributes d1
                     JOIN dwh_contributes d2
                         ON d1.dwh_reply_id=d2.dwh_contains_id
		           WHERE d1.dwh_author_id=m.dwh_member_id
                 AND d1.dwh_reply_id IN
			               (SELECT dwh_content_id
                        FROM dwh_content
                       WHERE tags LIKE '%<hive>%'
                     )
             ) --sum of (utility*debatableness)
		         / -- divided by
		         (SELECT mean_debatableness
                FROM dwh_cop
               WHERE dwh_topic_id=2
             )
           )
         ) -- mean debatableness of hive community
	       /	 -- divided by the number of answers from the member
		     m.nb_answers
       )
 WHERE m.dwh_topic_id=2;
--UPDATE 18114
--Time: 125738.341 ms (02:05.738).

-- Add MEC (Mean Expert Contribution) of members for the apache-pig community
UPDATE dwh_membership m
   SET mec =
       (
         (
           SELECT(
		         (SELECT SUM(d1.utility*d2.answer_count)
                FROM dwh_contributes d1
                     JOIN dwh_contributes d2
                         ON d1.dwh_reply_id=d2.dwh_contains_id
		           WHERE d1.dwh_author_id=m.dwh_member_id
                 AND d1.dwh_reply_id IN
				             (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-pig>%')) --sum of (utility*debatableness)
		                 / -- divided by
		                 (SELECT mean_debatableness
                        FROM dwh_cop
                       WHERE dwh_topic_id=3
                     )
           )
         ) -- mean debatableness of apache-pig community
	       /	 -- divided by the number of answers from the member
		     m.nb_answers
       )
 WHERE m.dwh_topic_id=3;
--UPDATE 4676
--Time: 33070.056 ms (00:33.070)

-- Add MEC (Mean Expert Contribution) of members for the apache-spark community
UPDATE dwh_membership m
   SET mec =
       (
         (
           SELECT(
		         (SELECT SUM(d1.utility*d2.answer_count)
                FROM dwh_contributes d1
                     JOIN dwh_contributes d2
                         ON d1.dwh_reply_id=d2.dwh_contains_id
		           WHERE d1.dwh_author_id=m.dwh_member_id
                 AND d1.dwh_reply_id IN
			               (SELECT dwh_content_id
                        FROM dwh_content
                       WHERE tags LIKE '%<apache-spark>%'
                     )
             ) --sum of (utility*debatableness)
		         / -- divided by
		         (SELECT mean_debatableness
                FROM dwh_cop
               WHERE dwh_topic_id=4
             )
           )
         ) -- mean debatableness of apache-spark community
	       /	 -- divided by the number of answers from the member
		     m.nb_answers
       )
 WHERE m.dwh_topic_id=4;
--UPDATE 43973
--Time: 412239.155 (06:52.239)


-- Add in-degree of members for the hadoop community
UPDATE dwh_membership
   SET in_degree =  (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
      WHERE dwh_contains_id in
				    (SELECT (dwh_reply_id)
				       FROM dwh_contributes
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2 AND
				 	          dwh_reply_id
                    IN (SELECT dwh_content_id
                          FROM dwh_content
                         WHERE tags LIKE '%<hadoop>%'
                    )
            )
   )
WHERE dwh_topic_id=1;
--UPDATE 36288
--Time: 271645.043 ms (04:31.645)


-- Add in-degree of members for the hive community
UPDATE dwh_membership
   SET in_degree =  (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
      WHERE dwh_contains_id in
				    (SELECT(dwh_reply_id)
				       FROM dwh_contributes
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2
                AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hive>%')
            )
   )
 WHERE dwh_topic_id=2;
--UPDATE 18114
--Time: 122255.246 ms (02:02.255)

-- Add in-degree of members for the apache-pig community
UPDATE dwh_membership
   SET in_degree =  (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
      WHERE dwh_contains_id in
				    (SELECT (dwh_reply_id)
				       FROM dwh_contributes
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2 AND
				 	          dwh_reply_id
                    IN (SELECT dwh_content_id
                          FROM dwh_content
                         WHERE tags LIKE '%<apache-pig>%'
                    )
            )
   )
WHERE dwh_topic_id=3;
--UPDATE 4676
--Time: 31743.035 ms (00:31.743)


-- Add in-degree of members for the apache-spark community
UPDATE dwh_membership
   SET in_degree =  (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
      WHERE dwh_contains_id in
				    (SELECT(dwh_reply_id)
				       FROM dwh_contributes
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2
                AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-spark>%')
            )
   )
 WHERE dwh_topic_id=4;
--UPDATE 43973
--Time: 414295.361 ms (06:54.295)

-- Add out-degree of members from the hadoop community
UPDATE dwh_membership
   SET out_degree = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
		  WHERE dwh_kind_of_id=2 AND dwh_reply_id IN 
				    (SELECT dwh_contains_id 
				       FROM dwh_contributes 
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hadoop>%')
            )
   )
 WHERE dwh_topic_id=1;
--UPDATE 36288
--Time: 742929.874 ms (12:22.930)

-- Add out-degree of members for the hive community
UPDATE dwh_membership
   SET out_degree = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
	    WHERE dwh_kind_of_id=2
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hive>%')
            )
   )
 WHERE dwh_topic_id=2;
--UPDATE 18114
--Time: 401891.648 ms (06:41.892)

-- Add out-degree of members from the apache-pig community
UPDATE dwh_membership
   SET out_degree = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
		  WHERE dwh_kind_of_id=2 AND dwh_reply_id IN 
				    (SELECT dwh_contains_id 
				       FROM dwh_contributes 
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-pig>%')
            )
   )
 WHERE dwh_topic_id=3;
--UPDATE 4676
--Time: 105393.021 ms (01:45.393)

-- Add out-degree of members for the apache-spark community
UPDATE dwh_membership
   SET out_degree = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
	    WHERE dwh_kind_of_id=2
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-spark>%')
            )
   )
 WHERE dwh_topic_id=4;
--UPDATE 43973
--Time: 1069199.300 ms (17:49.199)

-- Add degree for members membership (in-degree + out-degree)
UPDATE dwh_membership
   SET degree=in_degree + out_degree;
--UPDATE 103051
--Time 704.148 ms


-- Add in-degree of accepted answers for members of the hadoop community
UPDATE dwh_membership
   SET in_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
      WHERE dwh_contains_id in
				    (SELECT(dwh_reply_id)
               FROM dwh_contributes
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2
                AND is_accepted=true
                AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hadoop>%')
            )
   )
 WHERE dwh_topic_id=1;
--UPDATE 36288
--Time: 213784.735 ms (03:33.785)

-- Add in-degree of accepted answers for members of the hive community
UPDATE dwh_membership
   SET in_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
      WHERE dwh_contains_id in
				    (SELECT(dwh_reply_id) 
				       FROM dwh_contributes 
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2
                AND is_accepted=true
                AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hive>%')
            )
   )
 WHERE dwh_topic_id=2;
--UPDATE 18114
--Time: 93465.438 ms (01:33.465)

-- Add in-degree of accepted answers for members of the apache-pig community
UPDATE dwh_membership
   SET in_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
      WHERE dwh_contains_id in
				    (SELECT(dwh_reply_id)
               FROM dwh_contributes
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2
                AND is_accepted=true
                AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-pig>%')
            )
   )
 WHERE dwh_topic_id=3;
--UPDATE 4676
--Time: 26535.669 ms (00:26.536)

-- Add in-degree of accepted answers for members of the apache-spark community
UPDATE dwh_membership
   SET in_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
      WHERE dwh_contains_id in
				    (SELECT(dwh_reply_id) 
				       FROM dwh_contributes 
              WHERE dwh_author_id=dwh_member_id
                AND dwh_kind_of_id=2
                AND is_accepted=true
                AND dwh_reply_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-spark>%')
            )
   )
 WHERE dwh_topic_id=4;
--UPDATE 43973
--Time: 254821.550 (04:14.822)


-- Add out-degree of accepted answers for members of the hadoop community
UPDATE dwh_membership
   SET out_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
		  WHERE dwh_kind_of_id=2
        AND is_accepted=true
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id 
				       FROM dwh_contributes 
				      WHERE dwh_author_id=dwh_member_id 
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hadoop>%')
            )
   )
 WHERE dwh_topic_id=1;
--UPDATE 36288
--Time: 768951.848 ms (12:48.952)

-- Add out-degree of accepted answers for members of the hive community
UPDATE dwh_membership
   SET out_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
		  WHERE dwh_kind_of_id=2
        AND is_accepted=true
        AND dwh_reply_id IN
            (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hive>%')
            )
   )
 WHERE dwh_topic_id=2;
--UPDATE 18114
--Time: 412452.658 ms (06:52.453)

-- Add out-degree of accepted answers for members of the apache-pig community
UPDATE dwh_membership
   SET out_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
		  WHERE dwh_kind_of_id=2
        AND is_accepted=true
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id 
				       FROM dwh_contributes 
				      WHERE dwh_author_id=dwh_member_id 
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-pig>%')
            )
   )
 WHERE dwh_topic_id=3;
--UPDATE 4676
--Time: 108611.988 (01:48.612)

-- Add out-degree of accepted answers for members of the apache-spark community
UPDATE dwh_membership
   SET out_degree_accepted = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
		  WHERE dwh_kind_of_id=2
        AND is_accepted=true
        AND dwh_reply_id IN
            (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-spark>%')
            )
   )
 WHERE dwh_topic_id=4;
--UPDATE 43973
--Time: 1083042.679 ms (18:03.043)

-- Add in-degree of top-voted answers for members of the hadoop community
UPDATE dwh_membership
   SET in_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
      WHERE dwh_contains_id in
				(SELECT(dwh_reply_id) 
				   FROM dwh_contributes 
          WHERE dwh_author_id=dwh_member_id
            AND dwh_kind_of_id=2
            AND rank=1
            AND dwh_reply_id IN
                (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hadoop>%')
        )
   )
 WHERE dwh_topic_id=1;
--UPDATE 36288
--Time: 201869.821 ms (03:21.870)

-- Add in-degree of top-voted answers for members of the hive community
UPDATE dwh_membership
   SET in_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
      WHERE dwh_contains_id IN
				(SELECT(dwh_reply_id)
				   FROM dwh_contributes
          WHERE dwh_author_id=dwh_member_id
            AND dwh_kind_of_id=2
            AND rank=1
            AND	dwh_reply_id IN
                (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hive>%')
        )
   )
 WHERE dwh_topic_id=2;
--UPDATE 18114
--Time: 96449.893 ms (01:36.450)

-- Add in-degree of top-voted answers for members of the apache-pig community
UPDATE dwh_membership
   SET in_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes 
      WHERE dwh_contains_id in
				(SELECT(dwh_reply_id) 
				   FROM dwh_contributes 
          WHERE dwh_author_id=dwh_member_id
            AND dwh_kind_of_id=2
            AND rank=1
            AND dwh_reply_id IN
                (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-pig>%')
        )
   )
 WHERE dwh_topic_id=3;
--UPDATE 4676
--Time: 25734.512 ms (00:25.735)

-- Add in-degree of top-voted answers for members of the apache-spark community
UPDATE dwh_membership
   SET in_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
      WHERE dwh_contains_id IN
				(SELECT(dwh_reply_id)
				   FROM dwh_contributes
          WHERE dwh_author_id=dwh_member_id
            AND dwh_kind_of_id=2
            AND rank=1
            AND	dwh_reply_id IN
                (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-spark>%')
        )
   )
 WHERE dwh_topic_id=4;
--UPDATE 43973
--Time: 316993.488 ms (05:16.993)

-- Add out-degree of top-voted answers for members of the hadoop community
UPDATE dwh_membership
   SET out_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
		  WHERE dwh_kind_of_id=2
        AND rank=1
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hadoop>%')
            )
   )
 WHERE dwh_topic_id=1;
--UPDATE 36288
--Time: 830131.037 ms (13:50.131)

-- Add out-degree of top-voted answers for members of the hive community
UPDATE dwh_membership
   SET out_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
		  WHERE dwh_kind_of_id=2
        AND rank=1
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<hive>%')))
 WHERE dwh_topic_id=2;
--UPDATE 18114
--Time: 448383.123 ms (07:28.383)

-- Add out-degree of top-voted answers for members of the apache-pig community
UPDATE dwh_membership
   SET out_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
		  WHERE dwh_kind_of_id=2
        AND rank=1
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-pig>%')
            )
   )
 WHERE dwh_topic_id=3;
--UPDATE 4676
--Time: 118427.619 ms (01:58.428)

-- Add out-degree of top-voted answers for members of the apache-spark community
UPDATE dwh_membership
   SET out_degree_best = (
     SELECT COUNT(dwh_author_id)
		   FROM dwh_contributes
		  WHERE dwh_kind_of_id=2
        AND rank=1
        AND dwh_reply_id IN
				    (SELECT dwh_contains_id
				       FROM dwh_contributes
				      WHERE dwh_author_id=dwh_member_id
					      AND dwh_contains_id IN
                    (SELECT dwh_content_id FROM dwh_content WHERE tags LIKE '%<apache-spark>%')))
 WHERE dwh_topic_id=4;
--UPDATE 43979
--Time: 1174292.968 ms (19:34.293)


-- Add the seniority indicator for the memberships (time spent by an user in the community)
ALTER TABLE dwh_membership ADD seniority interval;
-- Start of the hadoop community
SELECT MIN(stamp)
  FROM dwh_contributes
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content
				 WHERE tags LIKE '%<hadoop>%');
-- 2008-08-20 10:43:13.227

-- End of the hadoop community
SELECT MAX(stamp) FROM dwh_contributes
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content
				 WHERE tags LIKE '%<hadoop>%');
-- 2021-12-05 04:15:33.17

-- Start of the hive community
SELECT MIN(stamp) FROM dwh_contributes
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content
				 WHERE tags LIKE '%<hive>%');
-- 2008-08-23 12:22:04.993

-- End of the hive community
SELECT MAX(stamp) FROM dwh_contributes 
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content 
				 WHERE tags LIKE '%<hive>%');
-- 2021-12-05 04:15:33.17


-- Start of the apache-pig community
SELECT MIN(stamp)
  FROM dwh_contributes
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content
				 WHERE tags LIKE '%<apache-pig>%');
-- 2008-12-15 13:57:23.883

-- End of the apache-pig community
SELECT MAX(stamp) FROM dwh_contributes
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content
				 WHERE tags LIKE '%<apache-pig>%');
-- 2021-11-25 21:34:53.76

-- Start of the apache-spark community
SELECT MIN(stamp) FROM dwh_contributes
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content
				 WHERE tags LIKE '%<apache-spark>%');
-- 2011-11-30 18:41:01.22

-- End of the apache-spark community
SELECT MAX(stamp) FROM dwh_contributes 
 WHERE dwh_contains_id IN
       (SELECT dwh_content_id
					FROM dwh_content 
				 WHERE tags LIKE '%<apache-spark>%');
-- 2021-12-05 05:22:22.207

-- Add the seniority of the hadoop community members
UPDATE dwh_membership
   SET seniority = (
     SELECT DATE_PART('DAY',
                      (SELECT
                         CASE
                         WHEN '2021-12-05 04:15:33.17' < last_access_date THEN '2021-12-05 04:15:33.17'
				                 ELSE last_access_date
				                 END AS MostEarlyDate FROM dwh_user
                        WHERE dwh_user_id=dwh_member_id)
                         -
			                   (SELECT
                            CASE WHEN '2008-08-20 10:43:13.227' > creation_date THEN '2008-08-20 10:43:13.227'
				                    ELSE creation_date
				                    END AS MostRecentDate FROM dwh_user
                           WHERE dwh_user_id=dwh_member_id))
              +1)
 WHERE dwh_topic_id=1;
--UPDATE 36288
--Time: 751.130 msec.

-- Add the seniority of the hive community members
UPDATE dwh_membership
   SET seniority = (
     SELECT DATE_PART('DAY',
                      (SELECT
                         CASE WHEN '2021-12-05 04:15:33.17' < last_access_date THEN '2021-12-05 04:15:33.17'
				                 ELSE last_access_date
				                 END AS MostEarlyDate FROM dwh_user
                        WHERE dwh_user_id=dwh_member_id)
                        -
			   		            (SELECT
                           CASE WHEN '2008-08-23 12:22:04.993' > creation_date THEN '2008-08-23 12:22:04.993'
				   		             ELSE creation_date
				   		             END AS MostRecentDate
                           FROM dwh_user WHERE dwh_user_id=dwh_member_id))
              +1)
 WHERE dwh_topic_id=2;
--UPDATE 18114
--Time: 243.815 msec.

-- Add the seniority of the apache-pig community members
UPDATE dwh_membership
   SET seniority = (
     SELECT DATE_PART('DAY',
                      (SELECT
                         CASE
                         WHEN '2021-11-25 21:34:53.76' < last_access_date THEN '2021-11-25 21:34:53.76'
				                 ELSE last_access_date
				                 END AS MostEarlyDate FROM dwh_user
                        WHERE dwh_user_id=dwh_member_id)
                         -
			                   (SELECT
                            CASE WHEN '2008-12-15 13:57:23.883' > creation_date THEN '2008-12-15 13:57:23.883'
				                    ELSE creation_date
				                    END AS MostRecentDate FROM dwh_user
                           WHERE dwh_user_id=dwh_member_id))
              +1)
 WHERE dwh_topic_id=3;
--UPDATE 4676
--Time: 76.592 msec.

-- Add the seniority of the apache-spark community members
UPDATE dwh_membership
   SET seniority = (
     SELECT DATE_PART('DAY',
                      (SELECT
                         CASE WHEN '2021-12-05 05:22:22.207' < last_access_date THEN '2021-12-05 05:22:22.207'
				                 ELSE last_access_date
				                 END AS MostEarlyDate FROM dwh_user
                        WHERE dwh_user_id=dwh_member_id)
                        -
			   		            (SELECT
                           CASE WHEN '2011-11-30 18:41:01.22' > creation_date THEN '2011-11-30 18:41:01.22'
				   		             ELSE creation_date
				   		             END AS MostRecentDate
                           FROM dwh_user WHERE dwh_user_id=dwh_member_id))
              +1)
 WHERE dwh_topic_id=4;
--UPDATE 43973
--Time: 697.469 msec.


-- Add probability to post for members of the hadoop community, i.e
-- number of answers/questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_post = (
     SELECT ((nb_answers+nb_questions)/(seniority/15.0))
		   FROM dwh_membership
			WHERE dwh_member_id=m.dwh_member_id
        AND dwh_topic_id=1)
 WHERE dwh_topic_id=1;
--UPDATE 36288
--Time: 416.273 msec.


-- Add probability to post for members of the hive community, i.e
-- number of answers/questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_post = (
     SELECT ((nb_answers+nb_questions)/(seniority/15.0))
		   FROM dwh_membership
			WHERE dwh_member_id=m.dwh_member_id
        AND dwh_topic_id=2)
 WHERE dwh_topic_id=2;
--UPDATE 18114
--Time: 202.292 msec.

-- Add probability to post for members of the apache-pig community, i.e
-- number of answers/questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_post = (
     SELECT ((nb_answers+nb_questions)/(seniority/15.0))
		   FROM dwh_membership
			WHERE dwh_member_id=m.dwh_member_id
        AND dwh_topic_id=3)
 WHERE dwh_topic_id=3;
--UPDATE 4676
--Time: 69.364 msec.

-- Add probability to post for members of the apache-spark community, i.e
-- number of answers/questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_post = (
     SELECT ((nb_answers+nb_questions)/(seniority/15.0))
		   FROM dwh_membership
			WHERE dwh_member_id=m.dwh_member_id
        AND dwh_topic_id=4)
 WHERE dwh_topic_id=4;
--UPDATE 43973
--Time: 436.407 msec.



-- Add probability to reply for members of the hadoop community, i.e.
-- number of answers divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_reply =
       (SELECT (nb_answers/(seniority/15.0))
		      FROM dwh_membership
			   WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=1)
 WHERE dwh_topic_id=1;
--UPDATE 36288
--Time 342.085 msec.

-- Add probability to reply for members of the hive community, i.e.
-- number of answers divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_reply =
       (SELECT (nb_answers/(seniority/15.0))
		      FROM dwh_membership
			   WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=2)
 WHERE dwh_topic_id=2;
--UPDATE 18114
--Time: 182.863 msec.


-- Add probability to reply for members of the apache-pig community, i.e.
-- number of answers divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_reply =
       (SELECT (nb_answers/(seniority/15.0))
		      FROM dwh_membership
			   WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=3)
 WHERE dwh_topic_id=3;
--UPDATE 4676
--Time 172.272 msec.

-- Add probability to reply for members of the apache-spark community, i.e.
-- number of answers divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_reply =
       (SELECT (nb_answers/(seniority/15.0))
		      FROM dwh_membership
			   WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=4)
 WHERE dwh_topic_id=4;
--UPDATE 43973
--Time: 397.554 msec.

-- Add probability of query for member of the hadoop community, i.e.
-- numbers of questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_query =
       (SELECT (nb_questions/(seniority/15.0))
		      FROM dwh_membership 
			   WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=1)
 WHERE dwh_topic_id=1;
--UPDATE 36288
--Time: 411.087 msec.

-- Add probability of query for member of the hive community, i.e.
-- numbers of questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_query =
       (SELECT (nb_questions/(seniority/15.0))
		      FROM dwh_membership 
			   WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=2)
 WHERE dwh_topic_id=2;
--UPDATE 18114
--Time: 178.164 msec.

-- Add probability of query for member of the apache-pig community, i.e.
-- numbers of questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_query =
       (SELECT (nb_questions/(seniority/15.0))
		      FROM dwh_membership 
			   WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=3)
 WHERE dwh_topic_id=3;
--UPDATE 4676
--Time: 166.251 msec.

-- Add probability of query for member of the apache-spark community, i.e.
-- numbers of questions divided by the numbers of 15 days period spent in the community
UPDATE dwh_membership m
   SET probability_query =
       (SELECT (nb_questions/(seniority/15.0))
		      FROM dwh_membership 
			   WHERE dwh_member_id=m.dwh_member_id AND dwh_topic_id=4)
 WHERE dwh_topic_id=4;
--UPDATE 43973
--Time: 402.307 msec.



-- Add the first_post_date (resp. last_post_date) indicator which are the date
-- id of the first contribution (resp. last contribution) of the member
ALTER TABLE dwh_membership DROP COLUMN first_post_date;
ALTER TABLE dwh_membership DROP COLUMN last_post_date; 
ALTER TABLE dwh_membership ADD COLUMN first_post_date DATE;
ALTER TABLE dwh_membership ADD COLUMN last_post_date  DATE;

UPDATE dwh_membership
   SET first_post_date = (
     SELECT MIN(dwh_date.date) AS first_date
       FROM dwh_membership AS M
            JOIN dwh_user ON M.dwh_member_id = dwh_user.dwh_user_id
            JOIN dwh_contributes ON dwh_user.dwh_user_id = dwh_contributes.dwh_author_id
            JOIN dwh_date ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
      WHERE dwh_membership.dwh_member_id = M.dwh_member_id
        AND M.dwh_topic_id = 1
        AND dwh_contributes.dwh_contains_id
            IN (SELECT dwh_content_id
                  FROM dwh_content
                 WHERE tags LIKE '%<hadoop>%')
   )
 WHERE dwh_topic_id = 1;
-- UPDATE 36288
-- Time: 2702.696 ms (00:02.703)

UPDATE dwh_membership
   SET first_post_date = (
     SELECT MIN(dwh_date.date) AS first_date
       FROM dwh_membership AS M
            JOIN dwh_user ON M.dwh_member_id = dwh_user.dwh_user_id
            JOIN dwh_contributes ON dwh_user.dwh_user_id = dwh_contributes.dwh_author_id
            JOIN dwh_date ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
      WHERE dwh_membership.dwh_member_id = M.dwh_member_id
        AND M.dwh_topic_id = 2
        AND dwh_contributes.dwh_contains_id
            IN (SELECT dwh_content_id
                  FROM dwh_content
                 WHERE tags LIKE '%<hive>%')
   )
 WHERE dwh_topic_id = 2;
-- UPDATE 18114
-- Time: 1586.210 ms (00:01.586)

UPDATE dwh_membership
   SET first_post_date = (
     SELECT MIN(dwh_date.date) AS first_date
       FROM dwh_membership AS M
            JOIN dwh_user ON M.dwh_member_id = dwh_user.dwh_user_id
            JOIN dwh_contributes ON dwh_user.dwh_user_id = dwh_contributes.dwh_author_id
            JOIN dwh_date ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
      WHERE dwh_membership.dwh_member_id = M.dwh_member_id
        AND M.dwh_topic_id = 3
        AND dwh_contributes.dwh_contains_id
            IN (SELECT dwh_content_id
                  FROM dwh_content
                 WHERE tags LIKE '%<apache-pig>%')

   )
 WHERE dwh_topic_id = 3;
-- UPDATE 4676
-- Time: 499.880 ms

UPDATE dwh_membership
   SET first_post_date = (
     SELECT MIN(dwh_date.date) AS first_date
       FROM dwh_membership AS M
            JOIN dwh_user ON M.dwh_member_id = dwh_user.dwh_user_id
            JOIN dwh_contributes ON dwh_user.dwh_user_id = dwh_contributes.dwh_author_id
            JOIN dwh_date ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
      WHERE dwh_membership.dwh_member_id = M.dwh_member_id
        AND M.dwh_topic_id = 4
        AND dwh_contributes.dwh_contains_id
            IN (SELECT dwh_content_id
                  FROM dwh_content
                 WHERE tags LIKE '%<apache-spark>%')

   )
-- WHERE dwh_topic_id = 4;
-- UPDATE 43973

UPDATE dwh_membership
   SET last_post_date = (
     SELECT MAX(dwh_date.date) AS first_date
       FROM dwh_membership AS M
            JOIN dwh_user ON M.dwh_member_id = dwh_user.dwh_user_id
            JOIN dwh_contributes ON dwh_user.dwh_user_id = dwh_contributes.dwh_author_id
            JOIN dwh_date ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
      WHERE dwh_membership.dwh_member_id = M.dwh_member_id
        AND M.dwh_topic_id = 1
        AND dwh_contributes.dwh_contains_id
            IN (SELECT dwh_content_id
                  FROM dwh_content
                 WHERE tags LIKE '%<hadoop>%')
   )
 WHERE dwh_topic_id = 1;
-- UPDATE 36288
-- Time: 2469.452 ms (00:02.469)

UPDATE dwh_membership
   SET last_post_date = (
     SELECT MIN(dwh_date.date) AS first_date
       FROM dwh_membership AS M
            JOIN dwh_user ON M.dwh_member_id = dwh_user.dwh_user_id
            JOIN dwh_contributes ON dwh_user.dwh_user_id = dwh_contributes.dwh_author_id
            JOIN dwh_date ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
      WHERE dwh_membership.dwh_member_id = M.dwh_member_id
        AND M.dwh_topic_id = 2
        AND dwh_contributes.dwh_contains_id
            IN (SELECT dwh_content_id
                  FROM dwh_content
                 WHERE tags LIKE '%<hive>%')
   )
 WHERE dwh_topic_id = 2;
-- UPDATE 18114
-- Time: 1630.933 ms (00:01.631)

UPDATE dwh_membership
   SET last_post_date = (
     SELECT MAX(dwh_date.date) AS first_date
       FROM dwh_membership AS M
            JOIN dwh_user ON M.dwh_member_id = dwh_user.dwh_user_id
            JOIN dwh_contributes ON dwh_user.dwh_user_id = dwh_contributes.dwh_author_id
            JOIN dwh_date ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
      WHERE dwh_membership.dwh_member_id = M.dwh_member_id
        AND M.dwh_topic_id = 3
        AND dwh_contributes.dwh_contains_id
            IN (SELECT dwh_content_id
                  FROM dwh_content
                 WHERE tags LIKE '%<apache-pig>%')
   )
 WHERE dwh_topic_id = 3;
-- UPDATE 4676
-- Time: 485.331 ms

UPDATE dwh_membership
   SET last_post_date = (
     SELECT MIN(dwh_date.date) AS first_date
       FROM dwh_membership AS M
            JOIN dwh_user ON M.dwh_member_id = dwh_user.dwh_user_id
            JOIN dwh_contributes ON dwh_user.dwh_user_id = dwh_contributes.dwh_author_id
            JOIN dwh_date ON dwh_contributes.dwh_creation_date_id = dwh_date.dwh_date_id
      WHERE dwh_membership.dwh_member_id = M.dwh_member_id
        AND M.dwh_topic_id = 4
        AND dwh_contributes.dwh_contains_id
            IN (SELECT dwh_content_id
                  FROM dwh_content
                 WHERE tags LIKE '%<apache-spark>%')
   )
 WHERE dwh_topic_id = 4;
-- UPDATE 43973
-- Time: 3167.627 ms (00:03.168)
