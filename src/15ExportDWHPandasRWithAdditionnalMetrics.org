#+TITLE: Notebook for exporting the data warehouse 
#+AUTHOR: Koboyoda Arthur ASSIMA Maxime MORGE
#+DATE:  <2022-02-17 Thu>


* DONE Source Database and relations size

#+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
 SELECT pg_size_pretty(pg_relation_size(('DWH_Body'))) AS Body,
 	pg_size_pretty(pg_relation_size(('DWH_Comments'))) AS Comments,
        pg_size_pretty(pg_relation_size(('DWH_Content'))) AS Content,
	pg_size_pretty(pg_relation_size(('DWH_Contributes'))) AS Contributes,
        pg_size_pretty(pg_relation_size(('DWH_Cop'))) AS Cop,
        pg_size_pretty(pg_relation_size(('DWH_Date'))) AS Date,
       	pg_size_pretty(pg_relation_size(('DWH_Membership'))) AS Membership,
        pg_size_pretty(pg_relation_size(('DWH_PostType'))) AS PostType,
	pg_size_pretty(pg_relation_size(('DWH_User'))) AS User;
#+END_SRC

#+RESULTS
:  body  | comments | content | contributes |    cop     |  date  | membership |  posttype  | user
:--------+----------+---------+-------------+------------+--------+------------+------------+-------
: 355 MB | 126 MB   | 1142 MB | 277 MB      | 8192 bytes | 368 MB | 67 MB      | 8192 bytes | 70 MB

* DONE Export the table DWH_User 

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   \COPY (SELECT dwh_user_id, reputation, views, down_votes, up_votes, creation_date, last_access_date, display_name, location, website_url, about_me, seniority FROM DWH_user) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/ExtendedPandasRStackoverlfow/users.csv' DELIMITER ';' CSV HEADER;
#+END_SRC

 #+RESULTS:
 : 242 148 users
 
* DONE Export the table DWH_Content
  
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   \COPY (SELECT dwh_content_id, title, tags, body FROM DWH_Content) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/ExtendedPandasRStackoverlfow/contents.csv' DELIMITER ';' CSV HEADER;
 #+END_SRC

 #+RESULTS:
 : 1 270 608 contents

* DONE Export the table DWH_Body
  
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   \COPY (SELECT dwh_body_id, text FROM DWH_Body) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/ExtendedPandasRStackoverlfow/bodies.csv' DELIMITER ';' CSV HEADER;
 #+END_SRC

 #+RESULTS:
 : 1 934 894 bodies
 
* DONE Export the table DWH_Date 
  
 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   \COPY (SELECT dwh_date_id, date, day, numberOfDay, numberOfWeek, month, numberOfMonth, year, epoch_date  FROM DWH_Date) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/ExtendedPandasRStackoverlfow/dates.csv' DELIMITER ';' CSV HEADER;
 #+END_SRC

 #+RESULTS:
 : 3 810 628 dates
  
* DONE Export the table DWH_PostType

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   \COPY (SELECT dwh_post_type_id, post_type  FROM DWH_PostType) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/ExtendedPandasRStackoverlfow/posttypes.csv' DELIMITER ';' CSV HEADER;
 #+END_SRC

 #+RESULTS:
 : 2 post types

* DONE Export the table DWH_Contributes

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   \COPY (SELECT dwh_author_id, dwh_kind_of_id, dwh_creation_date_id, dwh_contains_id, dwh_interlocutor_id, dwh_last_activity_id, dwh_last_edit_date_id, dwh_reply_id, score, view_count, favorite_count, answer_count, comment_count, delay, is_accepted, stamp, rank, utility FROM DWH_Contributes) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/ExtendedPandasRStackoverlfow/contributes.csv' DELIMITER ';' CSV HEADER;
 #+END_SRC

 #+RESULTS:
 : 1 268 600 contributions

* DONE Export the table DWH_Comments

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   \COPY (SELECT dwh_author_id, dwh_reacts_id, dwh_contains_id, dwh_creation_date_id, dwh_interlocutor_id, score, delay, stamp FROM DWH_Comments) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/ExtendedPandasRStackoverlfow/comments.csv' DELIMITER ';' CSV HEADER;
 #+END_SRC

 #+RESULTS:
 : 1 934 894 comments

* DONE Export the table DWH_Cop

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   \COPY (SELECT dwh_topic_id, topic, mean_debatableness FROM DWH_Cop) TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/ExtendedPandasRStackoverlfow/cops.csv' DELIMITER ';' CSV HEADER;
 #+END_SRC

 #+RESULTS:
 : 2 cops

* DONE Export the table DWH_Membership

 #+BEGIN_SRC sql :engine postgresql :cmdline "-d sodyonstack" :async
   \COPY (SELECT * FROM DWH_Membership)   TO '/Users/morge/Documents/Lille/Recherche/MoCiCoS/sodyonstack/data/ExtendedPandasRStackoverlfow/membership.csv' DELIMITER ';' CSV HEADER;
#+END_SRC

 #+RESULTS:
 |---|


 
  

