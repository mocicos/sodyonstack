DROP TABLE IF EXISTS DWH_POSTTYPE CASCADE;
CREATE TABLE DWH_POSTTYPE (
  dwh_post_type_id SERIAL,
  post_type_code SMALLINT,
  post_type VARCHAR(64),
  PRIMARY KEY (dwh_post_type_id)
);


DROP TABLE IF EXISTS DWH_BODY CASCADE;
CREATE TABLE DWH_BODY (
  dwh_body_id SERIAL,
  id_comment INTEGER,
  id_sody_comment_inquiry INTEGER,
  id_sody_comment_solution INTEGER,
  post_id INTEGER,
  id_sody_inquiry INTEGER,
  id_sody_solution INTEGER,
  id_sody_user INTEGER,
  text TEXT,
  PRIMARY KEY (dwh_body_id)
);


DROP TABLE IF EXISTS DWH_CONTENT CASCADE;
CREATE TABLE DWH_CONTENT (
  dwh_content_id SERIAL,
  id_post INTEGER,
  id_sody_inquiry INTEGER,
  id_sody_solution INTEGER,
  id_sody_parent INTEGER,
  id_sody_user INTEGER,
  title VARCHAR(512),
  tags VARCHAR(512),
  body TEXT,
  PRIMARY KEY (dwh_content_id)
);

DROP TABLE IF EXISTS DWH_USER CASCADE;
CREATE TABLE DWH_USER (
  dwh_user_id SERIAL,
  id_user INTEGER,
  id_sody INTEGER,
  account_id INTEGER,
  reputation INTEGER,
  views INTEGER,
  down_votes INTEGER,
  up_votes INTEGER,
  display_name VARCHAR(255),
  location VARCHAR(512),
  website_url VARCHAR(255),
  about_me TEXT,
  creation_date TIMESTAMP,
  last_access_date TIMESTAMP,
  PRIMARY KEY (dwh_user_id)
);


DROP TABLE IF EXISTS DWH_DATE CASCADE;
CREATE TABLE DWH_DATE (
  dwh_date_id SERIAL,
  date TIMESTAMP,
  day VARCHAR(10),
  numberofday INTEGER,
  numberofweek INTEGER,
  month VARCHAR(10),
  numberofmonth INTEGER,
  year NUMERIC(4),
  epoch_date BIGINT,
  PRIMARY KEY (dwh_date_id)
);


DROP TABLE IF EXISTS DWH_COMMENTS CASCADE;
CREATE TABLE DWH_COMMENTS (
  dwh_author_id SERIAL,
  dwh_reacts_id SERIAL,
  dwh_contains_id SERIAL,
  dwh_creation_date_id SERIAL,
  dwh_interlocutor_id SERIAL,
  score SMALLINT,
  delay BIGINT,
  stamp TIMESTAMP,
  PRIMARY KEY (dwh_author_id, dwh_reacts_id, dwh_contains_id, dwh_creation_date_id)
);

DROP TABLE IF EXISTS DWH_CONTRIBUTES CASCADE;
CREATE TABLE DWH_CONTRIBUTES (
  dwh_author_id SERIAL,
  dwh_contains_id SERIAL,
  dwh_creation_date_id SERIAL,
  dwh_kind_of_id SERIAL,
  dwh_interlocutor_id INTEGER,
  dwh_last_activity_id INTEGER,
  dwh_last_edit_date_id INTEGER,
  dwh_reply_id INTEGER,
  score INTEGER,
  view_count INTEGER,
  favorite_count INTEGER,
  answer_count INTEGER,
  comment_count INTEGER,
  delay BIGINT,
  is_accepted BOOLEAN,
  stamp TIMESTAMP,
  PRIMARY KEY (dwh_author_id, dwh_contains_id, dwh_creation_date_id, dwh_kind_of_id)
);
